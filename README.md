# Platform Ever Nature

[![pipeline status](https://gitlab.com/everbynatureti/plataforma-react/badges/master/pipeline.svg)](https://gitlab.com/everbynatureti/plataforma-react/-/commits/master)

## Links for online apps

- [Portal](https://portalever.web.app)
- [Storybook](https://platformever-bb86d.web.app)

## Stask of Project

- [React](https://github.com/facebook/create-react-app)
- [Redux](https://redux.js.org)
- [Redux Saga](https://redux-saga.js.org)
- [Styled Component](https://styled-components.com/)
- [Axios](https://github.com/axios/axios)
- [Yup Validation](https://github.com/jquense/yup#stringrequiredmessage-string--function-schema)
- [Formik](https://jaredpalmer.com/formik/docs/overview)
- [Airbnb React/JSX Style Guide](https://airbnb.io/javascript/react/)
- [Eslint](https://eslint.org/docs/user-guide/configuring)

## Structure

    .
    |__ .storybook
    |   |__ main.js
    |   |__ preview-head.html
    |   |__ preview.js
    |   |__ webpack.config.js
    |__ public
    |__ src
    |   |__ assets
    |   |  |__ images
    |   |  |__ styles
    |   |__ components
    |   |  |__ atoms
    |   |  |__ molecules
    |   |  |__ organisms
    |   |  |__ templates
    |   |  |__ index.js
    |   |__ config
    |   |__ pages
    |   |__ redux
    |   |__ routes
    |   |__ redux
    |   |__ utils
    |__ .babelrc
    |__ .editorconfig
    |__ .eslintignore
    |__ .eslintrc.json
    |__ .firebaserc
    |__ .gitignore
    |__ .gitlab-ci.yml
    |__ .prettierrc
    |__ config-overrides.js
    |__ CONTRIBUTING.md
    |__ firebase.json
    |__ jsconfig.json
    |__ LICENSE.md
    |__ package.json
    |__ README.md
    |__ yarn.lock
    |

## Available Alias

- @assets
- @components
- @atoms
- @molecules
- @organisms
- @templates
- @config
- @pages
- @redux
- @routes
- @services
- @utils

## Available Scripts

- `yarn start`
- `yarn test`
- `yarn build`
- `yarn storybook`

## Extensions

- Editor Config
- Prettier
- Eslint
- Auto Close Tag
- DotEnv
- JavaScript ES6
- Styled Component
- Color Highlight
- Git Lens
- Git Blame
- gitignore
- Guides

## Settings

- Tab Size: 2
- Format on Save: ON
- Word Wrap: 120

## Patterns of Projects

- **Commit**

  - feat: add page my profile
  - refactor:

- **Branch**

  - feat/myProfile

- **Open PR**
  - Name Page - feat/myProfile

## Structure of CSS

> Use methodology **BEM** CSS

**Text:**

- font
- text

**Display:**

- flex
- grid

**Lenght:**

- width
- height

**Spacing:**

- margin
- padding

**Other:**

- border-width
- border-style
- border-radius

**Colors:**

- background
- color

**Transitions:**

**Class:**

- .container {}

**Media Query**

```js
import styled from 'styled-component';

export const Container = styled.div`
  font-size: 1rem;
  font-weight: normal;
  text-align: center;

  display: flex;
  flex-direction: row;
  justify-content: center;

  width: 100%;
  height: 100%;

  margin: 0 16px;
  padding: 0 16px;

  border: none;
  border-radius: 0.2rem;

  background: #1e90ef;
  color: #fff;

  transition: all 500ms linear;

  .container__header {
    width: 100%;
    height: 70px;

    background: orange;
  }

  @media (min-width: 768px) {
    width: 50%;
    height: 80%;
  }
`;
```

## Structure of all component

    .
    |_ something
        |_ something.styory.jsx
        |_ something.test.jsx
        |_ index.jsx
        |_ styled.js

## References

- [Uilizando Git Flow](https://medium.com/trainingcenter/utilizando-o-fluxo-git-flow-e63d5e0d5e04)
- [Tutorial: Git com Git Flow](https://medium.com/@lariodiniz/tutorial-git-com-git-flow-476ad906c8ae)
- [Git Flow // Dicionário do Programador](https://www.youtube.com/watch?v=oweffeS8TRc)
- [GitFlow Rodrigo Branas](https://medium.com/@lariodiniz/tutorial-git-com-git-flow-476ad906c8ae)
- [Convetional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [Redux Saga: Você no controle das operações assíncronas](https://medium.com/nossa-coletividad/redux-saga-voc%C3%AA-no-controle-das-opera%C3%A7%C3%B5es-ass%C3%ADncronas-71c9e6b3aabc)
- [Redux-Saga: Gerenciando efeitos e ações](https://medium.com/@oieduardorabelo/redux-saga-gerenciando-efeitos-f518a31c744e)
- [Component for Credit Card](https://github.com/amarofashion/react-credit-cards)
- [Component for DataTable](https://github.com/jbetancur/react-data-table-component)
- [Yup Schema Validation](https://github.com/jquense/yup#stringrequiredmessage-string--function-schema)
- [Formik Form](https://jaredpalmer.com/formik/docs/overview)
- [Methodologies CSS](http://getbem.com/introduction/)
- [Como melhorar seus códigos usando Object Calisthenics](https://imasters.com.br/back-end/como-melhorar-seus-codigos-usando-object-calisthenics)
- [PHP Object Calisthenics Made Simple](https://www.tomasvotruba.com/blog/2017/06/26/php-object-calisthenics-rules-made-simple-version-3-0-is-out-now/#:~:text=Object%20Calisthenics%20are%209%20language,CodeSniffer%203.0%20and%20PHP%207.1.)
- [Object Calisthenics descreve 9 regras básicas](https://gist.github.com/suissa/79aa860161b13e50e5fa3a0120fb6d68)
- [Object Calisthenics In JavaScript - An Introduction](https://www.bennadel.com/blog/2374-object-calisthenics-in-javascript-an-introduction.htm)
- [Object Calisthenics In JavaScript - My First Attempt](https://www.bennadel.com/blog/2375-object-calisthenics-in-javascript-my-first-attempt.htm)
- [Object Calisthenics no Javascript ou Javascript para adultos! — Qualidade de Código no Front-End](https://medium.com/@evertonthepaula/object-calisthenics-no-javascript-ou-javascript-para-adultos-qualidade-de-c%C3%B3digo-no-front-end-4f2afc7bb1d3)
- [Testando componentes React](https://medium.com/mercos-engineering/testando-componentes-react-8c83ad9ffe6c)
- [Como testar aplicações em React ( ou a console do seu navegador merece menos erros ).](https://medium.com/@rodrigopicancotavares/como-testar-aplica%C3%A7%C3%B5es-em-react-ou-a-incr%C3%ADvel-hist%C3%B3ria-de-como-o-javascript-pode-ser-levado-a-8a32c91aaa48)
- [Testando na prática seus componentes React usando o Jest](https://alexandremagno.net/2018/09/testando-componentes-react-jest/)
- [Padrões para construir componentes com React](https://alexandremagno.net/2018/08/padroes-para-construir-componentes-usando-react/)
- [Estrutura Redux escalável com Ducks](https://blog.rocketseat.com.br/estrutura-redux-escalavel-com-ducks/)
- [Ducks: Redux Reducer Bundles](https://github.com/erikras/ducks-modular-redux)
- [What is Redux Ducks?](https://medium.com/@matthew.holman/what-is-redux-ducks-46bcb1ad04b7)
- [Organizando o Redux com Duck Pattern e Redux Sauce](https://www.youtube.com/watch?v=q-If9n-tUyA)
