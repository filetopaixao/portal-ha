import { api, config } from '@services/api';

const fetcher = (url) => api.get(url, config).then((res) => res.data);

export const getCategories = async (url) => {
  try {
    const response = await fetcher(url);
    const res = response.data.map((item) => {
      return {
        id: item.id,
        label: item.name,
        value: item.id,
      };
    });

    return {
      data: res,
    };
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const getCategoryProducts = async () => {
  try {
    const response = await api.get('/api/portal/categories', config);
    return response;
  } catch (e) {
    console.log('erro na busca pelo produto', e);
    return {};
  }
};

export const getProductByCategory = async (id) => {
  try {
    const response = await api.get(`/api/portal/categories/${id}`, config);
    return response;
  } catch (e) {
    console.log('erro na busca pelo produto', e);
    return {};
  }
};

export async function getProductById(id) {
  try {
    const response = await api.get(`/api/portal/products/${id}`, config);
    return response;
  } catch (e) {
    console.log('erro na busca pelo produto', e);
    return {};
  }
}
