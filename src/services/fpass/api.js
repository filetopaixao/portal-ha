import axios from 'axios';

export const api = axios.create({
  baseURL: `https://devs.api.fpass.com.br:3333`,
  timeout: 0,
  headers: {
    'Content-Type': 'application/json',
    Authorization: localStorage.getItem('token') ? `bearer ${localStorage.getItem('token')}` : '',
  },
});

export default api;
