import { api, config } from '@services/api';

export const getUser = async () => {
  try {
    const response = await api.get('/api/portal/user-data', config);
    return response;
  } catch (e) {
    console.log('Erro no carregamento dos dados do usuário!', e);
    return {};
  }
};

export const updateUser = async (data) => {
  try {
    const response = await api.put('/api/portal/user-data', data, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

/*
export const getUserSocials = async (page, limit) => {
  try {
    const response = await api.get(`/api/portal/user-socials/page=${page}&limit=${limit}`, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
}; */

export const getUserSocials = async () => {
  try {
    const response = await api.get(`/api/portal/user-socials`, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const addUserSocials = async (data) => {
  try {
    const response = await api.post('/api/portal/user-socials', data, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const getUserAddress = async () => {
  try {
    const response = await api.get('api/portal/user-addresses', config);

    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const addUserAddress = async (data) => {
  try {
    const response = await api.post('api/portal/user-addresses', data);
    console.log(response.data);

    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const updatedUserAddress = async (data) => {
  try {
    const response = await api.put(`api/portal/user-addresses/${data.id}`, data, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const deleteUserAddress = async (id) => {
  try {
    const response = await api.delete(`api/portal/user-addresses/${id}`, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};
