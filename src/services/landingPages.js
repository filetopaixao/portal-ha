import { api } from './api';

export const list = async () => {
  try {
    const response = await api.get('/api/portal/landing-pages');
    return response;
  } catch (e) {
    console.log('erro na busca pelo produto', e);
    return {};
  }
};

export const show = async (id) => {
  try {
    const response = await api.get(`/api/portal/landing-pages/${id}`);
    return response;
  } catch (e) {
    console.log('erro na busca pelo produto', e);
    return {};
  }
};
