import { api, config } from '@services/api';

export default async function getInfo() {
  try {
    const response = await api.get('api/portal/associations/partners', config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
}
