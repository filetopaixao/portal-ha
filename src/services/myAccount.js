import { api, config } from '@services/api';

export async function getAdress() {
  try {
    const response = await api.get('/api/portal/addresses', config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
}

export default getUser;
