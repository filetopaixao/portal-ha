import { api, config } from '@services/api';

export const getListTransference = async () => {
  try {
    const response = await api.get('/api/portal/bank-transferences', config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const postTransference = async (data) => {
  try {
    const response = await api.post('/api/portal/bank-transferences', data, config);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};
