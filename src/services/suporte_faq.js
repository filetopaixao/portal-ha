import { api, config } from '@services/api';
import axios from 'axios';

export const getAllCategories = async () => {
  try {
    const response = await api.get('/api/portal/faq-categories', config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const getByIdCategories = async (id) => {
  try {
    const response = await api.get(`/api/portal/faq-categories/${id}`, config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const getByIdFaqs = async (id) => {
  try {
    const response = await api.get(`/api/portal/faq/${id}`, config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const getAllFaks = async () => {
  try {
    const response = await api.get('/api/portal/faq', config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const sendFormContact = async (data) => {
  try {
    const response = await api.post('/api/portal/send-support-mail', data, config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const uploadFile = async (data) => {
  try {
    const response = await axios.post('http://18.221.121.253:4000/upload', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return response;
  } catch (e) {
    console.log('erro no carregamento da imagem', e);
    return {};
  }
};
