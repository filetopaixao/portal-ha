import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router';

import LoginPage from '@pages/Login';
import ForgotPassPage from '@pages/Login/ForgotPass';
import ConfirmationPage from '@pages/Login/Confirmation';
import TermBrPage from '@pages/Login/TermBR';
import TermEuaPage from '@pages/Login/TermEUA';
import HomePage from '@pages/Home';
import EntrepreneursPage from '@pages/Entrepreneurs';
import MainPage from '@components/templates/Main';
import CupomPage from '@pages/Coupon';
import CupomCreatePage from '@pages/Coupon/CreateCupom/';
import ProductsPage from '@pages/Products';
import DynamicCartPage from '@pages/DynamicCart';
import DynamicCartCreatePage from '@pages/DynamicCart/CreateCart/';
import MyEcommerce from '@pages/MyEcommerce';
import MyClients from '@pages/MyClients';
import MyAccount from '@pages/MyAccount';
import Suporte from '@pages/Suporte';
import FormSuporteDetalhe from '@pages/Suporte/detalheForm';
import FormSuporteContato from '@pages/Suporte/contatoForm';
import Withdraw from '@pages/Withdraw';
import MyManager from '@pages/MyManager';
import CalendarPage from '@pages/Calendar';
import MyPartners from '@pages/MyPartners';
import Admin from '@pages/Admin';
import Compras from '@pages/Reports/Shopping';
import Vendas from '@pages/Reports/Sales';
import { isAuthenticated } from '@services/auth';
import SupportLinks from '../pages/SupportLinks';
import LandingPages from '../pages/LandingPages';
import CheckoutDynamic from '../pages/CheckoutDynamic';

const PrivateRoute = ({ component: Component, name }) => (
  <Route
    render={
      (props) =>
        isAuthenticated() ? (
          <MainPage title={name}>
            <Component component />
          </MainPage>
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
      // eslint-disable-next-line react/jsx-curly-newline
    }
  />
);

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.objectOf(PropTypes.string),
  name: PropTypes.string,
};

PrivateRoute.defaultProps = {
  location: {},
  name: '',
};

const Routes = () => (
  <Switch>
    <Route exact path="/login" component={LoginPage} />
    <Route exact path="/forgot" component={ForgotPassPage} />
    <Route exact path="/confirmation" component={ConfirmationPage} />
    <Route exact path="/termBR" component={TermBrPage} />
    <Route exact path="/termEUA" component={TermEuaPage} />
    <Route exact path="/checkout/dynamic/:link?" component={CheckoutDynamic} />
    <PrivateRoute exact path="/" name="Home" component={HomePage} />
    <PrivateRoute exact path="/home" name="Home" component={HomePage} />
    <PrivateRoute exact path="/admin" name="Painel Administrativo" component={Admin} />
    <PrivateRoute exact path="/entrepreneurs" name="Empreendedores" component={EntrepreneursPage} />
    <PrivateRoute exact path="/platform" name="Plataforma FPASS" component={MyEcommerce} />
    <PrivateRoute exact path="/cupom" name="Cupom de desconto" component={CupomPage} />
    <PrivateRoute exact path="/cupom/create" name="Cupom de desconto" component={CupomCreatePage} />
    <PrivateRoute exact path="/playlist" name="Playlist" component={DynamicCartPage} />
    <PrivateRoute exact path="/playlist/create" name="Playlist" component={DynamicCartCreatePage} />
    <PrivateRoute exact path="/products" name="Adquirir Produto" component={ProductsPage} />
    <PrivateRoute exact path="/myClients" component={MyClients} />
    <PrivateRoute exact path="/myAccount" component={MyAccount} />
    <PrivateRoute exact path="/myAccount/tabcontent/:opContent?" component={MyAccount} />
    <PrivateRoute exact path="/myAccount/tabcontent/activeChangePass/:opContent?" component={MyAccount} />
    <PrivateRoute exact path="/suporte" component={Suporte} />
    <PrivateRoute exact path="/suporte/detalhe/:id?/:name?" component={FormSuporteDetalhe} />
    <PrivateRoute exact path="/suporte/contato" component={FormSuporteContato} />
    <PrivateRoute exact path="/withdraw" name="Saques" component={Withdraw} />
    <PrivateRoute exact path="/myManager" name="Meu Coordenador" component={MyManager} />
    <PrivateRoute exact path="/calendar" name="Calendario" component={CalendarPage} />
    <PrivateRoute exact path="/myPartners" name="Parceiros" component={MyPartners} />
    <PrivateRoute exact path="/relatorios/compras" name="Relatório de Compras" component={Compras} />
    <PrivateRoute exact path="/relatorios/vendas" name="Relatório de Doações" component={Vendas} />
    <PrivateRoute exact path="/supportLinks" name="Links de Apoio" component={SupportLinks} />
    <PrivateRoute exact path="/landingPages" name="Landing Pages" component={LandingPages} />
    <Redirect exact to="/" />
  </Switch>
);

export default Routes;
