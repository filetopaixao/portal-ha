export const Types = {
  GET_MY_CLIENTS: 'GET_MY_CLIENTS',
  POST_MY_CLIENTS: 'POST_MY_CLIENTS',
};

export const Actions = {
  getMyClients: () => ({
    type: Types.GET_MY_CLIENTS,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  myClients: [],
  pages: [],
};

export default function myClients(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_MY_CLIENTS:
      return state;
    case Types.POST_MY_CLIENTS:
      return {
        ...state,
        myClients: action.payload.myClients,
        pages: action.payload.pages,
      };
    default:
      return state;
  }
}
