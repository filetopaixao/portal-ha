export const Types = {
  GET_THEME: 'GET_THEME',
  POST_THEME: 'POST_THEME',
};

export const Actions = {
  getTheme: () => ({
    type: Types.GET_THEME,
    payload: {},
  }),
  postTheme: (themeName, themeColor, primaryColor, secondaryColor, tertiaryColor, logo, capa, tipoApresentacao) => ({
    type: Types.POST_THEME,
    themeName,
    themeColor,
    primaryColor,
    secondaryColor,
    tertiaryColor,
    logo,
    capa,
    tipoApresentacao,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  themeName: 'Dark',
  themeColor: '#F5F6FA',
  primaryColor: '#C99E3B',
  secondaryColor: '#D3C77B',
  tertiaryColor: '',
  logo: '',
  capa: '',
  // revenda ou doacao
  tipoApresentacao: 'revenda',
};

export default function theme(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_THEME:
      return state;
    case Types.POST_THEME:
      return {
        ...state,
        themeName: action.themeName,
        themeColor: action.themeColor,
        primaryColor: action.primaryColor,
        secondaryColor: action.secondaryColor,
        tertiaryColor: action.tertiaryColor,
        logo: action.logo,
        capa: action.capa,
        tipoApresentacao: action.tipoApresentacao,
      };
    default:
      return state;
  }
}
