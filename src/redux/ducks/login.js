export const Types = {
  GET_REMEMBER_ME: 'GET_REMEMBER_ME',
  SET_REMEMBER_ME: 'SET_REMEMBER_ME',
  DELETE_REMEMBER_ME: 'DELETE_REMEMBER_ME',
};

export const Actions = {
  getRemember: () => ({
    type: Types.GET_REMEMBER_ME,
    payload: {},
  }),
  setRemember: () => ({
    type: Types.SET_REMEMBER_ME,
    payload: {},
  }),
  deleteRemember: () => ({
    type: Types.DELETE_REMEMBER_ME,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  remember: false,
};

export default function dinamycCars(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_REMEMBER_ME:
      return state;
    case Types.SET_REMEMBER_ME:
      return {
        ...state,
        remember: true,
      };
    case Types.DELETE_REMEMBER_ME:
      return {
        ...state,
        remember: false,
      };
    default:
      return state;
  }
}
