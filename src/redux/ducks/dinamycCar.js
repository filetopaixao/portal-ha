export const Types = {
  GET_NAME_DINAMYC_CAR: 'GET_NAME_DINAMYC_CAR',
  SET_DINAMYC_CAR: 'SET_DINAMYC_CAR',
};

export const Actions = {
  getNameDinamicCar: () => ({
    type: Types.GET_NAME_DINAMYC_CAR,
    payload: {},
  }),
  setNameDinamicCar: (nameVerify) => ({
    type: Types.SET_DINAMYC_CAR,
    nameVerify,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  nameDinamycCar: '',
};

export default function dinamycCars(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_NAME_DINAMYC_CAR:
      return {
        ...state,
      };
    case Types.SET_DINAMYC_CAR:
      return {
        ...state,
        nameDinamycCar: action.nameVerify,
      };
    default:
      return state;
  }
}
