export const Types = {
  GET_WITHDRAW: 'GET_WITHDRAW',
  POST_WITHDRAW: 'POST_WITHDRAW',
};

export const Actions = {
  getWithdraw: () => ({
    type: Types.GET_WITHDRAW,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  withdraws: [],
  pages: [],
};

export default function withdraw(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_WITHDRAW:
      return state;

    case Types.POST_WITHDRAW:
      return {
        ...state,
        withdraws: action.payload.withdraw,
        pages: action.payload.pages,
      };

    default:
      return state;
  }
}
