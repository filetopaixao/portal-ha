export const Types = {
  GET_MY_PRODUCTS_SELECTEDS: 'GET_MY_PRODUCTS_SELECTEDS',
  SELECT_MY_PRODUCTS_SELECTEDS: 'SELECT_MY_PRODUCTS_SELECTEDS',
  FIND_PRODUCT: 'FIND_PRODUCT',
  SELECT_PRODUCT_EXISTS: 'SELECT_PRODUCT_EXISTS',
  REMOVE_PRODUCT: 'REMOVE_PRODUCT',
  REDUCE_PRODUCT_SELECTED: 'REDUCE_PRODUCT_SELECTED',
  SUBMIT_SEND_FORM: 'SUBMIT_SEND_FORM',
  GET_SEND_FORM: 'GET_SEND_FORM',
};

export const Actions = {
  getProductsSelected: () => ({
    type: Types.GET_MY_PRODUCTS_SELECTEDS,
    payload: {},
  }),
  reduceSelectProduct: (product) => ({
    type: Types.REDUCE_PRODUCT_SELECTED,
    product,
    payload: {},
  }),
  selectProduct: (product) => ({
    type: Types.SELECT_MY_PRODUCTS_SELECTEDS,
    product,
    payload: {},
  }),
  findProduct: (product) => ({
    type: Types.FIND_PRODUCT,
    product,
    payload: {},
  }),
  selectProductExists: (product) => ({
    type: Types.SELECT_PRODUCT_EXISTS,
    product,
    payload: {},
  }),
  removeProductselect: (id) => ({
    type: Types.REMOVE_PRODUCT,
    id,
    payload: {},
  }),
  handleSubmitSendForm: (valuesSend, optionSend, address, finalized) => ({
    type: Types.SUBMIT_SEND_FORM,
    valuesSend,
    optionSend,
    address,
    finalized,
    payload: {},
  }),
  getSendForm: () => ({
    type: Types.GET_SEND_FORM,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  productHandleSelected: [],
  productExists: [],
  valuesSendForm: {},
  optionSendSelected: '',
  addressesSelected: {},
  purchaseFinalized: false,
};

export default function products(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_SEND_FORM:
      return state;
    case Types.SUBMIT_SEND_FORM:
      return {
        ...state,
        valuesSendForm: action.valuesSend,
        optionSendSelected: action.optionSend,
        addressesSelected: action.address,
        purchaseFinalized: action.finalized,
      };
    case Types.GET_MY_PRODUCTS_SELECTEDS:
      return state;
    case Types.SELECT_MY_PRODUCTS_SELECTEDS:
      return {
        ...state,
        productHandleSelected: [...state.productHandleSelected, action.product],
      };
    case Types.SELECT_PRODUCT_EXISTS:
      return {
        ...state,
        productHandleSelected: state.productHandleSelected.map((product) => {
          if (action.product.id === product.id) {
            return {
              ...product,
              isSelected: action.product.isSelected,
              qtd: product.qtd + 1,
            };
          }
          return product;
        }),
      };
    case Types.REDUCE_PRODUCT_SELECTED:
      return {
        ...state,
        productHandleSelected: state.productHandleSelected.map((product) => {
          if (action.product.id === product.id) {
            if (product.qtd > 0) {
              return {
                ...product,
                qtd: product.qtd - 1,
              };
            }
            return {
              ...product,
              qtd: 0,
            };
          }
          return product;
        }),
      };
    case Types.FIND_PRODUCT:
      return {
        ...state,
        productExists: state.productHandleSelected.map((product) => action.product.id === product.id && product),
      };
    case Types.REMOVE_PRODUCT:
      return {
        ...state,
        productHandleSelected: state.productHandleSelected.filter((product) => product.id !== action.id),
      };
    default:
      return state;
  }
}
