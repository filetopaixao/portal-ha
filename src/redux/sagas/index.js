import { all } from 'redux-saga/effects';
import init from './home';
import menu from './menu';
import withdraw from './withdraw';
import myManager from './myManager';
import myClients from './myClients';

export default function* rootSaga() {
  yield all([init(), menu(), withdraw(), myManager(), myClients()]);
}
