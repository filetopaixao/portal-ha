import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContainerForm from '@components/atoms/Container/login';

import { Submit } from '@components/molecules/Forms/styled';

import * as S from './styled';

const Confirmation = () => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();

  const redirectToLogin = () => {
    localStorage.clear();
    localStorage.removeItem('data_user_email');
    localStorage.removeItem('data_user_password');
    localStorage.removeItem('first_session');
    localStorage.removeItem('token');
    setTimeout(() => {
      history.push('/login');
    }, 2000);
  };

  return (
    <S.Container theme={themeStore.themeName}>
      <ContainerForm
        title="Senha redefinida"
        subtitle="Uma nova senha foi enviada para seu e-mail, verifique sua caixa de entrada."
        redirectTo="/login"
      >
        <Submit
          icon="arrowRight"
          variant="primary"
          showIcon
          onClick={redirectToLogin}
          primaryColor={themeStore.primaryColor}
          secondaryColor={themeStore.secondaryColor}
          tertiaryColor={themeStore.tertiaryColor}
        >
          fazer login
        </Submit>
      </ContainerForm>
    </S.Container>
  );
};

export default Confirmation;
