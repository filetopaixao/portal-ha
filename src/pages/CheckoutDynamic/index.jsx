import React, { useEffect, useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import axios from 'axios';

import { cpfMask, phoneMask, cepMask, onlyNumber, numberCVV } from '../../utils/masksForms';
import { getCars, showCar } from '../../services/dinamycCar';
import Schema from './Schema';

import * as S from './styled';

const monthList = [
  {
    id: 1,
    label: 'Janeiro',
    value: 'JAN',
  },
  {
    id: 2,
    label: 'Fevereiro',
    value: 'FEV',
  },
  {
    id: 3,
    label: 'Março',
    value: 'MARC',
  },
  {
    id: 4,
    label: 'Abril',
    value: 'ABRI',
  },
  {
    id: 5,
    label: 'Maio',
    value: 'MAI',
  },
  {
    id: 6,
    label: 'Junho',
    value: 'JUN',
  },
  {
    id: 7,
    label: 'Julho',
    value: 'JUL',
  },
  {
    id: 8,
    label: 'Agosto',
    value: 'AGO',
  },
  {
    id: 9,
    label: 'Setembro',
    value: 'SET',
  },
  {
    id: 10,
    label: 'Outubro',
    value: 'OUT',
  },
  {
    id: 11,
    label: 'Novembro',
    value: 'NOV',
  },
  {
    id: 12,
    label: 'Dezembro',
    value: 'DEZ',
  },
];

const CheckoutDynamic = () => {
  const { register, handleSubmit, setValue, errors } = useForm({
    resolver: yupResolver(Schema),
  });

  const [yearList, setYearList] = useState([]);
  const yearNow = new Date().getFullYear();

  useMemo(() => {
    console.log(yearNow);
    const yearListArray = [];
    for (let index = 0; index < 60; index += 1) {
      yearListArray.push({
        id: index,
        label: yearNow - index,
        value: yearNow - index,
      });
    }
    setYearList(yearListArray);
  }, [yearNow]);

  const [products, setProducts] = useState([]);
  const { pathname } = useLocation();
  const routeArray = pathname.split('/');
  const linkCarName = routeArray[routeArray.length - 1];
  const [buttonCard, setButtonCard] = useState(true);
  const [buttonBillet, setButtonBillet] = useState(false);

  const [activeLoading, setActiveLoading] = useState(true);

  const [subTotal, setSubTotal] = useState(0);
  const [desconto, setDesconto] = useState(0);
  const [total, setTotal] = useState(0);
  const [stateList, setStatseList] = useState([]);
  const [numberOfInstallmentsList, setNumberOfInstallmentsList] = useState([]);

  useMemo(() => {
    axios.get(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/`).then((res) => {
      const newArray = res.data.map((item) => {
        return {
          id: item.id,
          name: item.nome,
          label: item.sigla,
          region: item.regiao,
        };
      });
      setStatseList(newArray);
    });
  }, []);

  useEffect(() => {
    Promise.all([
      getCars().then((response) => {
        const { data } = response.data;
        // console.log(data);
        const filteredItems = data.filter(
          (item) => item.url && item.url.toLowerCase().includes(linkCarName.toLowerCase())
        );

        showCar(filteredItems[0].id).then((responseShow) => {
          setProducts(responseShow.data.products);
          console.log(responseShow.data);

          const { subT, desc } = responseShow.data.products.reduce(
            (accumulator, product) => {
              accumulator.subT += parseFloat(product.price);
              accumulator.desc += (parseFloat(product.price) * responseShow.data.discount_applied) / 100;
              return accumulator;
            },
            {
              subT: 0,
              desc: 0,
            }
          );

          if (subT === 0) {
            setSubTotal(0);
            setDesconto(0);
            setTotal(0);
          }
          setSubTotal(subT);
          setDesconto(desc);
          setTotal(subT - desc);

          setActiveLoading(false);
        });
      }),
    ]).then(() => {});
  }, [linkCarName]);

  useEffect(() => {
    const array = [];
    for (let index = 1; index < 13; index += 1) {
      const valor = total / index;

      array.push({
        id: index,
        label: `${index < 10 ? `0${index}` : index} X de R$ ${valor.toFixed(2)} ${index < 6 ? '(sem juros)' : ''}`,
        value: index,
      });
    }

    setNumberOfInstallmentsList(array);
  }, [total, setNumberOfInstallmentsList]);

  const handleSelecteOp = (op) => {
    switch (op) {
      case 'card':
        setButtonCard(true);
        setButtonBillet(false);
        break;
      case 'billet':
        setButtonCard(false);
        setButtonBillet(true);
        break;
      default:
        setButtonCard(true);
        break;
    }
  };

  function handleSubmitFunc(data) {
    console.log(data);
  }
  function phoneMaskFunction(value, input) {
    const formatado = phoneMask(value);
    setValue(input, formatado);
  }
  function cpfMaskFunction(value) {
    const formatado = cpfMask(value);
    setValue('cpf', formatado);
  }
  function cepMaskFunction(value) {
    const formatado = cepMask(value);
    setValue('cep', formatado);
  }
  function onlyNumberMaskFunction(value, input) {
    const formatado = onlyNumber(value);
    setValue(input, formatado);
  }
  function numberMaskCVVFunction(value, input) {
    const formatado = numberCVV(value);
    setValue(input, formatado);
  }

  if (activeLoading) {
    return <S.ImageLoadding />;
  }
  // http://www.buscacep.correios.com.br/sistemas/buscacep/"
  return (
    <S.Container>
      <S.Header>
        <S.Logo>
          <S.ImageLogoEver />
        </S.Logo>
        <S.Login>
          <a href="https://portal.everbynature.com.br/">
            <S.IconUser />
            <span>Acessar minha conta</span>
          </a>
        </S.Login>
      </S.Header>

      <S.Banner />

      <S.Separator>
        <div className="discount">
          <S.IconPercentImg />
          <h4>20% DESCONTO</h4>
        </div>

        <div className="discount">
          <S.IconCardImg />
          <h4>ATÉ 5X SEM JUROS</h4>
        </div>
      </S.Separator>

      <S.Content>
        <S.ListProducts>
          <S.TableContainer>
            <S.Theader>
              <S.Th>Produto</S.Th>
              <S.Th>Descrição</S.Th>
              <S.Th>Preço Unit</S.Th>
              <S.Th>Quantidade</S.Th>
              <S.Th>SubTotal</S.Th>
            </S.Theader>

            <S.Tbody>
              {products.map((item) => (
                <S.TableLine key={item.id}>
                  <S.TableCell>
                    <img src={item.image.url} alt="Produto" />
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>{item.name}</S.Cell>
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>{item.price}</S.Cell>
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>2</S.Cell>
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>{item.price * 2}</S.Cell>
                  </S.TableCell>
                </S.TableLine>
              ))}
            </S.Tbody>
          </S.TableContainer>

          <div>
            <S.ContainerResults>
              <S.CepContainer>
                <div>
                  <p>Calcular o frete:</p>
                </div>
                <div>
                  <input type="text" name="cep" id="cep" />
                </div>
                <div>
                  <button type="button">Calcular</button>
                </div>
                <div>
                  <a
                    href="https://buscacepinter.correios.com.br/app/endereco/index.php"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    Não sei o CEP
                  </a>
                </div>
              </S.CepContainer>
              <S.Results>
                <S.LineResult>
                  <p>SubTotal:</p>
                  <p>R$ {subTotal.toFixed(2)}</p>
                </S.LineResult>

                <S.LineResult>
                  <p>Frete:</p>
                  <p>R$ 100,00</p>
                </S.LineResult>

                <S.LineResult>
                  <p>Desconto:</p>
                  <p>R$ {desconto.toFixed(2)}</p>
                </S.LineResult>
              </S.Results>
            </S.ContainerResults>
            <S.SumContainer>
              <p>Total:</p>
              <p>{total.toFixed(2)}</p>
            </S.SumContainer>
          </div>
        </S.ListProducts>

        <S.ContainerForms onSubmit={handleSubmit(handleSubmitFunc)}>
          <S.FormsData>
            <S.FormDataPersonal>
              <S.HeaderForm>
                <h2>Identificação</h2>
              </S.HeaderForm>

              <S.ContentForm>
                <S.ContainerInput>
                  <S.TitleInput>Nome Completo</S.TitleInput>
                  <S.InputComponent
                    id="name"
                    name="name"
                    type="name"
                    variant="default"
                    placeholder=""
                    register={register}
                    error={errors.name}
                    errorMessage={errors.name}
                  />
                </S.ContainerInput>

                <S.ContainerInput>
                  <S.TitleInput>E-mail</S.TitleInput>
                  <S.InputComponent
                    id="email"
                    name="email"
                    type="email"
                    variant="default"
                    placeholder=""
                    register={register}
                    error={errors.email}
                    errorMessage={errors.email}
                  />
                </S.ContainerInput>

                <S.ButtonGroup>
                  <S.ContainerInput>
                    <S.TitleInput>CPF</S.TitleInput>
                    <S.InputComponent
                      id="cpf"
                      name="cpf"
                      type="text"
                      variant="default"
                      placeholder="000.000.000-00"
                      register={register}
                      error={errors.cpf}
                      errorMessage={errors.cpf}
                      onChange={(e) => cpfMaskFunction(e.target.value)}
                    />
                  </S.ContainerInput>

                  <S.ContainerInput>
                    <S.TitleInput>Data de Nascimento</S.TitleInput>
                    <S.InputComponent
                      id="birthday"
                      name="birthday"
                      type="date"
                      variant="default"
                      placeholder="00/00/0000"
                      register={register}
                      error={errors.birthday}
                      errorMessage={errors.birthday}
                    />
                  </S.ContainerInput>
                </S.ButtonGroup>

                <S.ButtonGroup>
                  <S.ContainerInput>
                    <S.TitleInput>CEP</S.TitleInput>
                    <S.InputComponent
                      id="cep"
                      name="cep"
                      type="text"
                      variant="default"
                      placeholder="00 000 000"
                      register={register}
                      error={errors.cep}
                      errorMessage={errors.cep}
                      onChange={(e) => cepMaskFunction(e.target.value)}
                    />
                  </S.ContainerInput>
                  <S.ContainerInput>
                    <S.TitleInput>Telefone</S.TitleInput>
                    <S.InputComponent
                      id="phone"
                      name="phone"
                      type="text"
                      variant="default"
                      placeholder="+55 (00) 9 0000-0000"
                      register={register}
                      error={errors.phone}
                      errorMessage={errors.phone}
                      onChange={(e) => phoneMaskFunction(e.target.value, 'phone')}
                    />
                  </S.ContainerInput>
                </S.ButtonGroup>
              </S.ContentForm>
            </S.FormDataPersonal>

            <S.FormDataAddress>
              <S.HeaderForm>
                <h2>Endereço de Entrega</h2>
              </S.HeaderForm>

              <S.ContentForm>
                <S.ContainerInput>
                  <S.TitleInput>Endereço</S.TitleInput>
                  <S.InputComponent
                    id="address"
                    name="address"
                    type="text"
                    variant="default"
                    placeholder=""
                    register={register}
                    error={errors.address}
                    errorMessage={errors.address}
                  />
                </S.ContainerInput>

                <S.ButtonGroup className="input-number-address">
                  <S.ContainerInput>
                    <S.TitleInput>Número</S.TitleInput>
                    <S.InputComponent
                      id="number"
                      name="number"
                      type="text"
                      variant="default"
                      placeholder=""
                      register={register}
                      error={errors.number}
                      errorMessage={errors.number}
                      onChange={(e) => onlyNumberMaskFunction(e.target.value, 'number')}
                    />
                  </S.ContainerInput>

                  <S.ContainerInput>
                    <S.TitleInput>Bairro</S.TitleInput>
                    <S.InputComponent
                      id="neighborhood"
                      name="neighborhood"
                      type="text"
                      variant="default"
                      placeholder=""
                      register={register}
                      error={errors.neighborhood}
                      errorMessage={errors.neighborhood}
                    />
                  </S.ContainerInput>
                </S.ButtonGroup>

                <S.ContainerInput>
                  <S.TitleInput>Complemento</S.TitleInput>
                  <S.InputComponent
                    id="complement"
                    name="complement"
                    type="text"
                    variant="default"
                    placeholder=""
                    register={register}
                    error={errors.complement}
                    errorMessage={errors.complement}
                  />
                </S.ContainerInput>

                <S.ButtonGroup>
                  <S.ContainerInput>
                    <S.TitleInput>Estado</S.TitleInput>
                    <S.SelectComponent
                      id="state"
                      name="state"
                      variant="primary"
                      placeholder="Selecione o estado."
                      options={stateList}
                      defaultValue="Estatdo"
                      register={register}
                      error={errors.state}
                      errorMessage={errors.state}
                    />
                  </S.ContainerInput>
                  <S.ContainerInput>
                    <S.TitleInput>Cidade</S.TitleInput>
                    <S.InputComponent
                      id="city"
                      name="city"
                      type="text"
                      variant="default"
                      placeholder=""
                      register={register}
                      error={errors.city}
                      errorMessage={errors.city}
                    />
                  </S.ContainerInput>
                </S.ButtonGroup>
              </S.ContentForm>
            </S.FormDataAddress>
          </S.FormsData>

          <S.FormCardBillet>
            <S.HeaderForm>
              <h2>Cartão de credito</h2>
            </S.HeaderForm>

            <S.PagamentosOps>
              <S.ButtonCard
                className={buttonCard ? 'active' : ''}
                onClick={() => handleSelecteOp('card')}
                type="button"
              >
                <span>Cartão de Crédito</span>
              </S.ButtonCard>
              <S.ButtonBillet
                className={buttonBillet ? 'active' : ''}
                onClick={() => handleSelecteOp('billet')}
                type="button"
              >
                <span>Boleto</span>
              </S.ButtonBillet>
            </S.PagamentosOps>

            {buttonCard && (
              <S.ContentForm>
                <S.ContainerInput>
                  <S.TitleInput>Numero do Cartão</S.TitleInput>
                  <S.InputComponent
                    id="numberCard"
                    name="numberCard"
                    type="text"
                    variant="default"
                    placeholder=""
                    register={register}
                    error={errors.numberCard}
                    errorMessage={errors.numberCard}
                    onChange={(e) => onlyNumberMaskFunction(e.target.value, 'numberCard')}
                  />
                </S.ContainerInput>

                <S.ContainerInput>
                  <S.TitleInput>Nome do Titular do Cartão</S.TitleInput>
                  <S.InputComponent
                    id="nameHolder"
                    name="nameHolder"
                    type="text"
                    variant="default"
                    placeholder=""
                    register={register}
                    error={errors.nameHolder}
                    errorMessage={errors.nameHolder}
                  />
                </S.ContainerInput>

                <S.ButtonGroupCard className="input-numbers-card">
                  <S.ButtonGroup>
                    <S.ContainerInput>
                      <S.TitleInput>Mês</S.TitleInput>
                      <S.SelectComponent
                        id="month"
                        name="month"
                        width="80px"
                        variant="primary"
                        placeholder="Mês"
                        options={monthList}
                        defaultValue="Mês"
                        register={register}
                        error={errors.month}
                        errorMessage={errors.month}
                      />
                    </S.ContainerInput>
                    <S.ContainerInput>
                      <S.TitleInput>Ano</S.TitleInput>
                      <S.SelectComponent
                        id="year"
                        name="year"
                        width="80px"
                        variant="primary"
                        placeholder="Ano"
                        options={yearList}
                        defaultValue="Ano"
                        register={register}
                        error={errors.year}
                        errorMessage={errors.year}
                      />
                    </S.ContainerInput>
                  </S.ButtonGroup>

                  <S.ContainerInput>
                    <S.TitleInput>Código de Segurança</S.TitleInput>
                    <S.InputComponent
                      id="numberCVV"
                      name="numberCVV"
                      width="100%"
                      type="text"
                      variant="default"
                      placeholder=""
                      register={register}
                      error={errors.numberCVV}
                      errorMessage={errors.numberCVV}
                      onChange={(e) => numberMaskCVVFunction(e.target.value, 'numberCVV')}
                    />
                  </S.ContainerInput>
                </S.ButtonGroupCard>

                <S.ContainerInput>
                  <S.TitleInput>Número de parcelas</S.TitleInput>
                  <S.SelectComponent
                    id="numberOfInstallments"
                    name="numberOfInstallments"
                    width="100%"
                    variant="primary"
                    placeholder=""
                    options={numberOfInstallmentsList}
                    defaultValue="Escolha o parcelamento"
                    register={register}
                    error={errors.numberOfInstallments}
                    errorMessage={errors.numberOfInstallments}
                  />
                </S.ContainerInput>

                <hr />

                <button type="submit">Fechar Pedido</button>
              </S.ContentForm>
            )}

            {buttonBillet && (
              <S.ContentFormBillet>
                <h4>Atente-se aos detalhes:</h4>
                <article>
                  <ul>
                    <li>O boleto pode ser pago em qualquer agência bancária</li>
                    <li>O banco nos enviará automaticamente a confirmação do pagamento em até 3 dias úteis</li>
                    <li>
                      Se o boleto não for pago até a data de vencimento (Nos casos de vencimento em final de semana ou
                      feriado, no próximo dia útil), seu pedido será cancelado automaticamente{' '}
                    </li>
                    <li>
                      Depois do pagamento , verifique seu e-mail para receber mais informações sobre seu pedido,
                      verifique também a caixa de spam.
                    </li>
                  </ul>
                </article>

                <hr />

                <button type="submit">Gerar Boleto</button>
              </S.ContentFormBillet>
            )}

            <S.ImageSecurity />
          </S.FormCardBillet>
        </S.ContainerForms>
      </S.Content>

      <S.Footer>
        <S.RodapeCheckout />
        {/* <S.ContentInfoFooter>
          <p>
            EVER by Nature ® é uma marca registrada de EVER E-COMMERCE, IMPORTAÇÃO E EXPORTAÇÃO EIRELI |
            CNPJ30.816.903/0001-06
            <span> | </span>
            <a
              href="https://everbynature.com.br/politica/politica_de_devolucao_e_troca/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Política de Devolução e Troca
            </a>
            <span> | </span>
            <a
              href="https://everbynature.com.br/politica/politica_de_privacidade/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Política de Privacidade
            </a>
            <span> | </span>
            <a href="https://everbynature.com.br/politica/termos_de_uso/" rel="noopener noreferrer" target="_blank">
              Termos de uso
            </a>
            <span> | </span>
            Os preços anunciados neste site via e-mail ou qualquer outra ferramenta de marketing podem ser alterados sem
            prévio aviso.
          </p>
        </S.ContentInfoFooter> */}
      </S.Footer>
    </S.Container>
  );
};

export default CheckoutDynamic;
