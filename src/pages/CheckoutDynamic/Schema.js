import * as Yup from 'yup';

import { validCpf } from '../../utils/validators';

const Schema = Yup.object().shape({
  name: Yup.string()
    .required('Digite seu nome.')
    .test({
      name: 'name',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.test(value);

        if (!onlyText) {
          return this.createError({
            message: 'Não pode conter números.',
            path: 'name',
          });
        }
        return true;
      },
    }),
  email: Yup.string().email().required('Por favor, digite seu email.'),
  cpf: Yup.string()
    .required('Digite um CPF válido.')
    .test('testCpf', 'Digite um cpf válido', (value) => validCpf(value)),
  birthday: Yup.string()
    .required('Selecione uma data.')
    .default(() => new Date()),
  cep: Yup.string().required('Digite um cep válido.'),
  phone: Yup.string().required('Digite um telefone válido.'),
  address: Yup.string().required('Digite seu endereço.'),
  number: Yup.string().required('Digite seu número.'),
  neighborhood: Yup.string().required('Digite seu bairro.'),
  complement: Yup.string().required('Digite o complemento.'),
  state: Yup.string().required('Selecione um estado.'),
  city: Yup.string().required('Preencha o campo.'),
  numberCard: Yup.string().required('Digite seu número.'),
  nameHolder: Yup.string()
    .required('Digite o nome do titular.')
    .test({
      name: 'nameHolder',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.test(value);

        if (!onlyText) {
          return this.createError({
            message: 'Não pode conter números.',
            path: 'nameHolder',
          });
        }
        return true;
      },
    }),
  month: Yup.string().required('Preencha o campo corretamente.'),
  year: Yup.string().required('Preencha o campo corretamente.'),
  numberCVV: Yup.string().required('Preencha corretamente.'),
});

export default Schema;
