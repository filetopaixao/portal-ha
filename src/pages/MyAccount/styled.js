import styled, { css } from 'styled-components';

import { Container } from '@components/atoms/Container/styled';

import IconCamera from '@assets/images/icon_camera.svg';
import IconClose from '@assets/images/close.svg';
import IconAlert from '@assets/images/alert.svg';
import IconWhatsapp from '@assets/images/whatsapp_icon.svg';
import IconInstagram from '@assets/images/instagram_icon.svg';
import IconFacebook from '@assets/images/facebook_icon.svg';
import IconYoutube from '@assets/images/youtube_icon.svg';
import BgBusinessCard from '@assets/images/bg_business_card.svg';
import Button from '@components/atoms/Button/styled';
import Confirm from '@assets/images/confirm.svg';

const status = {
  approved: () => css`
    background: #2fdf46;
  `,
  pending: () => css`
    background: #f6b546;
  `,
  invalid: () => css`
    background: #f14479;
  `,
};

export const ContainerAccount = styled(Container)``;

export const ContainerButton = styled(Container)`
  display: flex;

  width: 100%;
  @media (min-width: 992px) {
    justify-content: flex-end;
  }
`;

export const ContainerProfile = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: auto;

  padding: 35px;

  border-radius: 15px;

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  flex: 100%;
  @media (min-width: 992px) {
    margin-right: 30px;

    flex: 35%;
  }
`;

export const Avatar = styled.div`
  width: 155px;
  height: 155px;

  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const UploadPhoto = styled.input`
  display: none;
`;

export const LbUploadPhoto = styled.label`
  width: 32px;
  height: 32px;

  border-radius: 50%;

  background: url(${IconCamera}) center center no-repeat #fff;

  position: relative;
  float: right;
  top: 125px;
  cursor: pointer;
`;

export const AvatarBC = styled.div`
  width: 155px;
  height: 155px;

  border: 3px solid #ffffff;
  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const PersonName = styled.div`
  margin-top: 18px;
`;

export const Office = styled.div`
  p {
    font-size: 15px;
    text-transform: uppercase;
    text-align: center;

    margin-bottom: 25px;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }
`;

export const ButtonAccount = styled(Button)`
  font-size: 12px;
  font-weight: 600;
  width: 100%;

  margin-bottom: 10px;
  padding: 10px 0px;

  border-radius: 15px;

  background-color: ${(props) => (props.primaryColor ? props.primaryColor : props.secondaryColor)};

  @media (min-width: 992px) {
    ${(props) => (props.size === '100%' ? 'width: 100%;' : 'width: 200px; position: relative; float: right;')}
  }
`;

export const CardAddress = styled.div`
  display: flex;

  width: 100%;
  height: auto;

  margin-bottom: 10px;

  border-radius: 10px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  overflow: hidden;
  .div-content-card {
    font-size: 13px;

    width: 100%;

    padding: 10px 20px;

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const RemoveAddress = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  flex-direction: column;
  width: 20px;

  background: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  cursor: pointer;
`;

export const RemoveIcon = styled.div`
  -webkit-mask-image: url(${IconClose});
  mask-image: url(${IconClose});
  mask-repeat: no-repeat;
  mask-size: 10px;
  mask-position: center;
  background-color: ${(props) => (props.theme === 'Dark' ? '#222933' : '#82868D')};
  width: 100%;
  height: 100%;
`;

export const Alert = styled.div`
  font-size: 10px;

  display: flex;

  margin: 10px 0 10px 0;

  color: ${(props) => props.primaryColor};
`;

export const AlertIcon = styled.div`
  padding: 0px 20px;

  -webkit-mask-image: url(${IconAlert});
  mask-image: url(${IconAlert});
  mask-repeat: no-repeat;
  mask-size: 23px;
  background-color: ${(props) => props.primaryColor};
  width: 23px;
  height: 23px;
`;

export const Status = styled.div`
  font-size: 10px;
  text-align: center;
  text-transform: uppercase;

  display: flex;
  align-items: center;
  justify-content: center;

  width: 133px;
  height: 22px;

  ${(props) => status[props.status]}
  color: #fff;

  box-shadow: 0px 1px 5px #00000029;
  opacity: 1;
`;

export const ConfirmIcon = styled.div`
  width: 150px;
  height: 150px;

  background: url(${Confirm}) center center no-repeat;
  background-size: cover;

  @media (min-width: 992px) {
    width: 200px;
    height: 200px;
  }
`;

export const ContainerModal = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;

  margin-bottom: 30px;
`;

export const ContentModal = styled.div`
  width: 100%;
`;

export const ContainerBusinessCard = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;

  margin-bottom: 20px;
`;

export const ContentBC = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;
`;

export const BusinessCard = styled.main`
  width: 280px;
  height: auto;

  padding: 20px;

  //background: url(${BgBusinessCard}) no-repeat;
  background: ${(props) => (props.theme === 'Dark' ? '#111A21' : '#F5F6FA')};

  background-position: 0px -45px;
  background-size: 100%;

  box-shadow: 0px 6px 19px #00000029;
  p {
    font-size: 12px;
  }
`;

export const H3 = styled.h3`
  text-align: center;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const H5 = styled.h5`
  text-align: center;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const P = styled.p`
  font-size: 13px;
  text-align: center;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const LogoStyle = styled.img`
  width: 130px;
  height: 50px;

  background-size: contain;
`;

export const ContainerCenter = styled.div`
  display: flex;
  justify-content: center;
`;

export const ContactBC = styled.div`
  display: flex;
  justify-content: flex-start;

  max-width: 150px;

  margin: 0 auto;
  margin-bottom: 5px;

  .whatsapp-icon {
    -webkit-mask-image: url(${IconWhatsapp});
    mask-image: url(${IconWhatsapp});
    mask-position: left;
    mask-repeat: no-repeat;
    mask-size: 13px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
    width: 13px;
  }

  .instagram-icon {
    -webkit-mask-image: url(${IconInstagram});
    mask-image: url(${IconInstagram});
    mask-position: left;
    mask-repeat: no-repeat;
    mask-size: 13px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
    width: 13px;
  }

  .facebook-icon {
    -webkit-mask-image: url(${IconFacebook});
    mask-image: url(${IconFacebook});
    mask-position: left;
    mask-repeat: no-repeat;
    mask-size: 13px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
    width: 13px;
  }

  .youtube-icon {
    -webkit-mask-image: url(${IconYoutube});
    mask-image: url(${IconYoutube});
    mask-position: left;
    mask-repeat: no-repeat;
    mask-size: 13px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
    width: 13px;
  }
`;

export const Whatsapp = styled.div`
  font-size: 9px;

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  //background: url(${IconWhatsapp}) center center no-repeat;
  //background-position: left;
`;

export const Instagram = styled.div`
  font-size: 9px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  //background: url(${IconInstagram}) center center no-repeat;
  background-position: left;
`;

export const Facebook = styled.div`
  font-size: 9px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  //background: url(${IconFacebook}) center center no-repeat;
  background-position: left;
`;

export const Youtube = styled.div`
  font-size: 9px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  //background: url(${IconYoutube}) center center no-repeat;
  background-position: left;
`;

export const Link = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;

  margin: 10px 0 -25px 0;
`;

export const ContainerChangePass = styled.div`
  padding: 20px;
`;

export const ContainerBtns = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media (min-width: 992px) {
    flex-direction: row;

    padding: 0 20px;
  }
`;

export const ContainerBtn = styled.div`
  @media (min-width: 992px) {
    width: 50%;

    padding: 0 20px;
  }
`;
