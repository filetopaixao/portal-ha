import React from 'react';

import ContactForm from '@components/molecules/Forms/MyAccount/Contact/';

import * as S from './styled';

const Contact = () => {
  return (
    <S.ContainerAccount>
      <ContactForm />
    </S.ContainerAccount>
  );
};

export default Contact;
