import React, { useState, useCallback, useEffect } from 'react';

import Filter from '@components/molecules/Filter';
import Select from '@components/molecules/Filter/select';
import Pagination from '@components/molecules/Pagination';
import ModalConfirm from '@components/molecules/Modals/ModalConf';
import Success from '@components/molecules/Modals/ModalConfSuccess';

import getInfo from '@services/myPartners';

import Arrow from '@assets/images/arrowCheckout.svg';

import * as S from './styled';

const MeusParceiros = () => {
  const [clients, setClients] = useState([]);

  const options = [
    {
      label: 'profissionais da saúde',
      value: 'profissionais da saúde',
    },
    {
      label: 'Profissionais fitness',
      value: 'Profissionais fitness',
    },
    {
      label: 'Profissionais da beleza',
      value: 'Profissionais da beleza',
    },
    {
      label: 'Revendedores online',
      value: 'Revendedores online',
    },
    {
      label: 'revendedores offline',
      value: 'revendedores offline',
    },
  ];

  const [filterText, setFilterText] = useState('');
  const [filterSelect, setFilterSelect] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const [titleSuccess, setTitleSuccess] = useState('');
  const [textSuccess, setTextSuccess] = useState('');
  const [isVisibleConfirm, setIsVisibleConfirm] = useState(false);
  const [isVisibleSuccess, setIsVisibleSuccess] = useState(false);

  const [activeLoading, setActiveLoading] = useState(true);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleActionNotApproved = () => {
    setIsVisibleConfirm(!isVisibleConfirm);
  };

  const handleActionApproved = () => {
    setTitleSuccess('Parceiro aprovado com sucesso!');
    setTextSuccess('As informações do novo parceiro estarão disponíveis na aba "Meus parceiros"');
    setIsVisibleSuccess(!isVisibleSuccess);
  };

  const handleConfirmRemove = () => {
    setIsVisibleConfirm(!isVisibleConfirm);
    setTitleSuccess('Parceiro removido com sucesso!');
    setTextSuccess('');
    setIsVisibleSuccess(!isVisibleSuccess);
    console.log('removeu');
  };

  const handleDesativeConfSuccess = () => {
    setIsVisibleSuccess(!isVisibleSuccess);
  };

  useEffect(() => {
    getInfo()
      .then((response) => {
        if (response.data.data) {
          const serializedClients = response.data.data.map((client) => {
            return {
              id: client.id,
              category: client.category,
              acting: client.acting,
              name: client.name,
              email: client.email,
              phone: client.phone,
              cityUf: `Palmas/TO`,
              solicitation: client.solicitation,
              percent: client.percent ? client.percent : 0,
              activeExpansed: false,
              action: '',
            };
          });
          setClients(serializedClients);
        }

        setActiveLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const filteredItems = clients.filter(
    (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentClients = filteredItems.slice(indexOfFirst, indexOfLast);

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    console.log('clients', clients);
    const newDatas = clients.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setClients(newDatas);
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <S.Container>
      <S.FilterContainer>
        <Select
          options={options}
          onChange={(e) => {
            setFilterSelect(e.target.value);
          }}
          defaultValue={filterSelect}
        />
        <Filter onFilter={(e) => setFilterText(e.target.value)} filterText={filterText} />
      </S.FilterContainer>
      <S.ContainerDataTable>
        <S.TableHeader>
          <S.TextCell className="text-upp" />
          <S.TextCell className="text-upp">Área de Atuação</S.TextCell>
          <S.TextCell className="text-upp off-1">Nome</S.TextCell>
          <S.TextCell className="text-upp off-1">Email</S.TextCell>
          <S.TextCell className="text-upp off-2">Telefone</S.TextCell>
          <S.TextCell className="text-upp off-3">Cidade/Estado</S.TextCell>
          <S.TextCell className="text-upp off-3">Solicitação</S.TextCell>
          <S.TextCell className="text-upp" />
          <S.TextCell className="text-upp btn-expansed" />
        </S.TableHeader>
        <S.TableBody>
          <>
            {currentClients.length <= 0 ? (
              <S.ContainerBackEndInfo>
                <S.InfoBackEnd>Não há parceiros cadastrados</S.InfoBackEnd>
                <S.ImageNotFound />
              </S.ContainerBackEndInfo>
            ) : (
              <>
                {currentClients.map((item) => (
                  <>
                    <S.TableLine>
                      <S.TableCell className="text-upp">
                        <S.ContainerAction>
                          <S.Acting acting={item.category} />
                        </S.ContainerAction>
                      </S.TableCell>
                      <S.TableCell className="text-upp">{item.acting}</S.TableCell>
                      <S.TableCell className="text-upp off-1">{item.name}</S.TableCell>
                      <S.TableCell className="text-upp off-1">{item.email}</S.TableCell>
                      <S.TableCell className="text-upp off-2">{item.phone}</S.TableCell>
                      <S.TableCell className="text-upp off-3">{item.cityUf}</S.TableCell>
                      <S.TableCell className="text-upp off-3">{item.solicitation}</S.TableCell>
                      <S.TableCell className="text-upp">
                        <S.ButtonGroup>
                          <S.ActionApproved onClick={() => handleActionApproved()} variant="">
                            aprovar
                          </S.ActionApproved>
                          <S.Separator>|</S.Separator>
                          <S.ActionNotApproved onClick={() => handleActionNotApproved()} variant="">
                            reprovar
                          </S.ActionNotApproved>
                        </S.ButtonGroup>
                      </S.TableCell>
                      <S.TableCell className="text-upp off-button-set">
                        <S.Expansed href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                          <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                        </S.Expansed>
                      </S.TableCell>
                    </S.TableLine>
                    {item.activeExpansed ? (
                      <S.TableLineExpansed className="activeExpansed">
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>NOME</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.name}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>EMAIL</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.email}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>TELEFONE</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.phone}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>CIDADE/ESTADO</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.cityUf}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>SOLICITAÇÂO</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.solicitation}</p>
                          </div>
                        </S.TableCellExpansed>
                      </S.TableLineExpansed>
                    ) : null}
                  </>
                ))}
              </>
            )}
          </>
        </S.TableBody>
      </S.ContainerDataTable>

      <Pagination currentPage={currentPage} perPage={perPage} total={clients.length} paginate={paginate} />

      <ModalConfirm
        exeFunctionAction={handleConfirmRemove}
        isModalDelConf={isVisibleConfirm}
        setIsModalDelConf={setIsVisibleConfirm}
        msg1="Tem certeza que deseja reprovar esse parceiro?"
      />
      <Success
        functionExecute={handleDesativeConfSuccess}
        isModalConfSuccess={isVisibleSuccess}
        setIsModalConfSuccess={setIsVisibleSuccess}
        msg1={titleSuccess}
        msg2={textSuccess}
      />
    </S.Container>
  );
};

export default MeusParceiros;
