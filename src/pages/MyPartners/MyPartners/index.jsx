import React, { useState, useCallback, useEffect } from 'react';

import Filter from '@components/molecules/Filter';
import Select from '@components/molecules/Filter/select';
import Pagination from '@components/molecules/Pagination';
import Modal from '@components/molecules/Modals/ModalComponent';
import ModalConfirm from '@components/molecules/Modals/ModalConf';
import Success from '@components/molecules/Modals/ModalConfSuccess';
import Button from '@components/atoms/Button';

import IconAdd from '@assets/images/icon_add.svg';
import AvatarTest from '@assets/images/profile.png';
import Trash from '@assets/images/trash_icon_grey.png';
import Arrow from '@assets/images/arrowCheckout.svg';

import getInfo from '@services/myPartners';

import * as S from './styled';

const MeusParceiros = () => {
  const [clients, setClients] = useState([]);

  const options = [
    {
      label: 'profissionais da saúde',
      value: 'profissionais da saúde',
    },
    {
      label: 'Profissionais fitness',
      value: 'Profissionais fitness',
    },
    {
      label: 'Profissionais da beleza',
      value: 'Profissionais da beleza',
    },
    {
      label: 'Revendedores online',
      value: 'Revendedores online',
    },
    {
      label: 'revendedores offline',
      value: 'revendedores offline',
    },
  ];

  const [filterText, setFilterText] = useState('');
  const [filterSelect, setFilterSelect] = useState('');
  const [currentPage, setCurrentPage] = useState(1);

  const [perPage] = useState(10);
  const [isVisible, setIsVisible] = useState(false);
  const [isVisibleConfirm, setIsVisibleConfirm] = useState(false);
  const [isVisibleSuccess, setIsVisibleSuccess] = useState(false);
  const [clickedUser, setClickedUser] = useState({});

  const [activeLoading, setActiveLoading] = useState(true);

  useEffect(() => {
    getInfo()
      .then((response) => {
        if (response.data.data) {
          const serializedClients = response.data.data.map((client) => {
            return {
              id: client.id,
              category: client.category,
              acting: client.acting,
              name: client.name,
              email: client.email,
              phone: client.phone,
              city: client.city,
              uf: client.uf,
              cityUf: `Palmas/TO`,
              solicitation: client.solicitation,
              percent: client.percent ? client.percent : 0,
              activeExpansed: false,
              action: '',
            };
          });
          setClients(serializedClients);
        }

        setActiveLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const handleAction = useCallback(
    (client) => {
      setClickedUser(client);
      setIsVisible(!isVisible);
    },
    [isVisible]
  );

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    console.log('clients', clients);
    const newDatas = clients.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setClients(newDatas);
  };

  const handleConfirmRemove = () => {
    setIsVisibleConfirm(!isVisibleConfirm);
    setIsVisibleSuccess(!isVisibleSuccess);
    setIsVisible(!isVisible);
  };

  const handleDesativeConfSuccess = () => {
    setIsVisibleSuccess(!isVisibleSuccess);
  };

  const filteredItems = clients.filter(
    (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentClients = filteredItems.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <S.Container>
      <S.FilterContainer>
        <Select
          options={options}
          onChange={(e) => {
            setFilterSelect(e.target.value);
          }}
          defaultValue={filterSelect}
        />
        <Filter onFilter={(e) => setFilterText(e.target.value)} filterText={filterText} />
      </S.FilterContainer>
      <S.ContainerDataTable>
        <S.TableHeader>
          <S.TextCell className="text-upp" />
          <S.TextCell className="text-upp">Área de Atuação</S.TextCell>
          <S.TextCell className="text-upp off-1">Nome</S.TextCell>
          <S.TextCell className="text-upp off-1">Email</S.TextCell>
          <S.TextCell className="text-upp off-2">Telefone</S.TextCell>
          <S.TextCell className="text-upp off-3">Cidade/Estado</S.TextCell>
          <S.TextCell className="text-upp off-3">%Empreendedor</S.TextCell>
          <S.TextCell className="text-upp" />
          <S.TextCell className="text-upp btn-expansed" />
        </S.TableHeader>
        <S.TableBody>
          <>
            {currentClients.length <= 0 ? (
              <S.ContainerBackEndInfo>
                <S.InfoBackEnd>Não há parceiros cadastrados</S.InfoBackEnd>
                <S.ImageNotFound />
              </S.ContainerBackEndInfo>
            ) : (
              <>
                {currentClients.map((item) => (
                  <>
                    <S.TableLine>
                      <S.TableCell className="text-upp">
                        <S.ContainerAction>
                          <S.Acting acting={item.category} />
                        </S.ContainerAction>
                      </S.TableCell>
                      <S.TableCell className="text-upp">{item.acting}</S.TableCell>
                      <S.TableCell className="text-upp off-1">{item.name}</S.TableCell>
                      <S.TableCell className="text-upp off-1">{item.email}</S.TableCell>
                      <S.TableCell className="text-upp off-2">{item.phone}</S.TableCell>
                      <S.TableCell className="text-upp off-3">{item.cityUf}</S.TableCell>
                      <S.TableCell className="text-upp off-3">{item.percent}</S.TableCell>
                      <S.TableCell className="text-upp">
                        <S.ButtonGroup>
                          <Button onClick={() => handleAction(item)} variant="">
                            <img src={IconAdd} alt="Editar" />
                          </Button>
                        </S.ButtonGroup>
                      </S.TableCell>
                      <S.TableCell className="text-upp off-button-set">
                        <S.Expansed href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                          <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                        </S.Expansed>
                      </S.TableCell>
                    </S.TableLine>
                    {item.activeExpansed ? (
                      <S.TableLineExpansed className="activeExpansed">
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>NOME</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.name}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>EMAIL</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.email}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>TELEFONE</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.phone}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>CIDADE/ESTADO</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.cityUf}</p>
                          </div>
                        </S.TableCellExpansed>
                        <S.TableCellExpansed className="text-upp">
                          <S.TitleValueExpan>
                            <p>EMPREENDEDOR</p>
                          </S.TitleValueExpan>
                          <div>
                            <p>{item.percent}</p>
                          </div>
                        </S.TableCellExpansed>
                      </S.TableLineExpansed>
                    ) : null}
                  </>
                ))}
              </>
            )}
          </>
        </S.TableBody>
      </S.ContainerDataTable>

      <Pagination currentPage={currentPage} perPage={perPage} total={clients.length} paginate={paginate} />

      <Modal
        alignCenter
        ContentHeight="auto"
        MaxWidth="600px"
        title="Resultados parceria"
        isVisible={isVisible}
        onClose={() => setIsVisible(!isVisible)}
      >
        <S.ResultContainer>
          <S.AvatarContainer>
            <S.Avatar photo={clickedUser.photo ? clickedUser.photo : AvatarTest} />
          </S.AvatarContainer>
          <S.DataResultContainer>
            <S.ContainerInfo>
              <S.TitleInfo>vendas realizadas</S.TitleInfo>
              <S.Info>{clickedUser.vendas ? clickedUser.vendar : '00'}</S.Info>
            </S.ContainerInfo>
            <S.ContainerInfo>
              <S.TitleInfo>total em vendas</S.TitleInfo>
              <S.Info>
                R$
                {clickedUser.totalVendas ? clickedUser.totalVendas : '00,00'}
              </S.Info>
            </S.ContainerInfo>
            <S.ContainerInfo>
              <S.TitleInfo>ganhos com esse parceiro</S.TitleInfo>
              <S.Info>
                R$
                {clickedUser.ganhos ? clickedUser.ganhos : '00,00'}
              </S.Info>
            </S.ContainerInfo>
          </S.DataResultContainer>
        </S.ResultContainer>

        <Button variant="primary" fullSize>
          ver relatório detalhado
        </Button>
        <Button fullSize onClick={() => setIsVisibleConfirm(!isVisibleConfirm)}>
          <S.RemoveBtnContainer>
            <S.IconImg src={Trash} alt="trash" />
            <div>Remover Parceiro</div>
          </S.RemoveBtnContainer>
        </Button>
      </Modal>

      <ModalConfirm
        exeFunctionAction={handleConfirmRemove}
        isModalDelConf={isVisibleConfirm}
        setIsModalDelConf={setIsVisibleConfirm}
        msg1="Tem certeza que deseja reprovar esse parceiro?"
        msg2="Ao confirmar o parceiro será removido da sua plataforma"
      />
      <Success
        functionExecute={handleDesativeConfSuccess}
        isModalConfSuccess={isVisibleSuccess}
        setIsModalConfSuccess={setIsVisibleSuccess}
        msg1="Parceiro removido com sucesso!"
      />
    </S.Container>
  );
};

export default MeusParceiros;
