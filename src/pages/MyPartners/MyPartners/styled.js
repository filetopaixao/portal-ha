import styled, { css } from 'styled-components';

import PageLoadding from '@assets/images/page_loadding.gif';
import NotFound from '@assets/images/not-found.png';

const actings = {
  medico: () => css`
    background-color: #2845aa;
  `,
  educadorFisico: () => css`
    background-color: #2fdf46;
  `,
  massagista: () => css`
    background-color: #f14479;
  `,
  influenciador: () => css`
    background-color: #01a3ff;
  `,
  revendedor: () => css`
    background-color: #ff7f00;
  `,
  development: () => css`
    background-color: #5e5e5e;
  `,
};

export const Container = styled.div``;

export const FilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ActionApproved = styled.button`
  font-size: 13px;

  border: none;
  outline: none;

  background-color: transparent;
  color: #01a3ff;
  cursor: pointer;
`;

export const ActionNotApproved = styled(ActionApproved)`
  color: #f14479;
`;

export const Separator = styled.p`
  margin-left: 5px;
  margin-right: 5px;
`;

export const ContainerDataTable = styled.div`
  width: 100%;

  margin: 20px 0;
  padding: 10px;
  border-radius: 15px;
  background: #fff;

  > p {
    font-size: 16px;
    font-weight: bold;
    color: #5e5e5e;
  }

  @media (min-width: 500px) {
    padding: 20px;
  }
`;

export const TableHeader = styled.div`
  text-align: center;

  display: grid;
  grid-template-columns: 0.3fr 1fr 1fr 1fr 1fr 1fr 1fr 0.3fr;

  @media (max-width: 1200px) {
    display: grid;
    grid-template-columns: 0.3fr 1fr 1fr 1fr 1fr 0.3fr;
  }

  @media (max-width: 1000px) {
    display: grid;
    grid-template-columns: 0.3fr 1fr 1fr 1fr 0.3fr;
  }

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 0.1fr 1fr 0.1fr 0.1fr;
  }

  margin-top: 15px;
  padding: 14px 0;

  border-bottom: 1px solid #dad2d2;

  p {
    font: normal normal normal 14px Poppins;
  }
`;

export const TextCell = styled.p`
  font-size: 12px;
  color: #5e5e5e;

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (max-width: 1200px) {
    &.off-3 {
      display: none;
    }
  }

  @media (max-width: 1000px) {
    &.off-2 {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const TableBody = styled.div`
  img {
    display: block;
    height: 100%;
    width: 20px !important;

    @media (min-width: 1400px) {
      height: 100%;
      width: 25px !important;
    }
  }
`;

export const TableLine = styled.div`
  text-align: center;

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  display: grid;
  grid-template-columns: 0.3fr 1fr 1fr 1fr 1fr 1fr 1fr 0.3fr;

  @media (max-width: 1200px) {
    display: grid;
    grid-template-columns: 0.3fr 1fr 1fr 1fr 1fr 0.3fr;
  }

  @media (max-width: 1000px) {
    display: grid;
    grid-template-columns: 0.3fr 1fr 1fr 1fr 0.3fr;
  }

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 0.1fr 1fr 0.1fr 0.1fr;
  }

  padding: 10px 0px;

  border-bottom: 1px solid #dad2d2;
`;

export const TableCell = styled.div`
  font-size: 12px;
  color: #5e5e5e;

  .toggle-switch {
    margin-left: -25px;
  }

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (min-width: 800px) {
    &.off-button-set {
      display: none;
    }
  }

  @media (max-width: 1200px) {
    &.off-3 {
      display: none;
    }
  }

  @media (max-width: 1000px) {
    &.off-2 {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;
export const TableCellExpansed = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;

  margin-bottom: 15px;

  p {
    font-size: 12px;
    text-transform: uppercase;
    color: #5e5e5e;
  }
`;

export const TableLineExpansed = styled.div`
  text-align: center;
  display: block;
  padding: 20px;

  transition: all cubic-bezier(0.175, 0.885, 0.32, 1.275);

  border-bottom: 1px solid #dad2d2;
  background: #f6f7f9;

  @media (min-width: 600px) {
    display: none;
  }

  &.activeExpansed {
    height: auto;
  }

  &.defaultExpansed {
    height: 0px;
  }
`;

export const ContainerPagination = styled.div`
  width: 100%;
`;

export const ButtonExpansed = styled.img`
  grid-area: 'close';

  width: 20px;
  height: 100%;
  margin: 0 auto;

  cursor: pointer;
  transform: rotate(180deg);
  transition: transform 300ms linear;

  ${(props) => (props.active ? 'transform: rotate(0);' : '')};
`;

export const TitleValueExpan = styled.div`
  p {
    text-align: left;
    font-weight: bold;
  }
`;

export const Expansed = styled.a``;

export const ButtonGroup = styled.div`
  display: flex;
  align-items: center;

  button {
    height: 17px;
    padding: 0;
    margin: 0 auto;
    font-size: 18px;
    text-transform: uppercase;
    text-align: center;
    position: relative;
    cursor: pointer;
    border-radius: 15px;
    outline: 0;
    color: #fff;
    background: transparent;
    border: none;
    box-shadow: none;
  }
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: #5e5e5e;
  font: normal normal normal 14px Poppins;
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ContainerAction = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Acting = styled.div`
  height: 15px;
  width: 15px;

  margin: 0 auto;

  border-radius: 50%;

  ${(props) => actings[props.acting]};
`;

export const TitleInfo = styled.h3`
  font-size: 14px;
  font-weight: 100;

  color: #fff;

  text-transform: uppercase;
`;

export const Info = styled.p`
  font-size: 25px;
  font-weight: 400;

  color: #fff;

  text-transform: uppercase;
`;

export const ContainerInfo = styled.div`
  margin-bottom: 30px;
`;

export const ResultContainer = styled.div`
  padding: 45px;
  border-radius: 20px;
  background-color: #01a3ff;
  margin-bottom: 10px;
  display: flex;
`;

export const Avatar = styled.div`
  width: 224px;
  height: 224px;

  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const AvatarContainer = styled.div`
  text-align: left;

  display: flex;
  align-items: center;

  width: 50%;
`;

export const DataResultContainer = styled.div`
  text-align: right;
  width: 50%;
`;

export const IconImg = styled.img`
  width: 10px;
`;

export const RemoveBtnContainer = styled.div`
  font-size: 10px;
  text-align: center;

  width: 100%;

  display: flex;
  justify-content: center;

  color: #cbcaca;
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
