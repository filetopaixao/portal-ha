import styled, { css } from 'styled-components';

import { Container as Ct } from '@components/atoms/Container/styled';
import { Button as Bt } from '@components/atoms/Button/styled';
import Text from '@components/atoms/Text';

import Medico from '@assets/images/medico.svg';
import Nutricionista from '@assets/images/nutricionista.svg';
import Terapeuta from '@assets/images/terapeuta.svg';
import PersonalTrainer from '@assets/images/personal_trainer.svg';
import EducadorFisico from '@assets/images/educador_fisico.svg';
import Academia from '@assets/images/academia.svg';
import SalaoBeleza from '@assets/images/salao_de_beleza.svg';
import Massagista from '@assets/images/massagista.svg';
import Esteticista from '@assets/images/esteticista.svg';
import Influenciador from '@assets/images/influenciador.svg';
import Afiliado from '@assets/images/afiliado.svg';
import Dropshipping from '@assets/images/dropshipping.svg';
import RevendedoresCosm from '@assets/images/revendedores_de_cosmeticos.svg';
import RevendedoresProdNat from '@assets/images/revendedores_de_produtos_naturais.svg';
import LojaFisica from '@assets/images/loja_fisica.svg';

const colors = {
  darkBlue: () => css`
    background-color: #2845aa;
  `,
  green: () => css`
    background-color: #2fdf46;
  `,
  pink: () => css`
    background-color: #f14479;
  `,
  blue: () => css`
    background-color: #01a3ff;
  `,
  orange: () => css`
    background-color: #ff7f00;
  `,
};

const icons = {
  no: () => css`
    background: none;
  `,
  medico: () => css`
    background: url(${Medico}) center center no-repeat;
  `,
  nutricionista: () => css`
    background: url(${Nutricionista}) center center no-repeat;
  `,
  terapeuta: () => css`
    background: url(${Terapeuta}) center center no-repeat;
  `,
  personalTrainer: () => css`
    background: url(${PersonalTrainer}) center center no-repeat;
  `,
  educadorFisico: () => css`
    background: url(${EducadorFisico}) center center no-repeat;
  `,
  academia: () => css`
    background: url(${Academia}) center center no-repeat;
  `,
  salaoBeleza: () => css`
    background: url(${SalaoBeleza}) center center no-repeat;
  `,
  massagista: () => css`
    background: url(${Massagista}) center center no-repeat;
  `,
  esteticista: () => css`
    background: url(${Esteticista}) center center no-repeat;
  `,
  influenciador: () => css`
    background: url(${Influenciador}) center center no-repeat;
  `,
  afiliado: () => css`
    background: url(${Afiliado}) center center no-repeat;
  `,
  dropshipping: () => css`
    background: url(${Dropshipping}) center center no-repeat;
  `,
  revendedoresCosm: () => css`
    background: url(${RevendedoresCosm}) center center no-repeat;
  `,
  revendedoresProdNat: () => css`
    background: url(${RevendedoresProdNat}) center center no-repeat;
  `,
  lojaFisica: () => css`
    background: url(${LojaFisica}) center center no-repeat;
  `,
};

export const Container = styled(Ct)``;

export const ContainerProfessional = styled(Ct)`
  margin-bottom: 30px;
`;

export const CommissionText = styled(Text)`
  font-size: 13px;

  padding-bottom: 5px;

  color: #fff;
`;

export const H3 = styled.h3`
  font-size: 15px;
  color: #5e5e5e;
  text-transform: uppercase;
  margin-bottom: 10px;
`;

export const ModalH3 = styled.h3`
  font-size: 14px;
  font-weight: 100;

  color: #fff;

  text-transform: uppercase;
`;

export const ModalData = styled.p`
  font-size: 25px;
  font-weight: 400;

  color: #fff;

  text-transform: uppercase;
`;

export const ModalDataCtn = styled.div`
  margin-bottom: 30px;
`;

export const ContentContainer = styled.main`
  width: 100%;

  display: block;

  border-radius: 20px;

  background: #ffffff 0% 0% no-repeat padding-box;
  @media (min-width: 992px) {
    padding: 20px 20px;
  }

  @media (min-width: 1600px) {
    padding: 20px 70px;
  }
`;

export const ContainerCards = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  align-items: center;

  min-height: 270px;

  @media (min-width: 992px) {
    grid-template-columns: 25% 25% 25% 25%;
  }
`;

export const Card = styled.div`
  margin: 0 15px;
  cursor: pointer;
`;

export const CardTitle = styled.h3`
  font-size: 15px;
  text-transform: uppercase;

  color: #ffffff;

  @media (min-width: 992px) {
    font-size: 20px;
  }
`;

export const SingleCard = styled.div`
  text-align: center;

  width: 100%;
  min-height: 235px;

  padding: 15px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  border-radius: 20px;

  ${(props) => colors[props.color]}
`;

export const NoIcon = styled.div`
  width: 80px;
  height: 80px;

  margin: 15px;
`;

export const CardIcon = styled.div`
  width: 80px;

  padding-bottom: 95px;

  ${(props) => icons[props.icon]}
  background-size: contain;
`;

export const IconMedico = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Medico}) center center no-repeat;
`;

export const IconNutricionista = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Nutricionista}) center center no-repeat;
`;

export const IconTerapeuta = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Terapeuta}) center center no-repeat;
`;

export const IconPersonalTrainer = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${PersonalTrainer}) center center no-repeat;
`;

export const IconEducadorFisico = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${EducadorFisico}) center center no-repeat;
`;

export const IconAcademia = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Academia}) center center no-repeat;
`;

export const IconSalaoBeleza = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${SalaoBeleza}) center center no-repeat;
`;

export const IconEsteticista = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Esteticista}) center center no-repeat;
`;

export const IconMassagista = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Massagista}) center center no-repeat;
`;

export const IconInfluenciador = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Influenciador}) center center no-repeat;
`;

export const IconAfiliado = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Afiliado}) center center no-repeat;
`;

export const IconDropshipping = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${Dropshipping}) center center no-repeat;
`;

export const IconRevendedoresCosm = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${RevendedoresCosm}) center center no-repeat;
`;

export const IconRevendedoresProdNat = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${RevendedoresProdNat}) center center no-repeat;
`;

export const IconLojaFisica = styled.div`
  width: 80px;
  height: 80px;

  padding-bottom: 95px;

  background: url(${LojaFisica}) center center no-repeat;
`;
export const H4 = styled.h4`
  text-transform: uppercase;
`;

export const ResultContainer = styled.div`
  padding: 45px;
  border-radius: 20px;
  background-color: #01a3ff;
  margin-bottom: 10px;
  display: flex;
`;

export const Avatar = styled.div`
  width: 224px;
  height: 224px;

  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const AvatarContainer = styled.div`
  text-align: left;

  display: flex;
  align-items: center;

  width: 50%;
`;

export const DataResultContainer = styled.div`
  text-align: right;
  width: 50%;
`;

export const IconImg = styled.img`
  width: 10px;
`;

export const RemoveBtnContainer = styled.div`
  font-size: 10px;
  text-align: center;

  width: 100%;

  display: flex;
  justify-content: center;

  color: #cbcaca;
`;

export const SimpleText = styled(Text)`
  font-size: 16px;

  color: #5e5e5e;
`;

export const InfoCtn = styled.div`
  ${(props) => (props.flex ? 'display:flex; align-items: center;' : null)};
  ${(props) => (props.noMargin ? null : 'margin-bottom: 30px;')};
  ${(props) => (props.marginRight ? 'margin-right: 30px;' : null)};
`;

export const FilterContainer = styled.div`
  display: flex;
`;
export const Action = styled.button`
  font-size: 13px;

  border: none;
  outline: none;

  background-color: transparent;
  color: ${(props) => (props.pink ? '#F14479' : '#01A3FF')};
  cursor: pointer;
`;

export const Separator = styled.p`
  margin-left: 5px;
  margin-right: 5px;
`;

export const GridButtons = styled.div`
  display: none;

  margin-bottom: 15px;

  @media (min-width: 992px) {
    display: grid;

    width: 100%;

    grid-template-columns: ${(props) => {
      const width = 100 / props.qtd;
      let grid = '';
      for (let i = 0; i < props.qtd; i += 1) {
        grid += `${width}% `;
      }
      return `${grid}`;
    }};
  }
`;

export const Column = styled.div`
  padding: 4px;

  button {
    padding: 0;
  }
`;

export const Button = styled(Bt)`
  font-size: 13px;
  width: 100%;

  &.btnGrid {
    height: 43px;
    border-radius: 10px;

    ${(props) => (props.active ? `background: #01a3ff; color: #fff;` : 'background: #fff; color: #5e5e5e')};
  }
`;

export const Content = styled.main``;
