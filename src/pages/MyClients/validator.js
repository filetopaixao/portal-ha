import * as Yup from 'yup';

export const DataPersonalValidation = Yup.object({
  name: Yup.string().test({
    name: 'validateName',
    test(value) {
      const onlyText = /^[a-zA-Z ]*$/.test(value);

      if (!onlyText) {
        return this.createError({
          message: 'não pode conter números',
          path: 'name',
        });
      }
      return true;
    },
  }),
  email: Yup.string().email('email inválido'),
  phone: Yup.string().test({
    name: 'validatePhone',
    test(value) {
      const rePhoneNumber = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/.test(
        value
      );

      if (!rePhoneNumber) {
        return this.createError({
          message: 'digite o numero corretamente',
          path: 'phone',
        });
      }
      return true;
    },
  }),
});

export default DataPersonalValidation;
