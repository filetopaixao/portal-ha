import React, { useState, useEffect, useCallback } from 'react';

import BannerEcommerce from '@components/molecules/Welcome/banner';
import MenuOpsFilterPrint from '@components/molecules/MenuOpsFilterPrint';
import Pagination from '@components/molecules/Pagination';
import ModalVideo from '@components/molecules/Modals/ModalVideo';
import Button from '@components/atoms/Button';

import { getInfo } from '@services/myClients';

import ConfirmationDelete from '@components/molecules/Modals/ModalConf';
import ConfirmationSuccess from '@components/molecules/Modals/ModalConfSuccess';

import Arrow from '@assets/images/arrowCheckout.svg';

import * as S from './styled';

function MyClients() {
  const linkEver = 'https://loja.everbynature.com.br?me=1';
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';
  const image = 'entrepreneursMobile';
  const BannerTitle = 'Link novo Empreendedor';

  const [filterText, setFilterText] = React.useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);

  const [selectedClient, setSelectedClient] = useState({});
  const [isModalDeleteVisible, setIsModalDeleteVisible] = useState(false);
  const [isModalConfSuccess, setIsModalConfSuccess] = useState(false);

  const [activeLoading, setActiveLoading] = useState(true);

  const handleDesativeConfSuccess = () => {
    console.log('Vocẽ excluiu o cliente', selectedClient);
    setIsModalDeleteVisible(!isModalDeleteVisible);
    setIsModalConfSuccess(!isModalConfSuccess);
  };

  const handleActiveConfSuccess = () => {
    setIsModalConfSuccess(!isModalConfSuccess);
  };

  const handleOpenModalDelete = (client) => {
    setSelectedClient(client);
    setIsModalDeleteVisible(!isModalDeleteVisible);
  };

  const [clients, setClients] = useState([]);

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    const newDatas = clients.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setClients(newDatas);
  };

  useEffect(() => {
    getInfo()
      .then((response) => {
        const dataClients = response.data.data;
        console.log(response.data, 'CLIENTES');
        if (dataClients) {
          const serializedClients = dataClients.map((client) => {
            return {
              id: client.id,
              name: client.name,
              email: client.email,
              phone: client.phone,
              cityUf: `${client.address.city}/${client.address.state}`,
              action: '',
              activeExpansed: false,
            };
          });
          setClients(serializedClients);
        }

        setActiveLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const filteredItems = clients.filter(
    (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentClients = filteredItems.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleOpenModalVideo = (op) => {
    setIsModalVideoVisible(op);
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.Container>
        <BannerEcommerce
          openModalVideo={handleOpenModalVideo}
          link={linkEver}
          image={image}
          imageMobile
          title={BannerTitle}
        />
        <MenuOpsFilterPrint filterText={filterText} setFilterText={setFilterText} />
        <S.ContainerDataTable id="print">
          <p>Clientes</p>
          <S.TableHeader>
            <S.TextCell className="text-upp">Nome</S.TextCell>
            <S.TextCell className="text-upp off-1">Email</S.TextCell>
            <S.TextCell className="text-upp">Telefone</S.TextCell>
            <S.TextCell className="text-upp off-3">Cidade/Estado</S.TextCell>
            <S.TextCell className="text-upp" />
            <S.TextCell className="text-upp btn-expansed" />
          </S.TableHeader>
          <S.TableBody>
            <>
              {currentClients.length <= 0 ? (
                <S.ContainerBackEndInfo>
                  <S.InfoBackEnd>Não há clientes cadastrados</S.InfoBackEnd>
                  <S.ImageNotFound />
                </S.ContainerBackEndInfo>
              ) : (
                <>
                  {currentClients.map((item) => (
                    <>
                      <S.TableLine>
                        <S.TableCell className="text-upp">{item.name}</S.TableCell>
                        <S.TableCell className="text-upp off-1">{item.email}</S.TableCell>
                        <S.TableCell className="text-upp">{item.phone}</S.TableCell>
                        <S.TableCell className="text-upp off-3">{item.cityUf}</S.TableCell>
                        <S.TableCell className="text-upp">
                          <S.ButtonGroup>
                            <Button
                              margin="0 auto"
                              icon="trash_grey"
                              iconPosition="center"
                              padding="0"
                              width="22px"
                              height="22px"
                              onClick={() => handleOpenModalDelete(item)}
                              showIcon
                              type="button"
                            />
                          </S.ButtonGroup>
                        </S.TableCell>
                        <S.TableCell className="text-upp off-button-set">
                          <S.Expansed href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                            <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                          </S.Expansed>
                        </S.TableCell>
                      </S.TableLine>
                      {item.activeExpansed ? (
                        <S.TableLineExpansed className="activeExpansed">
                          <S.TableCell className="text-upp">
                            <div>
                              <S.TitleValueExpan>
                                <p>Email</p>
                              </S.TitleValueExpan>
                              <div>
                                <p>{item.email}</p>
                              </div>
                            </div>
                          </S.TableCell>
                          <S.TableCell className="text-upp">
                            <div>
                              <S.TitleValueExpan>
                                <p>Cidade/Estado</p>
                              </S.TitleValueExpan>
                              <div>
                                <p>{item.cityUf}</p>
                              </div>
                            </div>
                          </S.TableCell>
                        </S.TableLineExpansed>
                      ) : null}
                    </>
                  ))}
                </>
              )}
            </>
          </S.TableBody>
        </S.ContainerDataTable>

        <S.ContainerPagination className="pagination">
          <Pagination currentPage={currentPage} perPage={perPage} total={clients.length} paginate={paginate} />
        </S.ContainerPagination>
      </S.Container>
      <ConfirmationDelete
        exeFunctionAction={handleActiveConfSuccess}
        isModalDelConf={isModalDeleteVisible}
        setIsModalDelConf={setIsModalDeleteVisible}
        msg1="Tem certeza que deseja excluir este cliente?"
        msg2="Ao excluir o cliente não estará mais na sua lista de contatos!"
      />
      <ConfirmationSuccess
        functionExecute={handleDesativeConfSuccess}
        isModalConfSuccess={isModalConfSuccess}
        setIsModalConfSuccess={setIsModalConfSuccess}
        msg1="Alterações realizadas com sucesso!"
        msg2="O cliente foi excluido"
      />
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
}
export default MyClients;
