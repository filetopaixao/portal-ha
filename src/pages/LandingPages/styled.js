import styled from 'styled-components';

import { Button as Bt } from '@components/atoms/Button/styled';

export const GridButtons = styled.div`
  display: block;

  margin-bottom: 15px;

  @media (min-width: 992px) {
    display: grid;

    width: 100%;

    grid-template-columns: ${(props) => {
      const width = 100 / props.qtd;
      let grid = '';
      for (let i = 0; i < props.qtd; i += 1) {
        grid += `${width}% `;
      }
      return `${grid}`;
    }};
  }
`;

export const Button = styled(Bt)`
  text-align: center;
  display: block;
  font-size: 13px;
  width: 100%;

  &.btnGrid {
    /* height: 43px; */
    border-radius: 10px;

    ${(props) =>
      // eslint-disable-next-line no-nested-ternary
      props.active
        ? `background: ${props.primaryColor}; color: #fff;`
        : props.theme === 'Dark'
        ? `background: #29303A; color: #979EA8;`
        : `background: #FBFBFB; color: #82868D;`};
  }
`;

export const Column = styled.div`
  padding: 4px;
`;

export const Content = styled.main``;

export const ContainerOptions = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 1000px) {
    display: grid;
    row-gap: 10px;
  }
`;

export const ContainerSelects = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 10px;
  align-items: center;

  @media (max-width: 1000px) {
    display: grid;
    grid-template-columns: 1fr;
    row-gap: 10px;

    width: 100%;

    > div {
      width: 100%;
    }

    select {
      width: 38rem;
    }
  }
`;
