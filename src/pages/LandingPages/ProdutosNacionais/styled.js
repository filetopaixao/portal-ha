import styled from 'styled-components';

import PageLoadding from '@assets/images/page_loadding.gif';
import NotFound from '@assets/images/erro-404.svg';

export const Container = styled.div`
  min-height: 500px;
  margin: 20px 0;
  padding: 20px;

  border-radius: 10px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const List = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;

  @media (max-width: 1100px) {
    grid-template-columns: 1fr 1fr;
    row-gap: 20px;
  }
`;

export const CardLandingPage = styled.a`
  text-decoration: none;
  margin: 0 auto;
`;

export const ContainerImage = styled.div``;

export const Image = styled.div`
  content: url(${(props) => props.image});

  width: 15rem;
  height: auto;

  margin: 0 auto;

  border-radius: 10px;
  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 800px) {
    width: 8rem;
    height: auto;
  }
`;

export const Title = styled.div`
  text-align: center;
  font-weight: bold;
  letter-spacing: 0px;

  margin-top: 10px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const Description = styled.div`
  text-align: center;
  letter-spacing: 0px;

  margin-top: 3px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
