import React, { useState, useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';

import PropTypes from 'prop-types';

import ModalShareCopy from '../../../components/molecules/Modals/ModalShareCopy';
import Pagination from '../../../components/molecules/Pagination';

import { list } from '../../../services/landingPages';

import landingPageExample from '../../../assets/images/landing-page-model-2.png';

import * as S from './styled';

const ProdutosImportados = ({ order, productSelected, filterText }) => {
  const themeStore = useSelector((state) => state.theme);
  const [landingPages, setLandingPages] = useState([]);
  const [activeLoading, setActiveLoading] = useState(true);

  useEffect(() => {
    list()
      .then((response) => {
        let arrayVerify = [];
        console.log(response.data.data);
        arrayVerify = response.data.data;
        if (arrayVerify.length > 0) {
          console.log('');
        } else {
          setLandingPages([
            {
              id: 1,
              name: 'reasons',
              image: landingPageExample,
              categorie: '',
              title: '10 razões',
              description: 'landing example',
              url: 'https://everbynature.com.br/reasons',
            },
            {
              id: 2,
              name: 'now',
              image: landingPageExample,
              categorie: '',
              title: 'Emargreça rápido',
              description: 'landing example',
              url: 'https://everbynature.com.br/now',
            },
            {
              id: 3,
              name: 'club',
              image: landingPageExample,
              categorie: '',
              title: 'Clube do chá',
              description: 'landing example',
              url: 'https://everbynature.com.br/club',
            },
            {
              id: 4,
              name: 'moment',
              image: landingPageExample,
              categorie: '',
              title: 'Momento do chá',
              description: 'landing example',
              url: 'https://everbynature.com.br/moment',
            },
          ]);
        }

        setActiveLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLandingPages([]);
      });
  }, []);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [linkSuport, setLinkSuport] = useState('aaaaaaaaaa');
  const [linkName, setLinkName] = useState('');

  const handleActive = (e, name, link) => {
    e.preventDefault();
    setLinkName(name);
    setLinkSuport(link);
    setIsModalVisible(!isModalVisible);
  };

  const filteredItems = landingPages.filter(
    (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
  );

  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  let currentLandings = filteredItems.slice(indexOfFirst, indexOfLast);

  if (productSelected) {
    console.log(productSelected);
  }

  if (order) {
    if (order === 'asc') {
      currentLandings = currentLandings.sort();
    } else {
      currentLandings = currentLandings.reverse();
    }
  }
  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  if (activeLoading) {
    return (
      <S.ContainerBackEndInfo>
        <S.ImageLoadding />
      </S.ContainerBackEndInfo>
    );
  }

  return (
    <>
      <S.Container theme={themeStore.themeName}>
        <S.List>
          <>
            {currentLandings.length <= 0 ? (
              <S.ContainerBackEndInfo>
                <S.InfoBackEnd theme={themeStore.themeName}>Não há clientes cadastrados</S.InfoBackEnd>
                <S.ImageNotFound />
              </S.ContainerBackEndInfo>
            ) : (
              <>
                {currentLandings.map((item) => (
                  <S.CardLandingPage key={item.name} href="!#" onClick={(e) => handleActive(e, item.title, item.url)}>
                    <S.ContainerImage>
                      <S.Image image={item.image} />
                    </S.ContainerImage>
                    <S.Title theme={themeStore.themeName}>{item.title}</S.Title>
                    <S.Description theme={themeStore.themeName}>{item.description}</S.Description>
                  </S.CardLandingPage>
                ))}
              </>
            )}
          </>
        </S.List>
      </S.Container>
      <Pagination currentPage={currentPage} perPage={perPage} total={landingPages.length} paginate={paginate} />
      <ModalShareCopy
        linkName={linkName}
        linkSuport={linkSuport}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};

ProdutosImportados.propTypes = {
  order: PropTypes.string,
  productSelected: PropTypes.string,
  filterText: PropTypes.string,
};
ProdutosImportados.defaultProps = {
  order: '',
  productSelected: '',
  filterText: '',
};

export default ProdutosImportados;
