import React, { useState, useEffect, useCallback } from 'react';

import Banner from '../../components/molecules/Banners/BannerInfo';

import InfoProdutos from './InfoProdutos';
import ProdutosImportados from './ProdutosImportados';
import ProdutosNacionais from './ProdutosNacionais';
import Filter from '../../components/molecules/Filter';
import Select from '../../components/atoms/Select';

import * as S from './styled';

const LandingPages = () => {
  const [activeBtn1, setActiveBtn1] = useState(false);
  const [activeBtn2, setActiveBtn2] = useState(false);
  const [activeBtn3, setActiveBtn3] = useState(true);
  const [filterText, setFilterText] = useState('');
  const [productSelected, setProductSelected] = useState('');
  const [order, setOrder] = useState('');

  const productsOptions = [];

  const orders = [
    {
      label: 'Alfabética',
      value: 'asc',
    },
    {
      label: 'Decrescente',
      value: 'desc',
    },
  ];

  const opContent = 'content3';
  const textTop = `Páginas curtas de vendas ideias para integrar e fortalecer sua estratégia de vendas.`;

  console.log(productSelected);
  console.log(order);

  const handleContent1 = useCallback(() => {
    setActiveBtn1(true);
    setActiveBtn2(false);
    setActiveBtn3(false);
  }, []);
  const handleContent2 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(true);
    setActiveBtn3(false);
  }, []);
  const handleContent3 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(true);
  }, []);

  const loadContent = useCallback(() => {
    switch (opContent) {
      case 'content1':
        return handleContent1();
      case 'content2':
        return handleContent2();
      case 'content3':
        return handleContent3();
      default:
        return handleContent3();
    }
  }, [handleContent1, handleContent2, handleContent3]);

  useEffect(() => {
    loadContent();
  }, [loadContent]);

  /*
  const clickButton = (e) => {
    if (e.target.id === 'btn1') {
      if (activeBtn1) {
        setActiveBtn1(false);
      } else {
        handleContent1();
      }
    } else if (e.target.id === 'btn2') {
      if (activeBtn2) {
        setActiveBtn2(false);
      } else {
        handleContent2();
      }
    } else if (e.target.id === 'btn3') {
      if (activeBtn3) {
        setActiveBtn3(false);
      } else {
        handleContent3();
      }
    }
  };
  */

  return (
    <div>
      {/*
      <S.GridButtons qtd={0}>
        <S.Column>
          <S.Button
            alignText="center"
            height="55px"
            padding="0 24px"
            className="btnGrid"
            onClick={clickButton}
            id="btn1"
            active={activeBtn1}
            primaryColor={themeStore.primaryColor}
            theme={themeStore.themeName}
          >
            Produtos Nacionais
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            alignText="center"
            height="55px"
            padding="0 24px"
            className="btnGrid"
            onClick={clickButton}
            id="btn2"
            active={activeBtn2}
            primaryColor={themeStore.primaryColor}
            theme={themeStore.themeName}
          >
            Infoprodutos
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            alignText="center"
            height="55px"
            padding="0 24px"
            className="btnGrid"
            onClick={clickButton}
            id="btn3"
            active={activeBtn3}
            primaryColor={themeStore.primaryColor}
            theme={themeStore.themeName}
          >
            Produtos Importados
          </S.Button>
        </S.Column>
      </S.GridButtons>
       */}

      <Banner
        alignCenter
        setMargin="50px auto"
        textTop={textTop}
        image="landingPage"
        title="Links de Suporte"
        imageMobile={false}
        sizeImage
        heightImage="auto"
        widthImage="190px"
        topPosition="150px"
      />

      <S.ContainerOptions>
        <S.ContainerSelects>
          <Select
            name="orders"
            id="orders"
            // width=""
            minWidth="300px"
            options={orders}
            onChange={(e) => setOrder(e.target.value)}
            defaultValue="Ordem"
          />
          <Select
            name="productionsOptions"
            id="productionsOptions"
            minWidth="300px"
            options={productsOptions}
            onChange={(e) => setProductSelected(e.target.value)}
            defaultValue="Produto"
          />
        </S.ContainerSelects>
        <Filter
          width="300px"
          className="filterText"
          onFilter={(e) => setFilterText(e.target.value)}
          filterText={filterText}
        />
      </S.ContainerOptions>

      {activeBtn1 ? (
        <S.Content>
          <ProdutosNacionais order={order} productSelected={productSelected} filterText={filterText} />
        </S.Content>
      ) : null}

      {activeBtn2 ? (
        <S.Content>
          <ProdutosImportados order={order} productSelected={productSelected} filterText={filterText} />
        </S.Content>
      ) : null}

      {activeBtn3 ? (
        <S.Content>
          <InfoProdutos order={order} productSelected={productSelected} filterText={filterText} />
        </S.Content>
      ) : null}
    </div>
  );
};

export default LandingPages;
