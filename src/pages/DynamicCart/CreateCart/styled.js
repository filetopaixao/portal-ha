import styled from 'styled-components';
import { shade } from 'polished';

import { Container } from '@components/atoms';

import ArrowTop from '@assets/images/arrowTop.svg';
import PageLoadding from '@assets/images/page_loadding.gif';
import NotFound from '@assets/images/erro-404.svg';
import AttentionImage from '@assets/images/attention.svg';

export const ContainerCart = styled.div`
  display: block;
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 20px;

  width: 100%;

  grid-template-areas: 'resume' 'create';
  @media (max-width: 1200px) {
    .resume {
      grid-area: resume;
    }

    .create {
      grid-area: create;
    }
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    column-gap: 20px;
  }
`;

export const ContainerCartCreate = styled.div`
  width: 100%;
  padding: 30px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  box-shadow: 0px 3px 20px #00000029;

  @media (max-width: 600px) {
    padding: 20px;
  }
`;

export const ContainerCartResume = styled.div`
  width: 100%;
  padding: 30px;

  border-radius: 15px;

  background: ${(props) =>
    props.tertiaryColor
      ? `transparent linear-gradient(219deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
      : `transparent linear-gradient(219deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};

  @media (max-width: 600px) {
    padding: 20px;
  }
`;

export const ContainerTableListProducts = styled.div``;

export const InputContainer = styled.div`
  display: block;
  margin-bottom: 5px;
`;

export const Incorrect = styled.p`
  font-size: 10px;
  font: normal normal medium 12px/18px Poppins;
  letter-spacing: 0px;
  color: #f14479;
`;

export const Available = styled.p`
  font-size: 10px;
  font: normal normal medium 12px/18px Poppins;
  letter-spacing: 0px;
  color: #2fdf46;
`;

export const InputLabel = styled.label`
  font: normal 14px;
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const Input = styled.input`
  width: 100%;
  height: 35px;
  padding: 0 20px;

  border: none;
  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
  @media (min-width: 1500px) {
    height: 40px;
  }
`;

export const ButtonGroup = styled.div`
  width: 100%;

  margin: 0;
  margin-top: 20px;
`;

export const ButtonPainelPixel = styled.button`
  //font-weight: bold;
  text-transform: uppercase;
  font-size: 17px;
  width: 100%;
  height: 53px;
  margin: 0 auto;

  border: none;
  border-radius: 15px;
  cursor: pointer;

  color: #fff;
  background: ${(props) => props.secondaryColor};

  &:hover {
    background: ${(props) => `${shade(0.2, props.secondaryColor)}`};
  }

  @media (min-width: 1500px) {
    height: 53px;
  }
`;

export const ContainerOps = styled.div`
  display: block;
  margin-top: 10px;
`;

export const Title = styled.label`
  font: lighter 12px;
  color: #fff;

  @media (min-width: 600px) {
    font: lighter 14px;
  }
`;

export const TitleCategories = styled(Title)`
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const ContentOpsCategories = styled.div`
  padding: 30px;
  margin-bottom: 15px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const CheckboxContainer = styled.div`
  width: auto;
  margin-bottom: 10px;
`;

export const ContainerListCategories = styled.div`
  overflow-y: scroll;
  max-height: 110px;

  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 14px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 14px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 3px;

    background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }
`;

export const ContainerSelectDiscount = styled.div`
  font-size: 12px;

  display: block;
  // grid-template-columns: 1fr 2fr;
  align-items: center;
  justify-content: space-between;

  padding: 30px;
  margin-bottom: 15px;
  width: 100%;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  p {
    text-align: left;
    font-size: 12px;
    letter-spacing: 0px;
    color: #5e5e5e;

    margin: 0 3px;

    @media (min-width: 600px) {
      font-size: 14px;
      display: flex;
      margin: 0 10px;
    }
  }

  @media (max-width: 600px) {
    padding: 10px;
  }

  @media (min-width: 600px) {
    display: flex;
  }
`;

export const InputTitle = styled.h4`
  font-weight: lighter;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const InputDate = styled.input`
  height: 40px;
  width: 100%;
  padding-left: 3px;

  border-radius: 10px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::-webkit-calendar-picker-indicator {
    filter: invert(0.5);
  }
`;

export const FirstOpDiscount = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  align-items: center;

  > div {
    margin-right: 5px;
    min-width: 140px;

    @media (max-width: 600px) {
      width: 100%;
    }
  }

  @media (min-width: 600px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
  }
`;

export const SecondOpDiscount = styled.div`
  display: flex;
  //grid-template-columns: 0.2fr 1fr 50px;
  p {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
  align-items: center;
`;

export const InputNumberDiscount = styled.div`
  display: flex;
  //grid-template-columns: 1fr auto;
  align-items: center;
  column-gap: 10px;

  padding: 5px;

  height: 40px;
  width: 90px;

  border: none;
  border-radius: 9px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  input {
    width: 35px;
    border: 0;
    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  @media (max-width: 600px) {
    width: 50px;
  }

  p {
    text-align: center;
  }

  button {
    display: block;
    border: none;
    background: transparent;
    cursor: pointer;
  }

  button:first-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
    }
  }

  button:last-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
      transform: rotate(180deg);
    }
  }
`;

export const ButtonGroupAddOrRemove = styled.div``;

export const CreateCartButton = styled.button`
  text-transform: uppercase;
  //font-weight: bold;
  font-size: 17px;

  height: 53px;
  width: 100%;

  margin: 0 auto;

  border: none;
  border-radius: 15px;

  color: #fff;
  background: ${(props) => props.primaryColor};

  &:hover {
    background: ${(props) => `${shade(0.2, props.primaryColor)}`};
  }
`;

export const ButtonCancel = styled.button`
  text-align: center;
  text-transform: uppercase;

  width: 100px;
  margin: 0 auto;

  cursor: pointer;
  border: none;
  background: transparent;
  color: #5b5b5b;
`;

export const ContainerPagination = styled.div`
  width: 100%;
`;

export const ButtonGroupCreate = styled.div`
  margin-top: 5px;

  text-align: center;
`;

export const ContainerCampaing = styled.div`
  display: block;

  margin: 0 auto;
  margin-top: 20px;
  padding: 78px 0px;

  p,
  h2 {
    text-align: center;
    text-transform: uppercase;
    color: #fff;
  }

  h2 {
    font-weight: bold;
    font-size: 24px;
  }

  P {
    font-size: 14px;
  }
`;

export const ImageCampaing = styled.img`
  display: block;

  width: 222px;
  height: 190px;

  margin: 0 auto;

  background-repeat: no-repeat;
  background-size: contain;
`;

export const TitleCartResume = styled(Title)`
  font-size: 14px;
  text-transform: uppercase;
  font-weight: bold;
  color: #fff;
`;

export const TitleListProducts = styled(Title)`
  font-size: 16px;
  color: #fff;
`;

export const ContainerResult = styled.div`
  width: 100%;

  padding-top: 70px;

  text-align: right;
`;

export const ResultResumeInfo = styled.div`
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #fff;
    font-weight: lighter;
  }

  h3 {
    font-size: 24px;
    font-weight: bold;
    text-transform: uppercase;
    color: #fff;
  }

  line-height: 30px;
`;

export const ContainerTableproduct = styled.div`
  text-align: center;
  padding: 20px;

  border-radius: 10px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (max-width: 600px) {
    padding: 10px;
  }
`;

export const TableHeader = styled.div`
  display: grid;
  grid-template-columns: 40px 75px 0.5fr 15px 1fr;
  align-items: center;

  padding-right: 10px;
  margin-bottom: 10px;

  p {
    font-size: 13px;
    font-weight: bold;
    text-transform: uppercase;
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }

  @media (min-width: 600px) {
    grid-template-columns: 1fr 75px 0.5fr 15px 1fr;
  }
`;

export const TableBody = styled.div`
  overflow-y: scroll;

  height: 10rem;

  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 14px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 14px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 3px;

    background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }
`;

export const TableLine = styled.div`
  text-align: center;

  display: grid;
  grid-template-columns: 40px 75px 0.5fr 15px 1fr;
  align-items: center;

  margin-bottom: 10px;

  .item-title {
    font-weight: bold;
  }

  p {
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }

  img {
    height: 35px;
    width: 35px;

    margin-right: 3px;

    @media (min-width: 600px) {
      margin: 0 auto;
    }
  }

  @media (min-width: 600px) {
    grid-template-columns: 1fr 75px 0.5fr 15px 1fr;
  }
`;

export const ProductToSelect = styled.div`
  display: grid;
  grid-template-columns: 2fr 0.5fr;
  align-items: center;
  column-gap: 10px;

  margin-bottom: 15px;

  select {
    background: #ffffff 0% 0% no-repeat padding-box;
    border: 1px solid #bdbdbd;
    border-radius: 15px;
  }
`;

export const InputProductQdt = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  column-gap: 10px;

  padding: 5px;

  height: 40px;
  width: 80px;

  border: none;
  border-radius: 9px;

  background: #f2f2f2 0% 0% no-repeat padding-box;

  p {
    text-align: center;
  }

  button {
    display: block;
    border: none;
    background: transparent;
    cursor: pointer;
  }

  button:first-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
    }
  }

  button:last-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
      transform: rotate(180deg);
    }
  }
`;

export const ButtonAddOrRemove = styled.div``;

export const LinkIncludeProduct = styled.a`
  display: flex;
  align-items: center;

  text-decoration: none;

  letter-spacing: 0px;
  color: #01a3ff;

  > div:first-child {
    display: grid;
    align-items: center;
    width: 17px;
    height: 17px;

    margin-right: 10px;

    border-radius: 4px;

    background: #01a3ff 0% 0% no-repeat padding-box;
    p {
      text-align: center;
      font-size: 12px;
      color: #fff;
    }
  }
`;

/** *********************** */
export const ContainerPurchaseList = styled(Container)``;

export const ContainerListProducts = styled(Container)`
  margin: 10px 0;

  > div {
    max-height: 260px;
    min-height: 260px;
    max-width: 1100px;

    padding: 10px;

    overflow-y: scroll;

    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

    @media (max-width: 600px) {
      max-height: 170px;
      min-height: 170px;
    }

    @media (max-width: 1500px) and (min-width: 600px) {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      width: 500px;
      margin: 0 auto;
    }

    @media (min-width: 1500px) {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;

      margin: 0 auto;
    }

    ::-webkit-scrollbar {
      -webkit-appearance: none;
    }
    ::-webkit-scrollbar:vertical {
      width: 14px;
    }
    ::-webkit-scrollbar:horizontal {
      height: 14px;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 3px;

      background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
    }
    ::-webkit-scrollbar-track {
      border-radius: 10px;
    }
  }
`;

export const ContentModalMessage = styled.div`
  text-align: center;
  color: #5e5e5e;
`;

export const Attention = styled.div`
  content: url(${AttentionImage});
  width: 15rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;
`;

export const ButtonsAddAndRemove = styled.div`
  margin: 0 3px;

  button {
    display: block;

    border: none;

    background: transparent;

    cursor: pointer;
  }

  button:first-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
    }
  }

  button:last-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
      transform: rotate(180deg);
    }
  }

  button:hover {
    background: ${shade(-0.2, '#F2F2F8')};
  }
`;

export const ContainerAddAndRemove = styled.div`
  display: flex;
  align-items: center;
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: #5e5e5e;
  font: normal normal normal 14px Poppins;
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
