import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import { Products } from '@components/molecules';
import ModalPanelPixel from '@components/molecules/Modals/PanelPixel';
import ModalAlert from '@components/molecules/Modals/ModalComponent';
import SelectCategories from '@components/atoms/Select/selectCategories';
import ConfirmationSuccess from '@components/molecules/Modals/ModalConfSuccess';

import { formateValueBr } from '@utils/formatValueBr';
import { Actions as DinamicCarActions } from '@redux/ducks/dinamycCar';
import { getUser } from '@services/user';
import { getCars, addCar } from '../../../services/dinamycCar';
import getAll from '../../../services/fpass/products';

import * as S from './styled';

const categoriesFpass = [
  {
    id: 0,
    name: 'Todos',
    type: '',
  },
  {
    id: 1,
    name: 'Cursos',
    type: 'product',
  },
  {
    id: 2,
    name: 'Planos',
    type: 'plan',
  },
  {
    id: 3,
    name: 'Programas',
    type: 'collection',
  },
];

const CartCreate = () => {
  const themeStore = useSelector((state) => state.theme);
  const [productsOrigin, setProductsOrigin] = useState([]);
  const [products, setProducts] = useState([]);
  const [productsSelected, setProductsSelected] = useState([]);

  const history = useHistory();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalAlert, setModalAlert] = useState(false);
  const [discountValue, setDiscountValue] = useState(0);

  const [subTotal, setSubTotal] = useState(0);
  const [desconto, setDesconto] = useState(0);
  const [total, setTotal] = useState(0);

  const [dateFinal, setDateFinal] = useState('');
  const [dateInitial, setDateInitial] = useState(Date.now());
  const [typeIdDinamicCar, setTypeIdlDinamicCar] = useState('');
  const [urlDinamicCar, setUrlDinamicCar] = useState('');
  const [nameDinamicCar, setNameDinamicCar] = useState('');
  const [verify, setVerify] = useState('pause');
  const [listCarDinamyc, setListCarDinamyc] = useState([]);
  const [panelPixel, setPanelPixel] = useState([]);
  const [isModalConfSuccess, setIsModalConfSuccess] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const dispatch = useDispatch();
  const dinamycCarStore = useSelector((state) => state.dinamycCar);

  useEffect(() => {
    dispatch(DinamicCarActions.getNameDinamicCar());
    setNameDinamicCar(dinamycCarStore.nameDinamycCar);
    setUrlDinamicCar(`http://3.131.133.84/checkout/dynamic/${dinamycCarStore.nameDinamycCar}`); // http://http://3.140.8.38/
  }, [dispatch, dinamycCarStore.nameDinamycCar]);

  const loadCars = useCallback(() => {
    getCars().then((response) => {
      const { data } = response.data;
      console.log(data);

      const res = data.map((item) => {
        return {
          id: item.id,
          name: item.name,
          discount: item.discount_applied,
          products: item.products,
          value: item.total,
          active: item.is_active,
          activeExpansed: false,
          linkCart: item.url,
          linkThanks: `${item.name} - linkThanks`,
          status: 'toogle',
          action: 'action',
        };
      });

      setListCarDinamyc(res);
    });
  }, []);

  useEffect(() => {
    loadCars();
  }, [loadCars]);

  const handleActiveModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const handleIncrement = () => {
    if (discountValue <= 9) {
      setDiscountValue(discountValue + 1);
    } else {
      setModalAlert(true);
      setDiscountValue(0);
    }
  };

  const handleSetDicount = (e) => {
    if (e.target.value <= 9) {
      setDiscountValue(e.target.value);
    } else {
      setModalAlert(true);
      setDiscountValue(0);
    }
  };

  const handleReduce = () => {
    setDiscountValue(discountValue - 1);
  };

  const handleSelectProduct = (product) => {
    if (productsSelected.find((productSelected) => productSelected.id === product.id)) {
      const newProducts = productsSelected.map((item) => {
        if (item.id === product.id) {
          return { ...item, qtd: item.qtd + 1 };
        }
        return item;
      });

      setProductsSelected(newProducts);
    } else {
      setProductsSelected([...productsSelected, { ...product, qtd: 1 }]);
    }
  };

  useEffect(() => {
    const { subT, desc } = productsSelected.reduce(
      (accumulator, product) => {
        if (product.qtd > 1) {
          accumulator.subT += parseFloat(product.valueProduct) * product.qtd;
        } else {
          accumulator.subT += parseFloat(product.valueProduct);
        }
        accumulator.desc += (parseFloat(product.valueProduct) * discountValue) / 100;
        return accumulator;
      },
      {
        subT: 0,
        desc: 0,
      }
    );

    if (subT === 0) {
      setSubTotal(0);
      setDesconto(0);
      setTotal(0);
    }
    setSubTotal(subT);
    setDesconto((subT * discountValue) / 100);
    setTotal(subT - desc);
  }, [productsSelected, discountValue]);

  const handleIncreaseQtdSelectProduct = (productReducer) => {
    const newArray = productsSelected.map((product) => {
      if (productReducer.id === product.id) {
        return {
          ...product,
          qtd: product.qtd + 1,
        };
      }
      return product;
    });

    setProductsSelected(newArray);
  };

  const handleReduceQtdSelectProduct = (productReducer) => {
    const newArray = productsSelected.map((product) => {
      if (productReducer.id === product.id) {
        if (product.qtd > 0) {
          return {
            ...product,
            qtd: product.qtd - 1,
          };
        }
        return {
          ...product,
          qtd: 0,
        };
      }
      return product;
    });

    setProductsSelected(newArray);
  };

  const handleSelectCod = (value) => {
    const newValue = value.replace(/\s/g, '');
    setNameDinamicCar(newValue);

    let filtered = listCarDinamyc.filter(
      (item) => item.name && item.name.toLowerCase().includes(newValue.toLowerCase())
    );
    if (filtered.length > 0) {
      setVerify('existente');
    } else {
      setVerify('liberado');
      filtered = [];
    }
  };

  useEffect(() => {
    getUser().then((response) => {
      if (response.data) {
        const { type_id: typeId } = response.data;
        setTypeIdlDinamicCar(typeId);
      }
    });
  }, []);

  const handleSubmit = async () => {
    // const newArrayProducts = productsSelected.map((item) => {
    //   return {
    //     product_id: item.id,
    //     quantity: item.qtd,
    //   };
    // });

    const arrayProductsTeste = [
      {
        product_id: 10,
        quantity: 1,
      },
    ];

    const object = {
      name: nameDinamicCar,
      url: urlDinamicCar,
      discount: discountValue,
      valid_from: dateInitial,
      valid_until: dateFinal,
      type_id: typeIdDinamicCar,
      products: arrayProductsTeste,
      pixel_data: {
        from: 'google',
        pixel: 'rvbroib131RVa',
        type: 'output',
      },
    };

    console.log('carrinho salvo', object);
    await addCar(object).then((responseAddCard) => {
      console.log('responseAddCard', responseAddCard);
      const newObject = {
        id: listCarDinamyc[listCarDinamyc.length - 1].id + 1,
        name: nameDinamicCar,
        discount: discountValue,
        products: arrayProductsTeste,
        value: 0,
        active: true,
        activeExpansed: false,
        linkCart: urlDinamicCar,
        linkThanks: `${nameDinamicCar} - linkThanks`,
        status: 'toogle',
        action: 'action',
      };

      setListCarDinamyc([...listCarDinamyc, newObject]);
    });

    setIsModalConfSuccess(!isModalConfSuccess);
  };

  // const handleDefineUrlCar = (value) => {
  //   setUrlDinamicCar(`everbynature/user/${value}`);
  // };

  function handleSelectedCategoryFpass(textFilter) {
    const filteredItems = productsOrigin.filter(
      (item) => item.type && item.type.toLowerCase().includes(textFilter.toLowerCase())
    );
    setProducts(filteredItems);
  }

  useEffect(() => {
    getAll()
      .then((response) => {
        console.log(response.data);

        const serializedProducts = response.data.map((item) => {
          const nameProduct = item.name;
          const arrayName = nameProduct.split(' ');
          return {
            id: item.id,
            title: arrayName[0],
            description: item.description,
            frequency: item.frequency,
            type: item.type,
            srcImg: item.images[0],
            valueProduct: item.price,
            valueCommision: 0,
            urlProduct: 'https://urldoproduto',
            purchase: true,
            product: false,
            isSelected: false,
            qtd: 0,
          };
        });
        setProducts(serializedProducts);
        setProductsOrigin(serializedProducts);
        setIsLoading(false);
      })
      .catch((getAllError) => {
        console.log(getAllError);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.ContainerCart>
        <S.ContainerCartCreate className="create" theme={themeStore.themeName}>
          <form>
            <S.InputContainer>
              <S.InputLabel theme={themeStore.themeName}>Qual o nome da sua playlist?</S.InputLabel>
              <S.Input
                name="name"
                placeholder="Digite o código da playlist"
                value={nameDinamicCar}
                onChange={(e) => handleSelectCod(e.target.value)}
                type="text"
                theme={themeStore.themeName}
              />
            </S.InputContainer>
            {verify === 'existente' ? <S.Incorrect>X Nome indisponível</S.Incorrect> : null}
            {verify === 'liberado' ? <S.Available>✓ Nome disponível</S.Available> : null}
            <S.InputContainer>
              <S.InputLabel theme={themeStore.themeName}>Nome da URL</S.InputLabel>
              <S.Input
                name="url"
                disabled
                placeholder="Digite o código da playlist"
                // onChange={(e) => handleDefineUrlCar(e.target.value)}
                value={urlDinamicCar}
                theme={themeStore.themeName}
              />
            </S.InputContainer>

            <S.ButtonGroup>
              <S.ButtonPainelPixel
                secondaryColor={themeStore.secondaryColor}
                type="button"
                onClick={() => handleActiveModal()}
              >
                Panel Pixel
              </S.ButtonPainelPixel>
            </S.ButtonGroup>
          </form>

          <S.ContainerOps>
            <S.TitleCategories theme={themeStore.themeName}>Categorias:</S.TitleCategories>
            <S.ContentOpsCategories theme={themeStore.themeName}>
              <SelectCategories
                name="categories"
                id="categories"
                options={categoriesFpass}
                defaultValue="Escolha a categoria"
                onChange={(e) => handleSelectedCategoryFpass(e.target.value)}
              />
            </S.ContentOpsCategories>
            <S.ContainerPurchaseList>
              <S.Title theme={themeStore.themeName} size="sm" variant="bold" className="text_title">
                Selecione os produtos que deseja incluir na playlist
              </S.Title>

              <S.ContainerListProducts theme={themeStore.themeName}>
                <Products handleSelected={handleSelectProduct} data={products} page="purchase" className="scrollbar" />
              </S.ContainerListProducts>
            </S.ContainerPurchaseList>

            <S.TitleCategories theme={themeStore.themeName}>
              Transformar comissão em desconto para essa playlist:
            </S.TitleCategories>

            <S.ContainerSelectDiscount theme={themeStore.themeName}>
              <S.FirstOpDiscount>
                <S.InputContainer>
                  <S.InputTitle theme={themeStore.themeName}>Iniciar em:</S.InputTitle>
                  <S.InputDate
                    type="date"
                    name="dateIni"
                    id="dateIni"
                    value={dateInitial}
                    onChange={(e) => setDateInitial(e.target.value)}
                    theme={themeStore.themeName}
                    min={new Date().toJSON().split('T')[0]}
                  />
                </S.InputContainer>

                <S.InputContainer>
                  <S.InputTitle theme={themeStore.themeName}>Encerrar em:</S.InputTitle>
                  <S.InputDate
                    type="date"
                    name="dateFim"
                    id="dateFim"
                    onChange={(e) => setDateFinal(e.target.value)}
                    theme={themeStore.themeName}
                  />
                </S.InputContainer>
              </S.FirstOpDiscount>

              <S.SecondOpDiscount theme={themeStore.themeName}>
                {/* <input name="btnDiscount" id="comDiscount" type="radio" value="comdesconto" /> */}
                <p>Desconto de</p>
                <S.InputNumberDiscount theme={themeStore.themeName} className="btn-bigger-one">
                  <input onChange={(e) => handleSetDicount(e)} type="text" value={discountValue} />
                  <span>%</span>
                  <S.ButtonGroupAddOrRemove className="btn-group">
                    <button onClick={() => handleIncrement()} type="button">
                      {' '}
                    </button>
                    <button onClick={() => handleReduce()} type="button">
                      {' '}
                    </button>
                  </S.ButtonGroupAddOrRemove>
                </S.InputNumberDiscount>
              </S.SecondOpDiscount>
            </S.ContainerSelectDiscount>
          </S.ContainerOps>

          <S.ButtonGroupCreate>
            <S.CreateCartButton
              primaryColor={themeStore.primaryColor}
              type="button"
              variant="primary"
              onClick={() => handleSubmit()}
            >
              Criar Playlist
            </S.CreateCartButton>
            <S.ButtonCancel type="button" onClick={() => history.push('/playlist')}>
              Cancelar
            </S.ButtonCancel>
          </S.ButtonGroupCreate>
        </S.ContainerCartCreate>

        <S.ContainerCartResume
          className="resume"
          primaryColor={themeStore.primaryColor}
          secondaryColor={themeStore.secondaryColor}
          tertiaryColor={themeStore.tertiaryColor}
        >
          <S.TitleCartResume>Visualização da Playlist</S.TitleCartResume>

          <S.ContainerCampaing>
            <p>Nome:</p>
            <h2>{nameDinamicCar}</h2>
            {/* <S.ImageCampaing src={Ecommerce} /> */}
          </S.ContainerCampaing>

          <S.ContainerTableListProducts>
            <S.TitleListProducts>Produtos:</S.TitleListProducts>

            <S.ContainerTableproduct theme={themeStore.themeName}>
              <S.TableHeader theme={themeStore.themeName}>
                <div />
                <div>
                  <p>Descrição</p>
                </div>
                <div>
                  <p>Qt</p>
                </div>
                <div />
                <div>
                  <p>Preço Un.</p>
                </div>
              </S.TableHeader>

              <S.TableBody theme={themeStore.themeName}>
                {productsSelected.map((item) => (
                  <S.TableLine theme={themeStore.themeName}>
                    <div>
                      <img src={item.srcImg} alt="Produto" />
                    </div>

                    <p className="item-title">{item.title}</p>

                    <p>{item.qtd}</p>

                    <S.ButtonsAddAndRemove>
                      <button onClick={() => handleIncreaseQtdSelectProduct(item)} type="button">
                        {' '}
                      </button>
                      <button onClick={() => handleReduceQtdSelectProduct(item)} type="button">
                        {' '}
                      </button>
                    </S.ButtonsAddAndRemove>

                    <p>{item.valueProduct}</p>
                  </S.TableLine>
                ))}
              </S.TableBody>
            </S.ContainerTableproduct>

            <S.ContainerResult>
              <S.ResultResumeInfo>
                <p>subtotal:</p>
                <h3>{formateValueBr(subTotal)}</h3>
              </S.ResultResumeInfo>

              <S.ResultResumeInfo>
                <p>desconto:</p>
                <h3>{formateValueBr(desconto)}</h3>
              </S.ResultResumeInfo>

              <S.ResultResumeInfo>
                <p>valor final:</p>
                <h3>{formateValueBr(total)}</h3>
              </S.ResultResumeInfo>
            </S.ContainerResult>
          </S.ContainerTableListProducts>
        </S.ContainerCartResume>
      </S.ContainerCart>
      <ModalPanelPixel
        panelPixel={panelPixel}
        setPanelPixel={setPanelPixel}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
      <ModalAlert
        title=""
        ContentHeight="370px"
        MaxHeight="370px"
        isVisible={modalAlert}
        alignCenter
        onClose={() => setModalAlert(!modalAlert)}
      >
        <S.ContentModalMessage>
          <h2>Você não pode dar mais que 10% de desconto!</h2>
          <S.Attention />
        </S.ContentModalMessage>
      </ModalAlert>

      <ConfirmationSuccess
        functionExecute={() => history.push('/playlist')}
        isModalConfSuccess={isModalConfSuccess}
        setIsModalConfSuccess={setIsModalConfSuccess}
        msg1="Playlist cadastrada com sucesso!"
        msg2="Divulgue e acompanhe os resultados"
      />
    </>
  );
};

export default CartCreate;
