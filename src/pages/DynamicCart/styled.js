import styled from 'styled-components';
import { shade } from 'polished';

import PageLoadding from '@assets/images/page_loadding.gif';
import NotFound from '@assets/images/erro-404.svg';
import IconEdit from '@assets/images/edit.svg';
import IconCopy from '@assets/images/copy.svg';

export const ContainerCart = styled.div`
  display: grid;
  grid-template-areas: 'resume' 'create';
  row-gap: 15px;
  width: 100%;

  @media (min-width: 1200px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    column-gap: 15px;
  }

  @media (max-width: 1200px) {
    .resume {
      grid-area: resume;
    }

    .create {
      grid-area: create;
    }
  }
`;

export const InputCopy = styled.input`
  cursor: pointer;
  display: -webkit-box;
  background: transparent;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  margin-right: 5px;
`;

export const ButtonEdit = styled.button`
  padding: 8px 12px;
  background: transparent;
  border: none;
  margin: 0;
`;

export const ContainerDataTable = styled.div`
  width: 100%;

  margin: 20px 0;
  padding: 10px;
  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  > p {
    font-size: 16px;
    font-weight: bold;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  @media (min-width: 500px) {
    padding: 20px;
  }
`;

export const TableHeader = styled.div`
  text-align: center;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 20% 25% 1fr 1fr;

  @media (max-width: 1200px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 20% 1fr 1fr;
  }

  @media (max-width: 1000px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  }

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 0.8fr 0.4fr 0.3fr;
  }

  margin-top: 15px;
  padding: 14px 0;

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  p {
  }
`;

export const TextCell = styled.p`
  font-size: 12px;
  font-weight: bold;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (max-width: 1200px) {
    &.off-3 {
      display: none;
    }
  }

  @media (max-width: 1000px) {
    &.off-2 {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const TableBody = styled.div`
  img {
    display: block;
    height: 100%;
    width: 20px !important;

    @media (min-width: 1400px) {
      height: 100%;
      width: 25px !important;
    }
  }
`;

export const TableLine = styled.div`
  text-align: center;

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 20% 25% 1fr 1fr;
  align-items: center;

  @media (max-width: 1200px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 20% 1fr 1fr;
  }

  @media (max-width: 1000px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  }

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 0.8fr 0.4fr 0.3fr;
  }

  padding: 10px 0px;

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
`;

export const TableCell = styled.div`
  font-size: 12px;
  font-weight: bold;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  .toggle-switch {
    margin-left: -25px;
  }

  .button-edit {
    -webkit-mask-image: url(${IconEdit});
    mask-image: url(${IconEdit});
    mask-repeat: no-repeat;
    mask-size: 20px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    width: 20px;
    height: 20px;
  }

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (min-width: 800px) {
    &.off-button-set {
      display: none;
    }
  }

  @media (max-width: 1200px) {
    &.off-3 {
      display: none;
    }
  }

  @media (max-width: 1000px) {
    &.off-2 {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const ContainerPagination = styled.div`
  width: 100%;
`;

export const TableLineExpansed = styled.div`
  transition: all cubic-bezier(0.175, 0.885, 0.32, 1.275);

  padding: 15px 20px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    display: none;
  }

  &.activeExpansed {
    height: auto;
  }

  &.defaultExpansed {
    height: 0px;
  }
`;

export const ButtonExpansed = styled.img`
  grid-area: 'close';
  width: 20px;
  height: 100%;
  cursor: pointer;
  transform: rotate(180deg);
  transition: transform 300ms linear;

  ${(props) => (props.active ? 'transform: rotate(0);' : '')};
`;

export const Button = styled.button`
  font-size: 18px;
  text-transform: uppercase;
  text-align: center;

  display: flex;
  align-items: center;
  margin: 0 auto;
  min-height: 26px;

  cursor: pointer;
  border: none;
  border-radius: 0;
  box-shadow: none;
  background: none;
  position: relative;
  outline: 0;

  color: #fff;

  @media (min-width: 1500px) {
    min-height: 38px;
  }
`;

export const ButtonCopy = styled(Button)`
  input {
    border: none;
  }

  div {
    -webkit-mask-image: url(${IconCopy});
    mask-image: url(${IconCopy});
    mask-repeat: no-repeat;
    mask-size: 20px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    width: 20px;
    height: 20px;
  }
`;

export const ExpansedContent = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;

  padding: 4px;

  h4 {
    font-weight: bold;
  }

  span {
    text-align: center;
  }
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;

/** ******************* */

export const CheckoutContainer = styled.div`
  padding: 15px 20px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  margin: 0px;

  @media (min-width: 600px) {
    padding: 48px;
    min-height: 317px;
  }
`;

export const CheckoutContent = styled.div`
  width: 100%;
`;

export const CheckoutTitle = styled.h2`
  font-size: 16px;
  letter-spacing: 0px;
  font-weight: 600;
  text-align: center;

  width: 100%;
  margin-bottom: 30px;

  opacity: 0.9;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;

export const CheckoutInput = styled.input`
  font-size: 14px;

  width: 100%;
  height: 40px;
  padding: 3px 20px;

  border: none;
  border-radius: 15px;
  opacity: 1;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 1500px) {
    font-size: 18px;
  }
  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const CheckoutLabel = styled.label`
  font-size: 14px;

  padding-left: 10px;
  margin-bottom: 10px;

  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  opacity: 1;

  @media (min-width: 1500px) {
    font-size: 16px;
    margin-bottom: 7px;
  }
`;

export const CheckoutButtonGroup = styled.div`
  margin: 30px 0 0;

  @media (min-width: 768px) {
    margin-top: 49px;
  }
`;

export const CheckoutButton = styled.button`
  width: 100%;
  height: 53px;

  font-size: 14px;
  text-transform: uppercase;
  text-align: center;

  min-height: 26px;

  cursor: pointer;
  border: none;
  border-radius: 15px;
  padding: 8px 24px;
  position: relative;
  outline: 0;

  color: #fff;
  background: ${(props) => props.primaryColor};
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);

  &:hover {
    background: ${(props) => `${shade(0.2, props.primaryColor)}`};
  }

  @media (min-width: 768px) {
    padding: 15px 0;
  }
`;

export const CheckoutText = styled.p`
  font-size: 14px;
  text-transform: uppercase;

  display: block;
  width: 100%;

  color: #fff;

  @media (min-width: 1500px) {
    font-size: 17px;
  }
`;

export const Incorrect = styled.p`
  font-size: 10px;
  letter-spacing: 0px;
  color: #f14479;
`;

export const Available = styled.p`
  font-size: 10px;
  letter-spacing: 0px;
  color: #2fdf46;
`;
