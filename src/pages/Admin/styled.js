import styled from 'styled-components';

import IconCamera from '@assets/images/icon_camera.svg';

export const Avatar = styled.div`
  width: 155px;
  height: 155px;

  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const UploadPhoto = styled.input`
  display: none;
`;

export const LbUploadPhoto = styled.label`
  width: 32px;
  height: 32px;

  border-radius: 50%;

  background: url(${IconCamera}) center center no-repeat #fff;

  position: relative;
  float: right;
  top: 125px;
  cursor: pointer;
`;
