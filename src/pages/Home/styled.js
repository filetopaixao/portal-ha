import styled from 'styled-components';

import PageLoadding from '@assets/images/page_loadding.gif';

export const Container = styled.div`
  justify-content: center;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 800px) {
    padding: 15px;
  }
`;

export const Status = styled.div`
  padding-bottom: 18px;
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
