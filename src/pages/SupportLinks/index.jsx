import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import Banner from '../../components/molecules/Banners/BannerInfo';

import ModalShareCopy from '../../components/molecules/Modals/ModalShareCopy';

import { list } from '../../services/SupportLinks';

import * as S from './styled';

const SupportLinks = () => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [linkSuport, setLinkSuport] = useState('aaaaaaaaaa');
  const [linkName, setLinkName] = useState('');

  const textTop = `
    Páginas informativas que te apresentarão ao FPASS, nossa história, modelo de negócio, 
    produção e qualidade dos produtos, possibilidades de ganhos e muito mais!
  `;

  const handleSuportLink = (e, name, link) => {
    e.preventDefault();
    setLinkName(name);
    setLinkSuport(link);
    setIsModalVisible(!isModalVisible);
  };

  useEffect(() => {
    list()
      .then((response) => {
        console.log(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <>
      <Banner
        alignCenter
        setMargin="50px auto"
        textTop={textTop}
        image="suportLinks"
        title="Links de Suporte"
        imageMobile={false}
        sizeImage
        heightImage="auto"
        widthImage="190px"
        topPosition="150px"
      />

      <div>
        <S.TitleContainerLink theme={themeStore.themeName}>A EMPRESA</S.TitleContainerLink>
        <S.ContainerLinks theme={themeStore.themeName}>
          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'História', 'https://fpass.com.br/nossahistoria')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="historia" />
              <S.TextCard>HISTÓRIA</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Visão', 'https://fpass.com.br/nossavisao')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="visao" />
              <S.TextCard>VISÃO</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Missão', 'https://fpass.com.br/nossamissao')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="missao" />
              <S.TextCard>MISSÃO</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Valores', 'https://fpass.com.br/nossavalores')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="valores" />
              <S.TextCard>VALORES</S.TextCard>
            </S.ContentLink>
          </S.CardLink>
        </S.ContainerLinks>
      </div>

      <div>
        <S.TitleContainerLink theme={themeStore.themeName}>OS PRODUTOS</S.TitleContainerLink>
        <S.ContainerLinks theme={themeStore.themeName}>
          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Catálogo', 'https://fpass.com.br/catalogo')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="catalogo" />
              <S.TextCard>Catálogo</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Ingredientes', 'https://fpass.com.br/ingredientes')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="ingredientes" />
              <S.TextCard>Ingredientes</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Qualidade', 'https://fpass.com.br/qualidade')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="qualidade" />
              <S.TextCard>Qualidade</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Produção', 'https://fpass.com.br/producao')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="producao" />
              <S.TextCard>Produção</S.TextCard>
            </S.ContentLink>
          </S.CardLink>
        </S.ContainerLinks>
      </div>

      <div>
        <S.TitleContainerLink theme={themeStore.themeName}>EMPREENDEDOR EVER</S.TitleContainerLink>
        <S.ContainerLinks theme={themeStore.themeName}>
          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'O que é', 'https://fpass.com.br/empreendedorOquee')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="empreendedorOquee" />
              <S.TextCard>O que é</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Ganhos', 'https://fpass.com.br/empreendedorGanhos')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="empreendedorGanhos" />
              <S.TextCard>Ganhos</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Vantagens', 'https://fpass.com.br/empreendedorVantagens')}
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="empreendedorVantagens" />
              <S.TextCard>Vantagens</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={
                (e) =>
                  handleSuportLink(e, 'Plano de Crescimento', 'https://fpass.com.br/empreendedorPlanoDeCrescimento')
                // eslint-disable-next-line react/jsx-curly-newline
              }
              secondaryColor={themeStore.secondaryColor}
            >
              <S.Image image="empreendedorPlanoDeCrescimento" />
              <S.TextCard>Plano de Crescimento</S.TextCard>
            </S.ContentLink>
          </S.CardLink>
        </S.ContainerLinks>
      </div>

      <div>
        <S.TitleContainerLink theme={themeStore.themeName}>PARCEIROS EVER</S.TitleContainerLink>
        <S.ContainerLinks theme={themeStore.themeName}>
          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'O que é', 'https://fpass.com.br/parceirosOquee')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="parceirosOquee" />
              <S.TextCard>O que é</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Ganhos', 'https://fpass.com.br/parceirosGanhos')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="parceirosGanhos" />
              <S.TextCard>Ganhos</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={(e) => handleSuportLink(e, 'Vantagens', 'https://fpass.com.br/parceirosVantagens')}
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="parceirosVantagens" />
              <S.TextCard>Vantagens</S.TextCard>
            </S.ContentLink>
          </S.CardLink>

          <S.CardLink>
            <S.ContentLink
              onClick={
                (e) => handleSuportLink(e, 'Plano de Crescimento', 'https://fpass.com.br/parceirosPlanoDeCrescimento')
                // eslint-disable-next-line react/jsx-curly-newline
              }
              primaryColor={themeStore.primaryColor}
            >
              <S.Image image="parceirosPlanoDeCrescimento" />
              <S.TextCard>Plano de Crescimento</S.TextCard>
            </S.ContentLink>
          </S.CardLink>
        </S.ContainerLinks>
      </div>

      <ModalShareCopy
        linkName={linkName}
        linkSuport={linkSuport}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};

export default SupportLinks;
