import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSWR from 'swr';

import { Text, Button } from '@components/atoms';
import SelectCategories from '@components/atoms/Select/selectCategories';
import { Products } from '@components/molecules';
import CardPurchase from '@components/atoms/Cards/purchase';
import ModalVideo from '@components/molecules/Modals/ModalVideo';
import Filter from '@components/molecules/Filter';
import PurchaseCollapses from '@components/organisms/Purchase/';

import { getProductByCategory } from '@services/products';
import { formateValueBr } from '@utils/formatValueBr';
import dataMock from '@utils/mock/purchase';
import { Actions as ProductsActions } from '@redux/ducks/products';
import { api, config } from '@services/api';

import Video from '@assets/images/video.svg';
import FinalizedPurchase from '@assets/images/aquirir_produtos.svg';

import * as S from './styled';

const fetcher = (url) => api.get(url, config).then((res) => res.data.data);

const infoCategories = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { data, error } = useSWR('/api/portal/categories', fetcher);

  return {
    isLoading: !data && !error,
    data,
    error,
  };
};

const Purchase = () => {
  const themeStore = useSelector((state) => state.theme);
  const { data: categories, isLoading, error } = infoCategories();

  const [products, setProducts] = useState([]);
  const [productsSelected, setProductsSelected] = useState([]);
  const [confirmOrder, setConfirmOrder] = useState(false);

  const dispatch = useDispatch();
  const productsStore = useSelector((state) => state.products);

  const [subTotal, setSubTotal] = useState(0);
  const [desconto, setDesconto] = useState(0);
  const [total, setTotal] = useState(0);
  const [filterText, setFilterText] = useState('');

  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';

  const [finalized, setFinalized] = useState(false);
  const [handleValueSelectPaymentOne, setHandleValueSelectPaymentOne] = useState('');
  const [handleValueSelectPaymentTow, setHandleValueSelectPaymentTow] = useState('');
  const [valuesSendForm, setValuesSendForm] = useState({});
  const [addressesSelected, setAddressesSelected] = useState({});

  const handleSelectedCategory = useCallback(
    (e) => {
      getProductByCategory(e.target.value)
        .then((response) => {
          console.log(productsStore);
          const serializedProducts = response.data.products.map((item) => {
            const nameProduct = item.name;
            const arrayName = nameProduct.split(' ');
            dispatch(ProductsActions.findProduct(item));
            return {
              id: item.id,
              title: arrayName[0],
              description: item.description,
              srcImg: item.image.url,
              valueProduct: item.price,
              valueCommision: item.commission,
              urlProduct: item.url,
              purchase: true,
              product: false,
              isSelected: false,
              qtd: 0,
            };
          });
          setProducts(serializedProducts);
        })
        .catch(() => {
          const serializedProducts = dataMock.map((item) => {
            return {
              id: item.id,
              title: item.title,
              description: item.nameImg,
              srcImg: item.srcImg,
              valueProduct: item.valueProduct,
              valueCommision: item.valueCommision,
              urlProduct: item.urlProduct,
              purchase: true,
              product: false,
              isSelected: false,
              qtd: 0,
            };
          });
          setProducts(serializedProducts);
        })
        .catch((err) => {
          if (err) {
            console.log(err);
          }
        });
    },
    [dispatch, productsStore]
  );

  const handleOpenModalVideo = useCallback(() => {
    setIsModalVideoVisible(!isModalVideoVisible);
  }, [isModalVideoVisible]);

  const handleSelectProduct = useCallback(
    (prodSelect) => {
      const newProducts = products.map((product) => {
        let newProduct = {};
        if (product.id === prodSelect.id) {
          if (productsSelected.find((productSelected) => productSelected.id === prodSelect.id)) {
            console.log('existe');
            newProduct = { ...product, isSelected: !product.isSelected, qtd: product.qtd + 1 };
            dispatch(ProductsActions.selectProductExists(newProduct));
            return newProduct;
          }
          newProduct = { ...product, isSelected: !product.isSelected, qtd: 1 };
          dispatch(ProductsActions.selectProduct(newProduct));
          return newProduct;
        }
        return product;
      });
      setProducts(newProducts);
    },
    [dispatch, products, productsSelected]
  );

  const handleReduceQtdSelectProduct = useCallback(
    (prodSelect) => {
      dispatch(ProductsActions.reduceSelectProduct(prodSelect));
    },
    [dispatch]
  );

  const handleIncreaseQtdSelectProduct = useCallback(
    (prodSelect) => {
      dispatch(ProductsActions.selectProductExists(prodSelect));
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(ProductsActions.getProductsSelected());
    setProductsSelected(productsStore.productHandleSelected || []);
  }, [productsStore, dispatch, productsSelected]);

  const handleRemoveProduct = useCallback(
    (index) => {
      dispatch(ProductsActions.removeProductselect(index));
      const newArray = productsSelected.filter((product) => product.id !== index);
      setProductsSelected(newArray);

      const newProducts = products.map((product) => {
        if (product.id === index) {
          return { ...product, isSelected: !product.isSelected };
        }
        return product;
      });
      setProducts(newProducts);
    },
    [dispatch, products, productsSelected]
  );

  useEffect(() => {
    const { subT, desc } = productsSelected.reduce(
      (accumulator, product) => {
        if (product.qtd > 1) {
          accumulator.subT += parseFloat(product.valueProduct) * product.qtd;
        } else {
          accumulator.subT += parseFloat(product.valueProduct);
        }
        accumulator.desc += (parseFloat(product.valueProduct) * 40) / 100;
        return accumulator;
      },
      {
        subT: 0,
        desc: 0,
      }
    );

    if (subT === 0) {
      setSubTotal(0);
      setDesconto(0);
      setTotal(0);
    }
    setSubTotal(subT);
    setDesconto(desc);
    setTotal(subT - desc);
  }, [productsSelected]);

  const handleConfirmOrder = () => {
    if (productsSelected.length >= 1) {
      setConfirmOrder(!confirmOrder);
    }
  };

  useEffect(() => {
    dispatch(ProductsActions.getSendForm());
    setAddressesSelected(productsStore.addressesSelected);
    setValuesSendForm(productsStore.valuesSendForm);
    setFinalized(productsStore.purchaseFinalized);

    setHandleValueSelectPaymentOne(productsStore.valuesSendForm.valueSelectPaymentOne);
    setHandleValueSelectPaymentTow(productsStore.valuesSendForm.valueSelectPaymentTow);
  }, [
    productsStore,
    dispatch,
    addressesSelected,
    valuesSendForm,
    finalized,
    handleValueSelectPaymentOne,
    handleValueSelectPaymentTow,
  ]);

  const handleCopy = () => {
    const btnCopy = document.getElementById('copy');
    btnCopy.select();
    document.execCommand('copy');
  };

  const codeBar = '00190500954014481606906809350314337370000000100';

  if (isLoading) {
    return <S.ImageLoadding />;
  }

  if (error) {
    return <h1>Desculpe ocorreu um erro</h1>;
  }

  return (
    <>
      <S.ContainerPage className="purchase" theme={themeStore.themeName}>
        <S.ContainerStatus className="purchase__status">
          <CardPurchase
            className="purchase__status__commision"
            icon="commision"
            f
            title="Saldo Liberado"
            variant="lootReleased"
            value="R$ 15.200"
          />
          <CardPurchase
            className="purchase__status__discount"
            icon="discount"
            title="Disconto para compra"
            variant="discount"
            discount="40%"
          />
        </S.ContainerStatus>

        {finalized ? (
          <S.ContainerPurchaseFinalized primaryColor={themeStore.primaryColor} theme={themeStore.themeName}>
            <header>
              <h2>Obrigado</h2>
              <p>Seu pedido foi realizado com Sucesso</p>
            </header>
            <section>
              <img className="image-purchase-finalized" src={FinalizedPurchase} alt="Compra realizada com sucesso!!" />
            </section>
            {handleValueSelectPaymentOne === 'boleto' || handleValueSelectPaymentTow === 'boleto' ? (
              <section>
                <p>
                  Aguardamos a confirmação do pagamento. Lembrando que boletos bancarios são compensados em até 2 dias
                  úteis após o pagamento. Você pode acompanhar o andamento do pedido em Relatórios de Compras.
                </p>

                <div className="mt30">
                  <p>código de barras</p>
                  <S.ContentInput onClick={() => handleCopy()}>
                    <S.SCInput
                      id="copy"
                      name="copy"
                      type="text"
                      placeholder="copiar"
                      value={codeBar}
                      onChange={() => false}
                    />
                    <S.CopyButton type="button" onClick={() => handleCopy()} />
                  </S.ContentInput>
                </div>

                <div className="mt30 button_group">
                  <button className="btn_pdf" type="button">
                    Gerar boleto em PDF
                  </button>
                  <button className="btn_relatorios" type="button">
                    Conferir Relatórios de Compras
                  </button>
                </div>
              </section>
            ) : (
              <section>
                <p>Você pode acompanhar o adamento do seu pedido em Relatórios de Compras.</p>
                <div className="mt100">
                  <button className="btn_relatorios" type="button">
                    Conferir Relatórios de Compras
                  </button>
                </div>
              </section>
            )}
          </S.ContainerPurchaseFinalized>
        ) : (
          <S.Content order={confirmOrder} theme={themeStore.themeName}>
            {confirmOrder ? (
              <PurchaseCollapses />
            ) : (
              <S.ContainerPurchaseList>
                <Text size="sm" variant="bold" className="text_title">
                  Selecione os produtos que deseja comprar
                </Text>

                <S.ContainerPurchaseListProducts theme={themeStore.themeName}>
                  <Text size="sm" variant="normal">
                    Categorias
                  </Text>

                  <SelectCategories
                    name="categories"
                    id="categories"
                    options={categories}
                    defaultValue="Escolha a categoria do seu produto"
                    onChange={(e) => handleSelectedCategory(e)}
                  />

                  <Filter
                    placeholder="Pesquise o nome dele aqui"
                    onFilter={(e) => setFilterText(e.target.value)}
                    filterText={filterText}
                  />
                  <S.ContainerListProducts theme={themeStore.themeName}>
                    <Products
                      handleSelected={handleSelectProduct}
                      data={products}
                      page="purchase"
                      className="scrollbar"
                    />
                  </S.ContainerListProducts>
                </S.ContainerPurchaseListProducts>
              </S.ContainerPurchaseList>
            )}

            <S.ContainerPurchaseListSelected className="line2">
              <div>
                <a href="#!" onClick={() => handleOpenModalVideo()}>
                  <img src={Video} alt="Assitir" />
                </a>
              </div>

              <Text size="sm" variant="bold" className="text_title">
                Seu Carrinho de Compras
              </Text>

              <S.ContainerTable theme={themeStore.themeName}>
                <div className="table_header_purchase">
                  <ul>
                    <li>Produto</li>
                    <S.LiTable className="off-title">Descrição</S.LiTable>
                    <li>Qt</li>
                    <li>Preço Un.</li>
                    {confirmOrder === false ? <li>Excluir</li> : null}
                  </ul>
                </div>

                <div className="table_body_purchase full_width">
                  {productsSelected.map((product) => (
                    <ul key={product.id}>
                      <li>
                        <img src={product.srcImg} alt={product.nameImg} />
                      </li>
                      <S.LiTable className="off-title">{product.title}</S.LiTable>
                      {confirmOrder === false && product.qtd >= 0 ? (
                        <S.LiTable className="align-center btn-bigger-one">
                          <p>{product.qtd}</p>
                          <div className="btn-group">
                            <button onClick={() => handleIncreaseQtdSelectProduct(product)} type="button">
                              {' '}
                            </button>
                            <button onClick={() => handleReduceQtdSelectProduct(product)} type="button">
                              {' '}
                            </button>
                          </div>
                        </S.LiTable>
                      ) : (
                        <S.LiTable className="align-center">{product.qtd}</S.LiTable>
                      )}

                      <S.LiTable className="align-center">{product.valueProduct}</S.LiTable>

                      {confirmOrder === false && product.qtd >= 0 ? (
                        <S.LiTable className="align-center" theme={themeStore.themeName}>
                          <button onClick={() => handleRemoveProduct(product.id)} type="button">
                            X
                          </button>
                        </S.LiTable>
                      ) : null}
                    </ul>
                  ))}
                </div>
              </S.ContainerTable>

              <S.ContainerResult
                theme={themeStore.themeName}
                productsSelected={productsSelected.length >= 1 ? 'selected' : 'not_selected'}
              >
                <section>
                  <ul>
                    <li>
                      <p>Subtotal:</p>
                      <p>{formateValueBr(subTotal)}</p>
                    </li>
                    <li>
                      <p>Desconto:</p>
                      <p>{formateValueBr(desconto)}</p>
                    </li>
                    <li>
                      <h3>TOTAL:</h3>
                      <h3>{formateValueBr(total)}</h3>
                    </li>
                  </ul>
                  {confirmOrder ? null : (
                    <Button onClick={() => handleConfirmOrder()} variant="success" type="button">
                      Confirmar Pedido
                    </Button>
                  )}
                </section>
              </S.ContainerResult>
            </S.ContainerPurchaseListSelected>
          </S.Content>
        )}
      </S.ContainerPage>
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

export default Purchase;
