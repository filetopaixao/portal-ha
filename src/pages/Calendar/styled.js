import styled from 'styled-components';
import { CardStatus } from '@components/atoms/Cards/styled';
import { Text } from '@components/atoms';
import * as S from '../../components/molecules/Welcome/styled';

export const Container = styled.div`
  justify-content: center;

  table.fc-scrollgrid-sync-table,
  .fc-media-screen {
    width: 100% !important;
    height: 100% !important;
  }
  .fc-daygrid-day-events {
    min-height: 1em !important;
  }
  .fc-daygrid-body {
    width: 100% !important;
  }
  .fc-col-header-cell {
    font-size: 13px;

    border: 1px solid #01a3ff !important;

    background: #01a3ff;
    color: #ffffff;
  }
  .fc .fc-daygrid-day-frame {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    max-height: 40px !important;

    @media (min-width: 992px) {
      min-height: 70px !important;
    }
  }

  .fc-event-time {
    display: none;
  }

  .fc-daygrid-day {
    padding-bottom: 0px !important;
  }
  .fc-prev-button,
  .fc-next-button {
    font-size: 2em !important;
    line-height: 0.6em !important;

    padding: unset !important;

    border: none !important;

    color: #5e5e5e !important;
    background: none !important;
  }

  .fc-prev-button:hover,
  .fc-next-button:hover,
  .fc-prev-button:active,
  .fc-next-button:active {
    font-size: 2em !important;
    line-height: 0.6em !important;
    outline: none !important;

    padding: unset !important;

    border: none !important;

    color: #5e5e5e !important;
    background: none !important;
  }

  ::-webkit-scrollbar {
    width: 7px;
  }

  ::-webkit-scrollbar-track {
    border-radius: 10px;

    box-shadow: inset 0 0 5px grey;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;

    background: rgb(136, 136, 136);
  }

  ::-webkit-scrollbar-thumb:hover {
    background: #6b6b6b;
  }

  .item-title {
    margin-bottom: 1px;
  }

  .fc-daygrid-event {
    width: 10px;
    height: 10px;

    border-radius: 30px;
  }

  @media screen and (max-width: 700px) {
    .fc-daygrid-event {
      margin-left: 50% !important;
    }
    .fc-col-header-cell {
      font-size: 0.7em !important;
    }
  }

  .table-bordered td {
    border: none;
  }

  .fc-scroller {
    overflow: hidden !important;
  }

  .fc-view-harness-active {
    padding-bottom: 110% !important;
  }

  .fc-toolbar-title {
    font-size: 1.2em !important;

    color: #01a3ff !important;
  }
  .fc-toolbar-title::before {
    content: '\A' !important;
  }

  .fc .fc-scrollgrid-liquid {
    height: 265px !important;
    @media (min-width: 992px) {
      height: 450px !important;
    }
  }

  .fc-day-today {
    background-color: #e1dede57 !important;
  }

  .fc-col-header {
    width: 100% !important;
  }

  .fc-daygrid-day-number {
    color: #5e5e5e !important;
  }

  .fc-daygrid-event {
    width: unset !important;
    height: unset !important;
    border-radius: unset !important;
    @media screen and (max-width: 992px) {
      margin-top: -10px !important;
      margin-left: 5px !important;
    }
  }
  .fc-daygrid-dot-event:hover,
  .fc-daygrid-dot-event.fc-event-mirror {
    background: none;
  }
`;

export const HalfContainer = styled.div`
  justify-content: center;

  width: 50%;
  max-height: 62vh;

  overflow-y: auto;

  @media screen and (max-width: 700px) {
    width: 100% !important;

    margin-top: 10px;
  }
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;

  margin-top: 3em;
`;

export const CalendarGrid = styled(CardStatus)`
  max-width: 48% !important;
  max-height: 50% !important;
  height: 395px !important;

  margin-left: 2%;

  overflow: hidden;

  @media (min-width: 992px) {
    height: 570px !important;
  }

  @media screen and (max-width: 700px) {
    max-width: 100% !important;
    width: 102% !important;
  }
`;

export const Item = {
  pink: styled(CardStatus)`
    display: block;

    width: 50em;
    height: auto;
    max-width: 100% !important;
    max-height: 11em !important;

    margin-bottom: 15px;
    padding: 15px 30px;

    border-left: 15px solid #f14479;

    @media screen and (max-width: 700px) {
      item-event {
        font-size: 10px !important;
      }
    }
  `,
  green: styled(CardStatus)`
    display: block;

    width: 50em;
    height: auto;
    max-width: 100% !important;
    max-height: 11em !important;

    margin-bottom: 15px;
    padding: 15px 30px;

    border-left: 15px solid #2fdf46;

    @media screen and (max-width: 700px) {
      item-event {
        font-size: 10px !important;
      }
    }
  `,
  yellow: styled(CardStatus)`
    display: block;

    width: 50em;
    height: auto;
    max-width: 100% !important;
    max-height: 11em !important;

    margin-bottom: 15px;
    padding: 15px 30px;

    border-left: 15px solid #f6b546;

    @media screen and (max-width: 700px) {
      item-event {
        font-size: 10px !important;
      }
    }
  `,
  blue: styled(CardStatus)`
    display: block;

    width: 50em;
    height: auto;
    max-width: 100% !important;
    max-height: 11em !important;

    margin-bottom: 15px;
    padding: 15px 30px;

    border-left: 15px solid #01a3ff;

    @media screen and (max-width: 700px) {
      item-event {
        font-size: 10px !important;
      }
    }
  `,
};

export const Dates = {
  pink: styled(Text)`
    text-transform: uppercase;

    color: #f14479;
  `,
  green: styled(Text)`
    text-transform: uppercase;

    color: #2fdf46;
  `,
  yellow: styled(Text)`
    text-transform: uppercase;

    color: #f6b546;
  `,
  blue: styled(Text)`
    text-transform: uppercase;

    color: #01a3ff;
  `,
};

export const Content = styled.div`
  padding: 5px 25px;

  color: #5e5e5e;
`;

export const ContainerStatus = styled(S.Welcome)`
  margin-top: 1.3% !important;
  @media screen and (max-width: 700px) {
    ::after {
      height: 208px !important;
      width: 150px !important;

      right: 15px !important;
    }
  }
`;

export const WelcomeCalendar = styled.div`
  width: 390px;
  height: 280px;

  position: absolute;
  right: 0;
  top: -20px;
`;

export const Img = styled.img`
  width: 100%;
  height: auto;
`;

export const EmptyEvents = styled.div`
  margin: 10px 0;
  padding: 8px;

  border-radius: 10px;

  background-color: #fff;

  p {
    color: #f14479;
  }
`;

export const TitleCalendar = styled.div`
  font-size: 22px;
  text-align: center;

  h2 {
    text-transform: capitalize;
    color: #01a3ff;
  }

  p {
    font-size: 16px;

    color: #5e5e5e;
  }
`;
