import styled from 'styled-components';

import NotFound from '@assets/images/erro-404.svg';
import PageLoadding from '@assets/images/page_loadding.gif';

export const Container = styled.div`
  > div:nth-child(2) {
    margin: 73px auto 62px;

    &::after {
      height: 284px;
      width: 364px;

      right: 50px;
      top: -73px;

      @media (max-width: 1051px) {
        top: -13px;
      }

      @media (max-width: 720px) {
        height: 115px;
        width: 148px;

        right: 0px;
        top: 110px;
      }
    }

    @media (max-width: 720px) {
      margin: 20px auto 24px;
    }
  }

  > div:nth-child(3) {
    margin-top: 30px;
  }

  div.filter__container__search {
    @media (max-width: 390px) {
      margin-left: 10px;
    }
  }
`;

export const Content = styled.div`
  margin: 25px 0;

  min-height: 400px;
`;

export const ButtonGroupCategoriesFilter = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;

  color: #5e5e5e;

  > div {
    width: 350px;

    @media (max-width: 800px) {
      width: 100%;
    }
  }

  @media (max-width: 800px) {
    display: block;

    > div:first-child {
      margin-bottom: 20px;
    }
  }
`;

export const SelectContainerCat = styled.div`
  select {
    text-transform: uppercase;
    display: block;
    width: 100%;

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    opacity: 0.7;

    background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

    cursor: pointer;
  }
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  font: normal normal normal 14px Poppins;
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
