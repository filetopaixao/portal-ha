import styled, { css } from 'styled-components';

import Lnk from '@components/atoms/Link';
import Txt from '@components/atoms/Text';

const selectedProduct = {
  yes: () => css`
    content: '';
    clear: both;
    display: block;
    visibility: visible;
    align-self: center;

    height: 100%;
    width: 100%;

    position: absolute;
    right: 0px;
    bottom: 0;

    border-radius: 8px;

    background: #e5e5e5;
    opacity: 0.5;
    background-size: contain;
  `,
};

export const ContentProductList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 20px;
  grid-row-gap: 50px;
  width: 100%;

  > div.product {
    margin: 0 auto;

    max-width: 220px;
    width: 100%;

    border-radius: 15px;

    background: inherit;
    box-shadow: inherit;

    > div.product__container {
      max-height: 320px;
      width: 100%;

      img {
        margin: 0 auto;

        width: 96px;
        height: 96px;

        @media (min-width: 600px) {
          width: 140px;
          height: 140px;
        }
      }
    }
  }

  @media (max-width: 1300px) and (min-width: 600px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    max-width: 500px;
    margin: 0 auto;
  }

  @media (min-width: 1300px) {
    max-width: 1800px;
    margin: 0 auto;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr 1fr;
    width: 100%;
  }
`;

export const Product = styled.div`
  position: relative;

  max-width: 250px;
  width: 100%;

  background: #ffffff 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 10px #00000029;
  border-radius: 10px;

  &::after {
    ${(props) => (props.isSelected ? selectedProduct.yes : '')}
  }

  ${(props) => props.purchase && 'cursor: pointer;'}

  p {
    @media (max-width: 1400px) {
      font-size: 10px;
    }
  }

  @media (min-width: 2000px) {
    width: 80%;
  }

  @media (min-width: 992px) and (max-width: 2000px) {
    margin: 0 auto;

    max-width: 281px;
    width: 100%;

    border-radius: 15px;
  }
`;

export const ProductContainer = styled.div`
  width: 100%;

  padding: 10px;

  border-radius: 10px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
`;

export const ProductTitle = styled.h3`
  font-size: 10px;
  font-weight: 600;
  text-align: center;

  width: 100%;

  margin-bottom: 15px;

  color: ${(props) => (props.theme === 'Dark' ? '#FFFFFF' : '#82868D')};

  @media (min-width: 800px) {
    font-size: 14px;
  }
`;

export const ProductImage = styled.div`
  width: 100%;

  img {
    margin: 0 auto;
    height: auto;
    height: 100%;
    width: 100%;

    max-height: 170px;
    max-width: 170px;

    @media (min-width: 992px) {
      max-width: 140px;
      height: 140px;
    }
  }
`;

export const ProductValues = styled.div`
  display: flex;
  justify-content: space-between;

  width: 100%;

  &.product__value {
    margin-top: 15px;

    @media (min-width: 992px) {
      margin-top: 20px;
    }
  }
`;

export const TextValue = styled.p`
  font-weight: 600;
  font-size: 10px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    font-size: 12px;
  }
`;

export const ButtonGroupCopyShare = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

export const ProducInfo = styled.div`
  display: flex;
  justify-content: space-between;

  margin-top: 15px;
  width: 100%;

  p {
    height: 25px;
    width: 25px;

    @media (max-width: 600px) {
      height: 15px;
      width: 15px;
    }
  }

  p:not(:last-child) {
    margin-right: 10px;
  }
`;

export const ProductLink = styled(Lnk)`
  font-size: 12px;
  font-weight: 500;

  background: ${(props) => `${props.primaryColor}`};

  @media (max-width: 600px) {
    font-size: 8px;
  }
`;

export const ProductTextClick = styled(Txt)`
  cursor: pointer;
`;
