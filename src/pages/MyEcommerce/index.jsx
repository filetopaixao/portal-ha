/* eslint-disable import/order */
import React, { useState, useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';

import BannerEcommerce from '@components/molecules/Welcome/banner';
import FilterComponent from '@components/molecules/Filter';
import Pagination from '@components/molecules/Pagination';

import Products from './ProductList';
import ModalProduct from './ProductDetail';
import { Text } from '@components/atoms';
import SelectCategories from '@components/atoms/Select/selectCategories';
import getAll from '../../services/fpass/products';

import * as S from './styled';

const categoriesFpass = [
  {
    id: 0,
    name: 'Todos',
    type: '',
  },
  {
    id: 1,
    name: 'Cursos',
    type: 'product',
  },
  {
    id: 2,
    name: 'Planos',
    type: 'plan',
  },
  {
    id: 3,
    name: 'Programas',
    type: 'collection',
  },
];

const MyEcommerce = () => {
  const themeStore = useSelector((state) => state.theme);

  const [productsEcommerce, setProductsEcommerce] = useState([]);
  const [currentProductsEcommerce, setCurrentProductsEcommerce] = useState([]);
  const [errorLoadProducts, setErrorLoadProducts] = useState(false);

  const linkEver = 'https://loja.fpass.com.br?me=1';
  const image = 'ecommerce';
  const BannerTitle = 'Plataforma FPASS';
  const imageMobile = false;

  const [filterText, setFilterText] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [productSelected, setProductSelected] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const handleOpenModalProduct = (op) => {
    setTimeout(() => {
      setIsModalVisible(op);
    }, 1000);
  };

  const handleSelectProduct = async (product) => {
    console.log(product);
    setProductSelected(product);
  };

  useEffect(() => {
    getAll()
      .then((response) => {
        console.log(response.data);
        setProductsEcommerce(response.data);
        setIsLoading(false);
      })
      .catch((getAllError) => {
        setErrorLoadProducts(true);
        console.log(getAllError);
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    const filteredItems = productsEcommerce.filter(
      (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
    );
    if (filteredItems) {
      setCurrentProductsEcommerce(filteredItems);
    }
  }, [filterText, productsEcommerce]);

  function handleSelectedCategoryFpass(textFilter) {
    console.log(textFilter);
    const filteredItems = productsEcommerce.filter(
      (item) => item.type && item.type.toLowerCase().includes(textFilter.toLowerCase())
    );
    setCurrentProductsEcommerce(filteredItems);
  }

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentProducts = currentProductsEcommerce.slice(indexOfFirst, indexOfLast);

  if (isLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.Container>
        <BannerEcommerce link={linkEver} image={image} title={BannerTitle} imageMobile={imageMobile} />
        <S.ButtonGroupCategoriesFilter>
          <S.SelectContainerCat theme={themeStore.themeName}>
            <Text size="sm" variant="normal">
              FILTRAR POR
            </Text>

            <SelectCategories
              name="categories"
              id="categories"
              options={categoriesFpass}
              defaultValue="Escolha a categoria do seu produto"
              onChange={(e) => handleSelectedCategoryFpass(e.target.value)}
            />
          </S.SelectContainerCat>
          <FilterComponent
            onFilter={(e) => setFilterText(e.target.value)}
            filterText={filterText}
            theme={themeStore.themeName}
          />
        </S.ButtonGroupCategoriesFilter>
        <S.Content>
          {errorLoadProducts ? <h2>Erro no servidor</h2> : null}
          {!productsEcommerce && errorLoadProducts === false ? (
            <h1>Carregando</h1>
          ) : (
            <>
              {productsEcommerce.length <= 0 ? (
                <S.ContainerBackEndInfo>
                  <S.InfoBackEnd theme={themeStore.themeName}>Selecione uma categoria...</S.InfoBackEnd>
                  {/* <S.ImageNotFound /> */}
                </S.ContainerBackEndInfo>
              ) : (
                <Products
                  selectedProduct={handleSelectProduct}
                  data={currentProducts}
                  openModal={handleOpenModalProduct}
                />
              )}
            </>
          )}
        </S.Content>
        <Pagination currentPage={currentPage} perPage={perPage} total={productsEcommerce.length} paginate={paginate} />
      </S.Container>
      {productSelected && (
        <ModalProduct
          product={productSelected}
          description={productSelected.description}
          allerg={[]}
          benef={[]}
          charac={[]}
          contraind={[]}
          indic={[]}
          ingred={[]}
          usageSug={[]}
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
        />
      )}
    </>
  );
};

export default MyEcommerce;
