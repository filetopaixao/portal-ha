import styled from 'styled-components';
import { shade } from 'polished';

import ArrowTop from '@assets/images/arrowTop.svg';

export const ContainerCart = styled.div`
  display: block;
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 20px;

  width: 100%;

  grid-template-areas: 'resume' 'create';
  @media (max-width: 1200px) {
    .resume {
      grid-area: resume;
    }

    .create {
      grid-area: create;
    }
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    column-gap: 20px;
  }
`;

export const ContainerCartCreate = styled.form`
  width: 100%;
  padding: 30px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const TitleCreateCupom = styled.h3`
  font-size: 18px;
  text-align: center;
  margin-bottom: 20px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const ContainerCartResume = styled.div`
  width: 100%;
  padding: 30px;

  border-radius: 15px;

  background: ${(props) =>
    props.tertiaryColor
      ? `transparent linear-gradient(145deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
      : `transparent linear-gradient(145deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};
`;

export const InputContainer = styled.div`
  display: block;

  input {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    ::placeholder {
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }
  }
`;

export const Title = styled.h4`
  font-weight: lighter;
  color: #5e5e5e;
`;

export const InputTitle = styled.h4`
  font-weight: lighter;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const ContainerOps = styled.div`
  display: block;
`;

export const TitleCategories = styled.label`
  font: normal 14px;
  font-weight: lighter;
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const ContainerSelectDiscount = styled.div`
  font-size: 12px;
  display: flex;
  column-gap: 20px;
  align-items: center;

  padding: 30px;

  width: 100%;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  p {
    text-align: left;
    font-size: 14px;
    letter-spacing: 0px;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const SecondOpDiscount = styled.div`
  display: grid;
  column-gap: 20px;
  grid-template-columns: 1.5fr 50px;
  align-items: center;
`;

export const InputNumberDiscount = styled.div`
  display: grid;
  grid-template-columns: auto 0.3fr 1fr;
  align-items: center;
  column-gap: 10px;

  padding: 5px 10px;

  height: 40px;
  width: 90px;

  border-radius: 9px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  input {
    border: none;
    background: transparent;

    width: 100%;

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  p {
    text-align: center;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  button {
    display: block;
    border: none;
    background: transparent;
    cursor: pointer;
  }

  button:first-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
    }
  }

  button:last-child {
    &:after {
      content: '';
      clear: both;
      display: block;
      visibility: visible;
      align-self: center;

      height: 10px;
      width: 10px;

      position: relative;
      right: 0px;
      bottom: 0;
      opacity: 1;
      z-index: 1;

      background: url(${ArrowTop}) no-repeat;
      background-size: contain;
      transform: rotate(180deg);
    }
  }
`;

export const ButtonGroupAddOrRemove = styled.div``;

export const CreateCartButton = styled.button`
  text-transform: uppercase;
  //font-weight: bold;
  font-size: 17px;

  height: 53px;
  width: 100%;

  margin: 0 auto;

  border: none;
  border-radius: 15px;

  color: #fff;
  background: ${(props) => props.primaryColor};
  box-shadow: 0px 3px 6px #00000029;

  &:hover {
    background: ${(props) => `${shade(0.2, props.primaryColor)}`};
  }
`;

export const ButtonCancel = styled.button`
  text-align: center;
  text-transform: uppercase;

  width: 100px;
  margin: 0 auto;

  cursor: pointer;
  border: none;
  background: transparent;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ButtonGroupCreate = styled.div`
  margin-top: 5px;

  text-align: center;
`;

export const CodContainer = styled.div`
  height: 40px;
`;

export const ContainerCampaing = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  flex-direction: column;

  margin: 0 auto;
  margin-top: 20px;

  height: 50%;

  p,
  h2 {
    text-transform: uppercase;
    text-align: center;
    color: #fff;
  }

  h2 {
    font-weight: bold;
  }
`;

export const ImageCampaing = styled.img`
  display: block;

  width: 222px;
  height: 190px;

  margin: 0 auto;

  background-repeat: no-repeat;
  background-size: contain;
`;

export const TitleCartResume = styled.p`
  font-size: 14px;
  font-weight: lighter;
  color: #fff;
`;

export const ContainerListProducts = styled.div``;

export const TitleListProducts = styled.p`
  font-size: 14px;
  font-weight: lighter;
  color: #fff;
`;

export const ContainerResult = styled.div`
  width: 100%;

  display: flex;
  justify-content: center;

  .container-result-left {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    margin-right: 30px;
    text-align: right;
  }

  .container-result-right {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin-right: 30px;
    text-align: left;
  }
`;

export const ResultResumeInfo = styled.div`
  p {
    font-size: 14px;
    color: #fff;
    font-weight: lighter;
  }

  h3 {
    font-size: 20px;
    font: normal;
    color: #fff;
    font-weight: lighter;
  }

  line-height: 30px;
`;

export const ResultContainer = styled.div`
  display: flex;
  align-items: center;

  h4 {
    margin-right: 15px;
  }
`;

export const ContainerForm = styled.div`
  @media (min-width: 800px) {
    > div:nth-child(n) {
      margin-bottom: 20px;
    }
  }
`;

export const InputCodCoupon = styled.input`
  height: 40px;
  width: 100%;

  padding: 3px 20px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ButtonGroupDate = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  column-gap: 15px;
  row-gap: 15px;
  align-items: center;

  input {
    padding: 3px 20px;
    font: normal normal normal 14px Poppins;
    letter-spacing: 0px;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  @media (min-width: 800px) {
    grid-template-columns: 1fr 1fr;
    column-gap: 15px;
  }
`;

export const InputDate = styled.input`
  height: 40px;
  width: 100%;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::-webkit-calendar-picker-indicator {
    filter: invert(0.5);
  }
`;

export const SelectContainer = styled.div`
  select {
    height: 40px;
    width: 100%;

    padding: 3px 20px;

    border-radius: 15px;
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;
