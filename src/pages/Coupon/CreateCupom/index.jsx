import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import { Actions as CouponsActions } from '@redux/ducks/coupon';

import ConfirmationSuccess from '@components/molecules/Modals/ModalConfSuccess';
import Select from '@components/atoms/Select';

// import Coupon from '@assets/images/cupom.svg';

import { addCoupon } from '../../../services/coupon';

import * as S from './styled';

const opsQtdPurchase = [
  {
    value: '1',
    label: '1',
  },
  {
    value: '10',
    label: '10',
  },
  {
    value: '20',
    label: '20',
  },
  {
    value: '50',
    label: '50',
  },
];

const CupomCreate = () => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();
  const [isModalConfSuccess, setIsModalConfSuccess] = useState(false);

  const dispatch = useDispatch();
  const couponsStore = useSelector((state) => state.coupons);

  const [discountValue, setDiscountValue] = useState('0');
  const [codigoCoupon, setCodigoCoupon] = useState('');
  const [dateInitial, setDateInitial] = useState('');
  const [dateFinal, setDateFinal] = useState('');
  const [qtdPurchase, setQtdPurchase] = useState('');

  useEffect(() => {
    dispatch(CouponsActions.getCodCoupon());
    setCodigoCoupon(couponsStore.setCodCoupon);
  }, [dispatch, couponsStore.setCodCoupon]);

  const handleAddCoupon = async (e) => {
    e.preventDefault();
    const newCoupon = {
      code: codigoCoupon,
      valid_from: dateInitial,
      valid_until: dateFinal,
      quantity: qtdPurchase,
      discount: discountValue,
    };
    dispatch(CouponsActions.loadCouponAdded(newCoupon));
    await addCoupon(newCoupon);
    setDiscountValue('0');
    setCodigoCoupon('');
    setDateInitial('');
    setDateFinal('');
    setQtdPurchase('');
  };

  const criar = () => {
    history.push('/cupom');
  };

  const handleIncrement = () => {
    setDiscountValue(parseFloat(discountValue) + 1);
  };

  const handleReduce = () => {
    setDiscountValue(parseFloat(discountValue) - 1);
  };

  const handleActiveConfSuccess = () => {
    setIsModalConfSuccess(!isModalConfSuccess);
  };

  const handleSelectCod = (value) => {
    const newValue = value.replace(/\s/g, '');
    setCodigoCoupon(newValue);
  };

  return (
    <>
      <S.ContainerCart>
        <S.ContainerCartCreate onSubmit={(e) => handleAddCoupon(e)} className="create" theme={themeStore.themeName}>
          <S.ContainerOps>
            <S.TitleCreateCupom theme={themeStore.themeName}>Crie um novo Cupom de Desconto</S.TitleCreateCupom>
            <S.ContainerForm>
              <S.InputContainer theme={themeStore.themeName}>
                <S.InputTitle theme={themeStore.themeName}> Código do cupom:</S.InputTitle>
                <S.InputCodCoupon
                  type="text"
                  name="CodCounpon"
                  id="CodCounpon"
                  placeholder="Digite o código"
                  onChange={(e) => handleSelectCod(e.target.value)}
                  value={codigoCoupon}
                  theme={themeStore.themeName}
                />
              </S.InputContainer>

              <S.ButtonGroupDate>
                <S.InputContainer>
                  <S.InputTitle theme={themeStore.themeName}>Iniciou em:</S.InputTitle>
                  <S.InputDate
                    type="date"
                    name="dateIni"
                    id="dateIni"
                    onChange={(e) => setDateInitial(e.target.value)}
                    theme={themeStore.themeName}
                  />
                </S.InputContainer>

                <S.InputContainer>
                  <S.InputTitle theme={themeStore.themeName}>Encerra em:</S.InputTitle>
                  <S.InputDate
                    theme={themeStore.themeName}
                    type="date"
                    name="dateFim"
                    id="dateFim"
                    onChange={(e) => setDateFinal(e.target.value)}
                  />
                </S.InputContainer>
              </S.ButtonGroupDate>
              <S.SelectContainer theme={themeStore.themeName}>
                <S.InputTitle theme={themeStore.themeName}>Quantas compras podem aplicar o cupom:</S.InputTitle>
                <Select
                  name="categories"
                  id="categories"
                  options={opsQtdPurchase}
                  defaultValue="Escolha a quantidade"
                  onChange={(e) => {
                    setQtdPurchase(e.target.value);
                  }}
                  value={qtdPurchase}
                />
              </S.SelectContainer>
            </S.ContainerForm>

            <S.TitleCategories theme={themeStore.themeName}>
              Transformar comissão em desconto para esse cupom
            </S.TitleCategories>
            <S.ContainerSelectDiscount theme={themeStore.themeName}>
              <S.SecondOpDiscount>
                <p>Desconto de</p>
                <S.InputNumberDiscount className="btn-bigger-one" theme={themeStore.themeName}>
                  <input onChange={(e) => setDiscountValue(e.target.value)} type="text" value={discountValue} />
                  <span>%</span>
                  <S.ButtonGroupAddOrRemove className="btn-group">
                    <button onClick={() => handleIncrement()} type="button">
                      {' '}
                    </button>
                    <button onClick={() => handleReduce()} type="button">
                      {' '}
                    </button>
                  </S.ButtonGroupAddOrRemove>
                </S.InputNumberDiscount>
              </S.SecondOpDiscount>
            </S.ContainerSelectDiscount>
          </S.ContainerOps>

          <S.ButtonGroupCreate>
            <S.CreateCartButton
              type="submit"
              variant="primary"
              onClick={() => handleActiveConfSuccess()}
              primaryColor={themeStore.primaryColor}
            >
              Criar cupom desconto
            </S.CreateCartButton>
            <S.ButtonCancel theme={themeStore.themeName} type="button" onClick={() => history.push('/cupom')}>
              Cancelar
            </S.ButtonCancel>
          </S.ButtonGroupCreate>
        </S.ContainerCartCreate>

        <S.ContainerCartResume
          className="resume"
          primaryColor={themeStore.primaryColor}
          secondaryColor={themeStore.secondaryColor}
          tertiaryColor={themeStore.tertiaryColor}
        >
          <S.TitleCartResume>Visualização do Cupom</S.TitleCartResume>

          <S.ContainerCampaing>
            <p>Código do cupom:</p>
            <S.CodContainer>
              <h2>{codigoCoupon}</h2>
            </S.CodContainer>
          </S.ContainerCampaing>

          <S.ContainerListProducts>
            <S.ContainerResult>
              <div className="container-result-left">
                <S.ResultResumeInfo class="result-resume-info">
                  <p>Válido de:</p>
                  <h3>{dateInitial}</h3>
                </S.ResultResumeInfo>

                <S.ResultResumeInfo class="result-resume-info">
                  <p>Quantidade de Cupons:</p>
                  <h3>{qtdPurchase}</h3>
                </S.ResultResumeInfo>

                <S.ResultResumeInfo class="result-resume-info">
                  <p>Desconto:</p>
                  <h3>{discountValue}%</h3>
                </S.ResultResumeInfo>
              </div>

              <div className="container-result-right">
                <S.ResultResumeInfo className="result-resume-info">
                  <p>Até:</p>
                  <h3>{dateFinal}</h3>
                </S.ResultResumeInfo>

                <S.ResultResumeInfo className="result-resume-info">
                  <p>Produto:</p>
                </S.ResultResumeInfo>
              </div>
            </S.ContainerResult>
          </S.ContainerListProducts>
        </S.ContainerCartResume>
      </S.ContainerCart>
      <ConfirmationSuccess
        functionExecute={criar}
        isModalConfSuccess={isModalConfSuccess}
        setIsModalConfSuccess={setIsModalConfSuccess}
        msg1="Cupom de desconto gerado com sucesso!"
        msg2="Divulgue e acompanhe os resultados"
      />
    </>
  );
};

export default CupomCreate;
