import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { Actions as CouponsActions } from '@redux/ducks/coupon';

import ConfirmationDelete from '@components/molecules/Modals/ModalConf';
import ConfirmationSuccess from '@components/molecules/Modals/ModalConfSuccess';
import Select from '@components/atoms/Select';

import Modal from '@components/molecules/Modals/ModalComponent';

import { updateCoupon, deleteCoupon } from '../../../services/coupon';

import * as S from './styled';

const opsQtdPurchase = [
  {
    value: '1',
    label: '1',
  },
  {
    value: '10',
    label: '10',
  },
  {
    value: '30',
    label: '30',
  },
  {
    value: '20',
    label: '20',
  },
  {
    value: '50',
    label: '50',
  },
];

const EditCoupon = ({ coupon, isModalVisible, setIsModalVisible }) => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalDelConf, setIsModalDelConf] = useState(false);
  const [isModalConfSuccess, setIsModalConfSuccess] = useState(false);

  const [couponSelected, setCouponSelected] = useState({});

  const dispatch = useDispatch();

  const [discountValue, setDiscountValue] = useState('0');
  const [codigoCoupon, setCodigoCoupon] = useState('');
  const [dateInitial, setDateInitial] = useState('');
  const [dateFinal, setDateFinal] = useState('');
  const [qtdPurchase, setQtdPurchase] = useState('');

  useEffect(() => {
    setCodigoCoupon(coupon.code);
    setDateInitial(coupon.valid_from);
    setDateFinal(coupon.valid_until);
    setQtdPurchase(coupon.quantity);
    setDiscountValue(coupon.discount);
  }, [coupon]);

  const handleActiveDelConf = (item) => {
    setCouponSelected(item);
    setIsModalDelConf(!isModalDelConf);
  };

  const handleDelConf = async () => {
    console.log({ 'cupom deletado': couponSelected });
    await deleteCoupon(couponSelected.id);
    await dispatch(
      CouponsActions.deletedCoupon({
        id: coupon.id,
        discount: discountValue,
        valid_from: dateInitial,
        valid_until: dateFinal,
        quantity: qtdPurchase,
      })
    );
    setIsModalVisible(!isModalVisible);
    setIsModalDelConf(!isModalDelConf);
  };

  const handleActiveConfSuccess = async () => {
    const newCoupon = {
      discount: discountValue,
      valid_from: dateInitial,
      valid_until: dateFinal,
      quantity: qtdPurchase,
    };
    await dispatch(
      CouponsActions.editedCoupon({
        id: coupon.id,
        discount: discountValue,
        valid_from: dateInitial,
        valid_until: dateFinal,
        quantity: qtdPurchase,
      })
    );

    await updateCoupon(coupon.id, newCoupon);
    console.log({ 'cupon editado': newCoupon });

    setIsModalConfSuccess(!isModalConfSuccess);
  };

  const handleDesativeConfSuccess = () => {
    setIsModalConfSuccess(!isModalConfSuccess);
    setIsModalVisible(!isModalVisible);
    window.location.href = '/cupom';
  };

  return (
    <>
      <Modal
        variant="md"
        alignCenter
        title=""
        isVisible={isModalVisible}
        onClose={() => setIsModalVisible(!isModalVisible)}
        MaxHeight="600px"
        ContentHeight="auto"
      >
        <S.ModalContent coupon={coupon}>
          <S.ModalHeader theme={themeStore.themeName}>
            <h3>Editar Cupom de Desconto</h3>
          </S.ModalHeader>
          <S.ModalBody>
            <S.ContainerForm>
              <div>
                <S.InputTitle theme={themeStore.themeName}> Código do cupom:</S.InputTitle>
                <S.InputCodCoupon
                  type="text"
                  name="CodCounpon"
                  id="CodCounpon"
                  placeholder="Digite o código"
                  onChange={(e) => setCodigoCoupon(e.target.value)}
                  value={codigoCoupon}
                  theme={themeStore.themeName}
                />
              </div>

              <S.ButtonGroupDate theme={themeStore.themeName}>
                <div>
                  <S.InputTitle theme={themeStore.themeName}>Iniciou em:</S.InputTitle>
                  <S.InputDateIni
                    type="date"
                    name="dateIni"
                    id="dateIni"
                    onChange={(e) => setDateInitial(e.target.value)}
                    value={dateInitial}
                    disabled
                    theme={themeStore.themeName}
                  />
                </div>

                <div>
                  <S.InputTitle theme={themeStore.themeName}>Encerra em:</S.InputTitle>
                  <S.InputDateFim
                    type="date"
                    name="dateFim"
                    id="dateFim"
                    onChange={(e) => setDateFinal(e.target.value)}
                    value={dateFinal}
                    theme={themeStore.themeName}
                  />
                </div>
              </S.ButtonGroupDate>
              <S.SelectContainer theme={themeStore.themeName}>
                <S.InputTitle theme={themeStore.themeName}>Quantas compras podem aplicar o cupom:</S.InputTitle>
                <Select
                  name="opsQtdPurchase"
                  id="opsQtdPurchase"
                  options={opsQtdPurchase}
                  onChange={(e) => {
                    setQtdPurchase(e.target.value);
                  }}
                  defaultValue={coupon.quantity}
                />
              </S.SelectContainer>
            </S.ContainerForm>
            <S.ResultContainer theme={themeStore.themeName}>
              <S.InputTitle theme={themeStore.themeName}>Bônus do cupom:</S.InputTitle>
              <p>Desconto {discountValue} %</p>
            </S.ResultContainer>
            <S.ButtonGroup>
              <S.ButtonDelete onClick={() => handleActiveDelConf(coupon)} type="button">
                Excluir Cupom
              </S.ButtonDelete>
              <S.ButtonSave
                onClick={() => handleActiveConfSuccess()}
                type="submit"
                primaryColor={themeStore.primaryColor}
              >
                Salvar Alterações
              </S.ButtonSave>
            </S.ButtonGroup>
          </S.ModalBody>
        </S.ModalContent>
      </Modal>
      <ConfirmationDelete
        exeFunctionAction={handleDelConf}
        isModalDelConf={isModalDelConf}
        setIsModalDelConf={setIsModalDelConf}
        msg1="Tem certeza que deseja excluir este cupom de desconto?"
        msg2="Ao excluir o cupom não estará mais disponível para utilização!"
      />
      <ConfirmationSuccess
        functionExecute={handleDesativeConfSuccess}
        isModalConfSuccess={isModalConfSuccess}
        setIsModalConfSuccess={setIsModalConfSuccess}
        msg1="Alterações realizadas com sucesso!"
        msg2="As configurações do cupom foram alteradas"
      />
    </>
  );
};

EditCoupon.propTypes = {
  coupon: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.object]),
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
};
EditCoupon.defaultProps = {
  coupon: {},
  isModalVisible: false,
  setIsModalVisible: () => {},
};

export default EditCoupon;
