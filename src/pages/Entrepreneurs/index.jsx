import React, { useState, useEffect, useCallback } from 'react';

import BannerEcommerce from '@components/molecules/Welcome/banner';

import Pagination from '@components/molecules/Pagination';
import ModalVideo from '@components/molecules/Modals/ModalVideo';
import MenuOpsFilterPrint from '@components/molecules/MenuOpsFilterPrint';

import Arrow from '@assets/images/arrowCheckout.svg';

import { getInfo } from '@services/entrepreneurs';
import { formateDate } from '@utils/formatDate';
import { formateValueBr } from '@utils/formatValueBr';

import * as S from './styled';

const Entrepreneurs = () => {
  const linkEver = 'https://loja.everbynature.com.br?me=1';
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';
  const image = 'entrepreneurs';
  const BannerTitle = 'Link novo Empreendedor';

  const [entrepreneurs, setEntrepreneurs] = useState([]);

  const [filterText, setFilterText] = React.useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const [activeLoading, setActiveLoading] = useState(true);

  useEffect(() => {
    getInfo()
      .then((response) => {
        const dataEntrepreneurs = response.data.data;
        console.log(response);
        if (dataEntrepreneurs) {
          const serializedEntrepreneurs = dataEntrepreneurs.map((item) => {
            return {
              id: item.id,
              name: item.name,
              indication: formateValueBr(item.indication_value),
              commission: formateValueBr(item.commission_value),
              cityUf: `${item.address.city}/${item.address.state}`,
              date: formateDate(item.commission_date),
              activeExpansed: false,
            };
          });
          setEntrepreneurs(serializedEntrepreneurs);
        }

        setActiveLoading(false);
      })
      .catch(() => {
        setEntrepreneurs([]);
      });
  }, []);

  const filteredItems = entrepreneurs.filter(
    (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentEntrepreneurs = filteredItems.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);

  const handleOpenModalVideo = (op) => {
    setIsModalVideoVisible(op);
  };

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    const newDatas = entrepreneurs.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setEntrepreneurs(newDatas);
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.Container>
        <BannerEcommerce
          openModalVideo={handleOpenModalVideo}
          link={linkEver}
          image={image}
          imageMobile
          title={BannerTitle}
        />
        <MenuOpsFilterPrint filterText={filterText} setFilterText={setFilterText} />

        <S.ContainerDataTable id="print">
          <p>Empreendedores</p>
          <S.TableHeader>
            <S.TextCell className="text-upp">NOME</S.TextCell>
            <S.TextCell className="text-upp off-1">VALOR DO PACOTE</S.TextCell>
            <S.TextCell className="text-upp">COMISSÃO RECEBIDA</S.TextCell>
            <S.TextCell className="text-upp off-2">CIDADE/ESTADO</S.TextCell>
            <S.TextCell className="text-upp off-3">DATA</S.TextCell>
          </S.TableHeader>
          <S.TableBody>
            <>
              {currentEntrepreneurs.length <= 0 ? (
                <S.ContainerBackEndInfo>
                  <S.InfoBackEnd>Não há empreendedores cadastrados</S.InfoBackEnd>
                  <S.ImageNotFound />
                </S.ContainerBackEndInfo>
              ) : (
                <>
                  {currentEntrepreneurs.map((item) => (
                    <>
                      <S.TableLine>
                        <S.TableCell className="text-upp">{item.name}</S.TableCell>
                        <S.TableCell className="text-upp off-1">{item.indication}</S.TableCell>
                        <S.TableCell className="text-upp">{item.commission}</S.TableCell>
                        <S.TableCell className="text-upp off-2">{item.cityUf}</S.TableCell>
                        <S.TableCell className="text-upp off-3">{item.date}</S.TableCell>
                        <S.TableCell className="text-upp off-button-set">
                          <S.Expansed href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                            <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                          </S.Expansed>
                        </S.TableCell>
                      </S.TableLine>
                      {item.activeExpansed ? (
                        <S.TableLineExpansed className="activeExpansed">
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>VALOR DO PACOTE</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.indication}</p>
                            </div>
                          </S.TableCellExpansed>
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>CIDADE/ESTADO</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.cityUf}</p>
                            </div>
                          </S.TableCellExpansed>
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>DATA</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.date}</p>
                            </div>
                          </S.TableCellExpansed>
                        </S.TableLineExpansed>
                      ) : null}
                    </>
                  ))}
                </>
              )}
            </>
          </S.TableBody>
        </S.ContainerDataTable>

        <Pagination currentPage={currentPage} perPage={perPage} total={entrepreneurs.length} paginate={paginate} />
      </S.Container>
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

export default Entrepreneurs;
