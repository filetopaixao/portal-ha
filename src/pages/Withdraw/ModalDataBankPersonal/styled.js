import styled from 'styled-components';
import { Button as Bt } from '@components/atoms/Button/styled';
import { Container as Ct } from '@components/atoms/Container/styled';

import IconCamera from '@assets/images/icon_camera.svg';
import IconWhatsapp from '@assets/images/whatsapp_icon.svg';
import IconInstagram from '@assets/images/instagram_icon.svg';
import IconFacebook from '@assets/images/facebook_icon.svg';
import IconYoutube from '@assets/images/youtube_icon.svg';
import BgBusinessCard from '@assets/images/bg_business_card.svg';

export const ModalContainer = styled.div`
  div#modal {
    background-color: #bfbfbfab;
    pointer-events: stroke;

    > div {
      @media (min-width: 600px) {
        max-width: 90%;
        min-height: 90%;
      }

      @media (max-width: 600px) {
        height: auto;
        max-width: 90%;
      }
    }
  }
`;

export const DataTableTitle = styled.h3`
  color: #5e5e5e;
`;

export const CardContainer = styled.div`
  > div#change_pass {
    // pointer-events: none;
    pointer-events: stroke;
  }

  > div#business_card {
    pointer-events: stroke;
  }

  > div:first-child {
    min-height: 375px;

    padding: 10px;

    border-radius: 10px;

    background: linear-gradient(to bottom, #0dbafb, #2551e1);
  }
`;

export const ModelTitle = styled.h2`
  text-align: center;
`;

export const GridButtons = styled.div`
  display: none;

  @media (min-width: 992px) {
    display: grid;

    width: 100%;

    grid-template-columns: ${(props) => {
      const width = 100 / props.qtd;
      let grid = '';
      for (let i = 0; i < props.qtd; i += 1) {
        grid += `${width}% `;
      }
      return `${grid}`;
    }};
  }
`;

export const GridCollapse = styled.div`
  display: block;

  margin-top: 20px;
  > div {
    > div:nth-child(2n + 1) {
      padding: 10px;
    }
  }

  /* > div:nth-child(2n + 1) {
    background: #d6d5d5;
  } */

  width: 100%;
  @media (min-width: 992px) {
    display: none;
  }
`;

export const ContainerChangePass = styled.div`
  padding: 20px;
`;

export const ContactBC = styled.div`
  display: flex;
  justify-content: flex-start;

  max-width: 150px;

  margin: 0 auto;
  margin-bottom: 5px;
`;

export const Whatsapp = styled.div`
  font-size: 9px;

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  background: url(${IconWhatsapp}) center center no-repeat;
  background-position: left;
`;

export const Instagram = styled.div`
  font-size: 9px;

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  background: url(${IconInstagram}) center center no-repeat;
  background-position: left;
`;

export const Facebook = styled.div`
  font-size: 9px;

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  background: url(${IconFacebook}) center center no-repeat;
  background-position: left;
`;

export const Youtube = styled.div`
  font-size: 9px;

  /* width: 100px; */
  height: 13px;

  padding-left: 18px;

  background: url(${IconYoutube}) center center no-repeat;
  background-position: left;
`;

export const AvatarBC = styled.div`
  width: 190px;
  height: 190px;

  border: 3px solid #ffffff;
  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const PersonName = styled.div`
  margin-top: 18px;
`;

export const Office = styled.div`
  p {
    font-size: 15px;
    text-transform: uppercase;
    text-align: center;

    margin-bottom: 25px;

    color: #404040;
  }
`;

export const Content = styled.main`
  display: none;

  @media (min-width: 992px) {
    display: block;
  }

  height: 400px;

  padding: 30px;

  border-radius: 10px;
  box-shadow: 0px 3px 6px #00000029;

  overflow-y: scroll;

  ::-webkit-scrollbar-track {
    background-color: #f4f4f4;
  }
  ::-webkit-scrollbar {
    width: 10px;

    border-radius: 10px;

    background: #f4f4f4;
  }
  ::-webkit-scrollbar-thumb {
    background: #dad7d7;
  }
`;

export const Container = styled(Ct)`
  display: block;

  @media (min-width: 992px) {
    display: grid;
    grid-template-columns: 1fr 3fr;
    grid-column-gap: 10px;
  }

  align-items: center;
`;

export const Column = styled.div`
  padding: 4px;

  button {
    padding: 0;
  }
`;

export const Button = styled(Bt)`
  font-size: 13px;
  width: 100%;

  &.btnGrid {
    border-radius: 15px 15px 0 0;

    ${(props) => (props.active ? 'background: #0fd3e663; color: #20aeda;' : 'background: #bdbdbd; color: #fff;')}
  }
`;

export const Welcome = styled.div`
  margin: 10px 0px;
  @media (min-width: 992px) {
    margin: 70px 0px;
    width: 100%;
  }
`;

export const ContainerWithdraw = styled.div`
  @media (min-width: 1280px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr;
    grid-gap: 20px;

    padding: 0px;
  }
`;

export const ContainerProfile = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: auto;

  padding: 35px;

  border-radius: 15px;

  background: #f2f2f2;

  flex: 100%;
  @media (min-width: 992px) {
    margin-right: 30px;

    flex: 35%;
  }
`;

export const Avatar = styled.div`
  width: 155px;
  height: 155px;

  margin: 0 auto;

  border-radius: 50%;

  background-image: ${(props) => `url(${props.photo})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const ButtonAccount = styled(Button)`
  font-size: 12px;

  width: 100%;

  margin-top: 30px;
  padding: 10px 0px;

  border-radius: 8px;

  &.change-card {
    margin-top: 35px;
  }

  &.password {
    margin-top: 30px;
    background: #bbf0f5;
    color: #20aeda;
  }
`;

export const BusinessCard = styled.main`
  width: 280px;
  height: auto;

  padding: 20px;

  background: url(${BgBusinessCard}) no-repeat;
  background-position: 0px -45px;
  background-size: 100%;

  box-shadow: 0px 6px 19px #00000029;
  p {
    font-size: 12px;
  }
`;

export const P = styled.p`
  font-size: 13px;
  text-align: center;

  color: #404040;
`;

export const LogoStyle = styled.img`
  width: 90px;
  height: 50px;
`;

export const ContentModal = styled.div`
  width: 100%;
`;

export const H5 = styled.h5`
  text-align: center;

  color: #404040;
`;

export const Link = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;

  margin: 10px 0 -25px 0;
`;

export const ContainerBusinessCard = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;

  margin-bottom: 20px;
`;

export const ContentBC = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;
`;

export const ContainerModal = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;

  margin-bottom: 30px;
`;

export const ContainerCenter = styled.div`
  display: flex;
  justify-content: center;
`;

export const H3 = styled.h3`
  text-align: center;

  color: #404040;
`;

export const LbUploadPhoto = styled.label`
  width: 32px;
  height: 32px;

  border-radius: 50%;

  background: url(${IconCamera}) center center no-repeat #fff;

  position: relative;
  float: right;
  top: 125px;
  cursor: pointer;
`;

export const UploadPhoto = styled.input`
  display: none;
`;

export const RequestWithdraw = styled.div`
  margin-bottom: 20px;

  @media (min-width: 1280px) {
    grid-column-start: 1;
    grid-column-end: 3;

    margin-bottom: 0px;
    max-width: none;
  }

  .purchase {
    background-image: linear-gradient(#6aff6f, #28bc8f);
  }
`;

export const Form = styled.form`
  margin-top: 15px;

  padding: 30px;

  border-radius: 15px;

  background-color: #ffffff;

  button {
    width: 100%;
    margin-top: 20px;
  }

  button:nth-child(4n) {
    padding: 15px;
  }

  label {
    margin-left: 7px;
    margin-bottom: 3px;
  }
`;

export const MinimumValue = styled.p`
  font-size: 10px;
  font-weight: bold;

  margin: 3px 0px 20px 7px;

  color: #7c7c7c;
`;

export const BankAccount = styled.div`
  padding: 11px;

  border-radius: 11px;
  border: 1px solid rgba(189, 189, 189, 0.5);

  background-color: #f2f2f2;

  label {
    color: #707070;
  }
`;

export const BankName = styled.p`
  font-size: 12px;

  margin-left: 37px;

  color: #707070;

  @media (min-width: 1280px) {
    font-size: 14px;
  }
`;

export const Fees = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 16px 1fr;

  font-size: 9px;

  padding: 9px;
  margin-top: 200px;

  border-radius: 15px;
  border: 1px solid rgba(189, 189, 189, 0.5);

  background-color: #f2f2f2;
  color: #7c7c7c;
`;

export const WithdrawDay = styled.div`
  grid-column-start: 1;
  grid-column-end: 2;

  p {
    margin-left: 13px;
  }
`;

export const WithdrawValue = styled.div`
  grid-column-start: 2;
  grid-column-end: 3;

  text-align: right;
`;

export const ListWithdraw = styled.div`
  display: none;

  min-height: 734px;

  > div:last-child {
    margin-top: 15px;
    margin-bottom: 0;
  }

  @media (min-width: 1280px) {
    display: block;
    grid-column-start: 3;
    grid-column-end: 7;

    padding: 15px 25px;
    max-width: none;

    border-radius: 15px;

    background-color: #ffffff;
  }
`;

export const ListWithdrawMobile = styled.div`
  min-height: 734px;

  padding: 15px 25px;

  border-radius: 15px;

  background-color: #ffffff;

  > div:first-child {
    height: 100%;
  }

  > div:last-child {
    margin-top: 15px;
    margin-bottom: 0;
  }

  @media (min-width: 1280px) {
    display: none;
  }
`;

export const TitleListWithdraw = styled.h4`
  font-size: 14px;
`;

export const Table = styled.table`
  margin-bottom: 40px;

  width: 100%;

  border-bottom: 1px solid #dcdcdc;
  border-collapse: collapse;
`;

export const Th = styled.th`
  text-align: center;
  font-weight: bold;
  font-size: 16px;

  margin-right: -1px;
  padding: 15px 0px;

  color: #707070;
`;

export const Td = styled.td`
  text-align: center;
  font-size: 16px;
  font-weight: ${(props) => (!props.permitted ? 'bold' : 'light')};

  padding: 15px 0px;

  border-top: 1px solid #dcdcdc;

  color: #707070;
`;

export const Thead = styled.thead``;

export const Tbody = styled.tbody``;

export const DataTableStyle = {};
