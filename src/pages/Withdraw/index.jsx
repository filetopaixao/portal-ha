import React, { useEffect, useState, useCallback } from 'react';
// import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';

// import { Actions as PoupupActions } from '@redux/ducks/poupup';

import Purchase from '@components/atoms/Cards/purchase';

import Pagination from '@components/molecules/Pagination';

import { formatReal } from '@utils/masksForms';
import { getUser } from '@services/user';
import { formateDateTime } from '@utils/formatDate';
import { getListTransference } from '@services/bankTransferences'; // postTransference

import Arrow from '@assets/images/arrowCheckout.svg';

import Validation from './validation';

import * as S from './styled';

const Withdraw = () => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();
  // const dispatch = useDispatch();

  const [withdraws, setWithdraws] = useState([]);
  const [userData, setUserData] = useState({});

  const [activeLoading, setActiveLoading] = useState(true);

  useEffect(() => {
    getUser()
      .then((response) => {
        const userDataResponse = response.data;
        setUserData(userDataResponse);

        setActiveLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    // const withdraw = [
    //   {
    //     id: 1,
    //     value: 'R$ 78.955,00',
    //     bankAccount: 'Banco Bradesco S.A',
    //     requestDate: '10/03/2020 16:57',
    //     status: 'Estornado por motivo: CONTA INVÁLIDA',
    //     permitted: false,
    //   },
    //   {
    //     id: 2,
    //     value: 'R$ 300,00',
    //     bankAccount: 'Banco Bradesco S.A',
    //     requestDate: '10/03/2020 16:57',
    //     status: 'Realizado em 13/03/2020',
    //     permitted: true,
    //   },
    //   {
    //     id: 3,
    //     value: 'R$ 165,00',
    //     bankAccount: 'Banco Bradesco S.A',
    //     requestDate: '10/03/2020 16:57',
    //     status: 'stornado por motivo: CONTA INVÁLIDA',
    //     permitted: false,
    //   },
    // ];
    // setWithdraws(withdraw);
    getListTransference().then((response) => {
      console.log(response.data);
      // const res = response.data.map((item) => {
      //   return {
      //     id: item.id,
      //     value: item.value,
      //     user_id: item.user_id,
      //     bankAccount: 'Banco Bradesco S.A',
      //     requestDate: formateDateTime(new Date(item.request_date)),
      //     statusTransf: `Realizado em ${formateDateTime(item.created_at)}`,
      //     status: response.status,
      //     permitted: true,
      //   };
      // });

      // setWithdraws(res);
    });
  }, []);

  const { handleChange, handleSubmit, values, setFieldValue } = useFormik({
    initialValues: {
      name: '',
      bank: '',
      saldo: '',
      valor: '',
    },
    validationSchema: Validation,
    onSubmit(valuesInput) {
      // const objectTransf = {
      //   token_id: localStorage.getItem('token'),
      //   value: valuesInput.valor,
      // };

      // postTransference(objectTransf).then((response) => {
      //   console.log(response);

      //   setWithdraws([
      //     ...withdraws,
      //     {
      //       id: response.id,
      //       value: response.value,
      //       bankAccount: 'Banco Bradesco S.A',
      //       requestDate: formateDateTime(new Date(response.request_date)),
      //       statusTransf: `Realizado em ${formateDateTime(response.created_at)}`,
      //       status: response.status,
      //       permitted: response.status === 'pending',
      //     },
      //   ]);
      // });
      // console.log('Page WithDraw => ', valuesInput);
      // dispatch(
      //   PoupupActions.visibleSuccess({
      //     title: 'Saque solicitado com sucesso!',
      //     visible: true,
      //     description: 'O valor será creditado em sua conta em até 3 dias úteis',
      //   })
      // );
      setWithdraws([
        ...withdraws,
        {
          id: Math.random(),
          value: valuesInput.valor,
          bankAccount: 'Banco Bradesco S.A',
          requestDate: formateDateTime(new Date()),
          statusTransf: `Realizado em ${formateDateTime(new Date())}`,
          status: 'pending',
          permitted: true,
        },
      ]);

      setFieldValue('valor', '');
    },
  });

  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentWithdraw = withdraws.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    const newDatas = withdraws.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setWithdraws(newDatas);
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.ContainerWithdraw>
        <S.RequestWithdraw>
          <Purchase
            icon="liberado"
            title="Saldo Liberado"
            value="R$ 15.200"
            primaryColor={themeStore.primaryColor}
            secondaryColor={themeStore.secondaryColor}
            tertiaryColor={themeStore.tertiaryColor}
          />
          <S.Form onSubmit={handleSubmit} theme={themeStore.themeName}>
            <S.LabelInput theme={themeStore.themeName}>Quanto você deseja sacar?</S.LabelInput>
            <S.InputValueWithdraws
              type="text"
              name="valor"
              id="valor"
              placeholder="Digite o valor R$ 0,00"
              variant=""
              value={formatReal(values.valor)}
              onChange={handleChange}
              theme={themeStore.themeName}
            />
            <S.MinimumValue theme={themeStore.themeName}>Valor mínimo de saque: R$ 20,00</S.MinimumValue>
            <S.LabelInput theme={themeStore.themeName}>Conta</S.LabelInput>
            <S.BankAccount theme={themeStore.themeName}>
              <S.BankName theme={themeStore.themeName}>{`${userData.first_name} ${userData.last_name}`}</S.BankName>
              <S.BankName theme={themeStore.themeName}>Banco Bradesco S.A.</S.BankName>
            </S.BankAccount>
            <S.ButtonBank
              type="button"
              onClick={() => history.push('/myAccount/tabcontent/content4')}
              variant="secondary"
              secondaryColor={themeStore.secondaryColor}
            >
              TROCAR DE CONTA
            </S.ButtonBank>
            <S.Fees theme={themeStore.themeName}>
              <p>TAXAS</p>
              <S.WithdrawDay>
                <p>01 saque no dia</p>
                <p>02 ou mais saques no dia</p>
              </S.WithdrawDay>
              <S.WithdrawValue>
                <p>Grátis</p>
                <p>R$ 3,80 / saque</p>
              </S.WithdrawValue>
            </S.Fees>
            <S.ButtonWithdraw variant="primary" type="submit" primaryColor={themeStore.primaryColor}>
              SOLICITAR SAQUE
            </S.ButtonWithdraw>
          </S.Form>
        </S.RequestWithdraw>
        <S.ListWithdraw theme={themeStore.themeName}>
          <S.DataTableTitle theme={themeStore.themeName}>ÚLTIMOS SAQUES</S.DataTableTitle>

          <S.ContainerDataTable>
            <S.TableHeader theme={themeStore.themeName}>
              <S.TextCell theme={themeStore.themeName} className="text-upp">
                VALOR
              </S.TextCell>
              <S.TextCell theme={themeStore.themeName} className="text-upp off-1">
                CONTA
              </S.TextCell>
              <S.TextCell theme={themeStore.themeName} className="text-upp">
                DATA DA SOLICITAÇÂO
              </S.TextCell>
              <S.TextCell theme={themeStore.themeName} className="text-upp off-3">
                STATUS
              </S.TextCell>
              <S.TextCell theme={themeStore.themeName} className="text-upp btn-expansed" />
            </S.TableHeader>
            <S.TableBody>
              <>
                {currentWithdraw.length <= 0 ? (
                  <S.ContainerBackEndInfo>
                    <S.InfoBackEnd theme={themeStore.themeName}>Não há solicitações !</S.InfoBackEnd>
                    {/* <S.ImageNotFound /> */}
                  </S.ContainerBackEndInfo>
                ) : (
                  <>
                    {currentWithdraw.map((item) => (
                      <>
                        <S.TableLine theme={themeStore.themeName} permitted={item.permitted}>
                          <S.TableCell theme={themeStore.themeName} className="text-upp">
                            {item.value}
                          </S.TableCell>
                          <S.TableCell theme={themeStore.themeName} className="text-upp off-1">
                            {item.bankAccount}
                          </S.TableCell>
                          <S.TableCell theme={themeStore.themeName} className="text-upp">
                            {item.requestDate}
                          </S.TableCell>
                          <S.TableCell theme={themeStore.themeName} className="text-upp off-3">
                            {item.status}
                          </S.TableCell>
                          <S.TableCell theme={themeStore.themeName} className="text-upp off-button-set">
                            <S.Expansed href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                              <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                            </S.Expansed>
                          </S.TableCell>
                        </S.TableLine>
                        {item.activeExpansed ? (
                          <S.TableLineExpansed className="activeExpansed" theme={themeStore.themeName}>
                            <S.TableCellExpansed theme={themeStore.themeName} className="text-upp">
                              <S.TitleValueExpan>
                                <p>CONTA</p>
                              </S.TitleValueExpan>
                              <div>
                                <p>{item.bankAccount}</p>
                              </div>
                            </S.TableCellExpansed>
                            <S.TableCellExpansed theme={themeStore.themeName} className="text-upp">
                              <S.TitleValueExpan>
                                <p>STATUS</p>
                              </S.TitleValueExpan>
                              <div>
                                <p>{item.status}</p>
                              </div>
                            </S.TableCellExpansed>
                          </S.TableLineExpansed>
                        ) : null}
                      </>
                    ))}
                  </>
                )}
              </>
            </S.TableBody>
          </S.ContainerDataTable>

          <Pagination
            className="pagination"
            currentPage={currentPage}
            perPage={perPage}
            total={withdraws.length}
            paginate={paginate}
          />
        </S.ListWithdraw>
      </S.ContainerWithdraw>
    </>
  );
};

export default Withdraw;
