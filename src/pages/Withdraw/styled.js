import styled from 'styled-components';
import NotFound from '@assets/images/erro-404.svg';

import PageLoadding from '@assets/images/page_loadding.gif';

export const DataTableTitle = styled.h3`
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const LabelInput = styled.p`
  font-size: 14px;
  text-align: left;
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const InputValueWithdraws = styled.input`
  text-align: left;
  letter-spacing: 0px;

  width: 100%;
  height: 40px;

  padding: 0 20px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
  border-radius: 15px;

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ButtonBank = styled.button`
  text-align: center;
  letter-spacing: 0px;
  color: #ffffff;
  text-transform: uppercase;

  font-size: 17px;
  width: 100%;
  height: 53px;

  border: none;
  border-radius: 15px;

  cursor: pointer;
  background: ${(props) => props.secondaryColor};
  box-shadow: 0px 3px 6px #00000029;
`;

export const ButtonWithdraw = styled.button`
  text-align: center;
  letter-spacing: 0px;
  color: #ffffff;
  text-transform: uppercase;

  font-size: 17px;
  width: 100%;
  height: 53px;

  border: none;
  border-radius: 15px;

  cursor: pointer;
  background: ${(props) => props.primaryColor};
  box-shadow: 0px 3px 6px #00000029;
`;

export const ContainerWithdraw = styled.div`
  @media (min-width: 1280px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr;
    grid-gap: 20px;

    padding: 0px;
  }
`;

export const RequestWithdraw = styled.div`
  margin-bottom: 20px;

  @media (min-width: 1280px) {
    grid-column-start: 1;
    grid-column-end: 3;

    margin-bottom: 0px;
    max-width: none;
  }

  .purchase {
    background: ${(props) =>
      props.tertiaryColor
        ? `transparent linear-gradient(353deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
        : `transparent linear-gradient(353deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};
  }
`;

export const Form = styled.form`
  margin-top: 15px;

  padding: 30px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  button {
    width: 100%;
    margin-top: 20px;
  }

  button:nth-child(4n) {
    padding: 15px;
  }

  label {
    margin-left: 7px;
    margin-bottom: 3px;
  }
`;

export const MinimumValue = styled.p`
  font-size: 10px;
  font-weight: normal;

  margin: 3px 0px 20px 0;

  text-align: left;
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const BankAccount = styled.div`
  padding: 11px;

  border-radius: 11px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  label {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const Fees = styled.div`
  display: grid;
  grid-template-columns: 1.5fr 1fr;
  grid-template-rows: 16px 1fr;

  font-size: 9px;

  padding: 9px;
  margin-top: 10rem;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  > p {
    font-weight: bold;
  }
`;

export const WithdrawDay = styled.div`
  grid-column-start: 1;
  grid-column-end: 2;

  p {
    margin-left: 13px;
  }
`;

export const WithdrawValue = styled.div`
  grid-column-start: 2;
  grid-column-end: 3;

  text-align: right;
`;

export const ListWithdraw = styled.div`
  display: block;
  min-height: 734px;

  > div:last-child {
    margin-top: 15px;
    margin-bottom: 0;
  }

  @media (min-width: 1280px) {
    grid-column-start: 3;
    grid-column-end: 7;

    padding: 15px 25px;
    max-width: none;

    border-radius: 15px;

    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
    background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  }
`;

export const BankName = styled.p`
  font-size: 12px;

  margin-left: 37px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 1280px) {
    font-size: 14px;
  }
`;

export const ContainerDataTable = styled.div`
  width: 100%;

  border-radius: 15px;
  background: transparent;

  @media (max-width: 600px) {
    padding: 15px;
  }

  > p {
    font-size: 16px;
    font-weight: bold;
    color: #5e5e5e;
  }
`;

export const TableHeader = styled.div`
  text-align: center;

  display: grid;
  grid-template-columns: 0.8fr 1.2fr 1.4fr 2fr;

  @media (max-width: 1200px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1.5fr 0.3fr;
  }

  margin-top: 15px;
  padding: 14px 0;

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
`;

export const TextCell = styled.p`
  font-size: 12px;
  font-weight: bold;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (max-width: 1200px) {
    &.off-3 {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const TableBody = styled.div`
  img {
    display: block;
    height: 100%;
    width: 20px !important;

    @media (min-width: 1400px) {
      height: 100%;
      width: 25px !important;
    }
  }
`;

export const TableLine = styled.div`
  text-align: center;

  ${(props) => (props.permitted ? '' : 'background: #EEF0F4 0% 0% no-repeat padding-box; opacity: 0.5;')};

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  display: grid;
  grid-template-columns: 0.8fr 1.2fr 1.4fr 2fr;

  @media (max-width: 1200px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1.5fr 0.3fr;
  }

  padding: 10px 0px;

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
`;

export const TableCell = styled.div`
  font-size: 12px;
  font-weight: bold;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  .toggle-switch {
    margin-left: -25px;
  }

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (min-width: 800px) {
    &.off-button-set {
      display: none;
    }
  }

  @media (max-width: 1200px) {
    &.off-3 {
      display: none;
    }
  }

  @media (max-width: 1000px) {
    &.off-2 {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const TableCellExpansed = styled.div`
  display: grid;
  grid-template-columns: 0.5fr 2fr;
  align-items: center;

  margin-bottom: 15px;

  p {
    font-size: 12px;
    text-transform: uppercase;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const TableLineExpansed = styled.div`
  text-align: center;
  display: block;
  padding: 10px;

  transition: all cubic-bezier(0.175, 0.885, 0.32, 1.275);

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#111a21' : '#FFFFFF')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    display: none;
  }

  &.activeExpansed {
    height: auto;
  }

  &.defaultExpansed {
    height: 0px;
  }
`;

export const ContainerPagination = styled.div`
  width: 100%;
`;

export const ButtonExpansed = styled.img`
  grid-area: 'close';

  width: 20px;
  height: 100%;
  margin: 0 auto;

  cursor: pointer;
  transform: rotate(180deg);
  transition: transform 300ms linear;

  ${(props) => (props.active ? 'transform: rotate(0);' : '')};
`;

export const TitleValueExpan = styled.div`
  p {
    text-align: left;
    font-weight: bold;
  }
`;

export const Expansed = styled.a``;

export const ButtonGroup = styled.div`
  display: flex;
  align-items: center;

  @media (max-width: 800px) {
    button {
      width: 17px;
      height: 17px;
    }
  }
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  font-size: 14px;
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
