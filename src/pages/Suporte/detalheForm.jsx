import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom'; // ,useParams

import { getByIdCategories, getByIdFaqs } from '@services/suporte_faq';

import * as S from './styled';

function FormSuporteDetalhe() {
  const themeStore = useSelector((state) => state.theme);
  // const { name } = useParams();
  // console.log(name);
  const { pathname } = useLocation();
  const routeArray = pathname.split('/');
  const id = routeArray[routeArray.length - 2];
  const name = routeArray[routeArray.length - 1];

  const [datas, setDatas] = useState([]);
  const [activeLoading, setActiveLoading] = useState(true);

  useEffect(() => {
    if (id) {
      getByIdCategories(id)
        .then((response) => {
          console.log(response.data);
          const serialized = response.data.faq.map((item) => {
            return {
              id: item.id,
              question: item.question,
              answer: item.answer,
              media_url: item.media_url,
              ativo: false,
            };
          });
          setDatas(serialized);
          setActiveLoading(false);
        })
        .catch(() => {
          setDatas([]);
          setActiveLoading(false);
        });
    }
  }, [id]);

  function handleQuestion(index) {
    getByIdFaqs(index).then((response) => {
      console.log(response.data);
    });
    const newDatas = datas.map((data) => {
      return data.id === index ? { ...data, ativo: !data.ativo } : { ...data, ativo: false };
    });
    setDatas(newDatas);
  }

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <S.Container theme={themeStore.themeName}>
      <S.ButtonCloser>
        <Link to="/suporte">
          <S.CloseButton>FecharX</S.CloseButton>
        </Link>
      </S.ButtonCloser>
      <S.Div className="contact_form">
        <S.Titulo size="lg" variant="bold" theme={themeStore.themeName}>
          {name}
        </S.Titulo>
        <S.DivConteudo>
          {datas.length <= 0 ? (
            <S.ContainerBackEndInfo>
              <S.InfoBackEnd theme={themeStore.themeName}>Desculpe! Não há informações cadastradas.</S.InfoBackEnd>
              <S.ImageNotFound />
            </S.ContainerBackEndInfo>
          ) : (
            <ul>
              {datas.map((data) => (
                <li key={data.id}>
                  <S.QuestionLink href="#!" onClick={() => handleQuestion(data.id)}>
                    <S.LabelDetailForm
                      theme={themeStore.themeName}
                      active={data.ativo ? 'active' : 'default'}
                      className="question_label"
                    >
                      {data.question}
                    </S.LabelDetailForm>
                    <S.LabelLink>
                      {data.ativo === true && (
                        <S.CardSpaceIcon theme={themeStore.themeName}>
                          <p className="indicative_arrow">⋀</p>
                          {/* <img src={images.arrowTop} alt="title" /> */}
                        </S.CardSpaceIcon>
                      )}
                      {data.ativo !== true && (
                        <S.CardSpaceIcon theme={themeStore.themeName}>
                          <p className="indicative_arrow">⋁</p>
                          {/* <img src={images.arrowDown} alt="title" /> */}
                        </S.CardSpaceIcon>
                      )}
                    </S.LabelLink>
                  </S.QuestionLink>
                  {data.ativo === true ? (
                    <S.QuestionContainer theme={themeStore.themeName}>
                      <div className="answer_content">
                        <p>{data.answer}</p>
                      </div>
                      {data.media_url ? (
                        <div className="video_content">
                          <iframe
                            title="tutorial"
                            className="iframe-responsive-video"
                            src={data.media_url}
                            allowFullScreen
                          />
                        </div>
                      ) : null}
                    </S.QuestionContainer>
                  ) : null}
                </li>
              ))}
            </ul>
          )}
          <ul>
            {datas.map((data) => (
              <li key={data.id}>
                <S.QuestionLink href="#!" onClick={() => handleQuestion(data.id)}>
                  <S.LabelDetailForm
                    theme={themeStore.themeName}
                    active={data.ativo ? 'active' : 'default'}
                    className="question_label"
                  >
                    {data.question}
                  </S.LabelDetailForm>
                  <S.LabelLink>
                    {data.ativo === true && (
                      <S.CardSpaceIcon theme={themeStore.themeName}>
                        <p className="indicative_arrow">⋀</p>
                        {/* <img src={images.arrowTop} alt="title" /> */}
                      </S.CardSpaceIcon>
                    )}
                    {data.ativo !== true && (
                      <S.CardSpaceIcon theme={themeStore.themeName}>
                        <p className="indicative_arrow">⋁</p>
                        {/* <img src={images.arrowDown} alt="title" /> */}
                      </S.CardSpaceIcon>
                    )}
                  </S.LabelLink>
                </S.QuestionLink>
                {data.ativo === true ? (
                  <S.QuestionContainer theme={themeStore.themeName}>
                    <div className="answer_content">
                      <p>{data.answer}</p>
                    </div>
                    {data.media_url ? (
                      <div className="video_content">
                        <iframe
                          title="tutorial"
                          className="iframe-responsive-video"
                          src={data.media_url}
                          allowFullScreen
                        />
                      </div>
                    ) : null}
                  </S.QuestionContainer>
                ) : null}
              </li>
            ))}
          </ul>
        </S.DivConteudo>
      </S.Div>
    </S.Container>
  );
}
export default FormSuporteDetalhe;
