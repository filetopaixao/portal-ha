import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import Button from '@components/atoms/Button';

import { getAllCategories } from '@services/suporte_faq';

import * as S from './styled';

function Suporte() {
  // const [sup, setSup] = useState([]);
  const themeStore = useSelector((state) => state.theme);

  const [datas] = useState([
    {
      icon: 'tutorialPlataforma',
      id: 2,
      name: 'Tutorial plataforma',
      variant: 'store',
      isComing: false,
    },
    {
      icon: 'vendaFisica',
      id: 3,
      name: 'Venda física',
      variant: 'store',
      isComing: false,
    },
    {
      icon: 'vendaOnline',
      id: 4,
      name: 'Venda online',
      variant: 'store',
      isComing: false,
    },
    { icon: 'produtos', id: 5, name: 'Produtos', variant: 'store', isComing: false },
    {
      icon: 'materialMarketing',
      id: 6,
      name: 'Material de Marketing',
      variant: 'store',
      isComing: false,
    },
    {
      icon: 'plataformaDeEnsino',
      id: 7,
      name: 'Plataforma de Ensino',
      variant: 'store',
      isComing: false,
    },
    {
      icon: 'parceriasIndicacoes',
      id: 8,
      name: 'Parceiros e Empreendedores',
      variant: 'store',
      isComing: false,
    },
    {
      icon: 'acompanhamentoEverHelp',
      id: 9,
      name: 'Acompanhamento Ever',
      variant: 'store',
      isComing: false,
    },
  ]);

  useEffect(() => {
    getAllCategories().then((response) => {
      console.log(response.data.data);
      // const serialized = response.data.map((item) => {
      //   return {
      //     id: item.id,
      //     name: item.name,
      //     icon: item.icon,
      //     variant: 'store',
      //     isComing: false,
      //   };
      // });
      // setSup(serialized);
    });
  }, []);

  return (
    <S.Container className="space-justify-center-container" theme={themeStore.themeName}>
      <div>
        <S.TextTitulo size="lg" variant="bold" theme={themeStore.themeName}>
          Qual tópico você precisa de ajuda?
        </S.TextTitulo>
        <S.Space data={datas} rota="suporte/detalhe" isSupport />
      </div>
      <S.GetInTouch primaryColor={themeStore.primaryColor}>
        <S.Text size="md" variant="bold" theme={themeStore.themeName}>
          Entrar em contato com o suporte
        </S.Text>
        <S.TextRodape size="sm" class="text-rodape" theme={themeStore.themeName}>
          Obtenha ajuda com nossos especialistas sobre questões e preocupações específicas
        </S.TextRodape>
        <Link className="link_default" to="/suporte/contato">
          <Button.Group className="center">
            <Button variant="secondary">FALAR COM SUPORTE</Button>
          </Button.Group>
        </Link>
      </S.GetInTouch>
    </S.Container>
  );
}
export default Suporte;
