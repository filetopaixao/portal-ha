import styled from 'styled-components';

export const Container = styled.div``;

export const Content = styled.section``;

export const ContainerStatus = styled.div`
  display: block;
  width: 100%;

  div.purchase {
    margin: 0 auto;
    margin-bottom: 10px;

    @media (min-width: 1300px) {
      margin: inherit;
    }
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 20px;
  }
`;

export const SampleStyle = styled.ul`
  padding: 16px;

  list-style: none;
  border-radius: 10px;

  background: #cecece42;

  li {
    font-size: 12px;

    display: flex;
    justify-content: space-between;

    margin-bottom: 10px;

    color: #7c7c7c;

    p:first-child {
      font-weight: bold;
      text-transform: uppercase;

      display: block;

      /* margin-right: 30px; */
      /* width: 110px; */
    }

    p:last-child {
      text-align: center;

      /* width: 100px; */
    }
  }
`;

export const TitleContainer = styled.div`
  margin: 15px 0;

  color: #5e5e5e;
`;

export const ContainerFilterPrint = styled.div`
  > div {
    margin-top: 10px;
  }
`;

export const ContentDataTable = styled.section`
  margin-top: 10px;
  margin-bottom: 10px;
  min-height: 17rem;
  padding: 5px 20px;

  border-radius: 10px;

  background: #ffffff;

  .lkQkBS {
    flex-grow: 1;
    max-width: inherit;
    min-width: inherit;
  }

  .ghCufV {
    flex-grow: 0.5;
    min-width: inherit;
    justify-content: center;
  }
`;

export const ContainerOps = styled.div`
  display: block;

  width: 100%;

  > div {
    margin-bottom: 10px;
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 20px;

    margin-bottom: inherit;
  }

  select {
    border: 1px solid #cdcdcd;
    background: #f1eeee 0% 0% no-repeat padding-box;
    color: #5e5e5e;
  }
`;

export const ModalContainer = styled.div`
  > div#modal {
    > div {
      width: 65%;
    }
  }
`;

export const ModalContent = styled.div`
  &.content {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 20px;
  }

  .p10 {
    padding: 10px;
  }

  .p20 {
    padding: 20px;
  }

  .content-info {
    padding: 10px 5px;
  }

  @media (min-width: 1500px) {
    .content-info {
      padding: 20px 10px;
    }
  }
`;

export const ContainerInfo = styled.div`
  &.border_top {
    border-top: ${(props) => (props.theme === 'Dark' ? '4px solid #979EA8' : '4px solid #82868D')};
  }

  .titleContainer {
    font-size: 18px;
    text-transform: uppercase;
    padding-left: 15px;
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }

  .flex-center {
    display: flex;
    align-items: center;

    > p:first-child {
      margin-right: 20px;
    }
  }

  .border-tb {
    border-top: ${(props) => (props.theme === 'Dark' ? '4px solid #979EA8' : '4px solid #82868D')};
    border-bottom: ${(props) => (props.theme === 'Dark' ? '4px solid #979EA8' : '4px solid #82868D')};
  }

  @media (min-width: 1500px) {
    .mb20 {
      margin-bottom: 20px;
    }
  }

  .mb30 {
    margin-bottom: 30px;
  }

  .text-bold {
    font-weight: bold;
    text-transform: uppercase;
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }

  .flex-center-between {
    display: flex;
    align-items: center;
    justify-content: space-between;

    > p:first-child {
      margin-right: 20px;
    }
  }
`;

export const ContainerList = styled.div`
  &.list {
  }

  .background-gray {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
    border-radius: 12px;

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const ContainerTable = styled.div`
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};
  margin-top: 25px;

  li {
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#82868D')};
    font-weight: bold;
    font-size: 14px;
    text-transform: uppercase;
  }

  > div:first-child {
    ul {
      display: flex;
      justify-content: space-between;

      padding: 10px 15px 0 10px;
      margin-bottom: 20px;

      width: 100%;

      list-style: none;
    }
  }

  > div.table_body_purchase {
    min-height: 200px;
    height: 100%;
    max-height: 200px;

    @media (max-width: 1500px) {
      max-height: 300px;
    }

    overflow-y: scroll;

    ::-webkit-scrollbar {
      -webkit-appearance: none;
    }
    ::-webkit-scrollbar:vertical {
      width: 14px;
    }
    ::-webkit-scrollbar:horizontal {
      height: 14px;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 3px;

      background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
    }
    ::-webkit-scrollbar-track {
      border-radius: 10px;
    }

    ul {
      display: flex;
      justify-content: space-between;

      width: 100%;

      padding: 0 0px 0 10px;
      margin: 0 5px 30px 0;

      list-style: none;

      li.btn-bigger-one {
        display: flex;
        justify-content: space-around;

        width: 60px;
        height: 40px;

        border-radius: 9px;

        border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

        background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

        color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
      }

      .priceUn {
        color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
      }

      li {
        align-items: center;
        display: flex;
        justify-content: center;

        max-width: 60px;
        width: 100%;
        height: 40px;

        img {
          display: flex;
          justify-content: center;
          align-items: center;

          width: 40px;
          height: auto;

          margin: 0 auto;
        }
      }
    }

    @media (max-width: 1500px) {
      font-size: 12px;
    }
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;

  margin-top: 10px;

  width: 100%;
  button {
    background: ${(props) => props.primaryColor};
    font-size: 17px;
  }
`;
