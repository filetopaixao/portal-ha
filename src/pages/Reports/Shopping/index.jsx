import React, { useState, useEffect, useCallback } from 'react';

import CardPurchase from '@components/atoms/Cards/purchase';
import MenuOpsFilterPrint from '@components/molecules/MenuOpsFilterPrint';
import Pagination from '@components/molecules/Pagination';
import { Select } from '@components/atoms';

import InconAdd from '@assets/images/icon_add.svg';

import Arrow from '@assets/images/arrowCheckout.svg';

import ModalCompra from './ViewPurchase';

import * as S from './styled';

const formasPagamento = [
  {
    id: '1',
    label: 'Cartão de Crédito',
    value: 'cartao',
  },
  {
    id: '2',
    label: 'Boleto Bancário',
    value: 'boleto',
  },
];

const statusPgto = [
  {
    id: '1',
    label: 'Pendente',
    value: 'pendente',
  },
  {
    id: '2',
    label: 'Aprovado',
    value: 'aprovado',
  },
  {
    id: '3',
    label: 'Cancelado',
    value: 'cancelado',
  },
];

const statusPedi = [
  {
    id: '1',
    label: 'Aguardando Pagamento',
    value: 'aguardando',
  },
  {
    id: '2',
    label: 'Em Separação',
    value: 'separacao',
  },
  {
    id: '3',
    label: 'Enviado',
    value: 'enviado',
  },
  {
    id: '4',
    label: 'Entregue',
    value: 'entregue',
  },
  {
    id: '5',
    label: 'Devolvido',
    value: 'devolvido',
  },
];

const datasPedido = [
  {
    id: '1',
    label: '7 Dias',
    value: '7',
  },
  {
    id: '2',
    label: '15 Dias',
    value: '15',
  },
  {
    id: '3',
    label: '30 Dias',
    value: '30',
  },
  {
    id: '4',
    label: '60 Dias',
    value: '60',
  },
];

const Compras = () => {
  const [filterText, setFilterText] = React.useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const [compras, setCompras] = useState([]);
  const [selected, setSelected] = useState({});
  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleOpenModal = (item) => {
    setSelected(item);
    setIsModalVisible(true);
  };

  useEffect(() => {
    setCompras([
      {
        id: 1,
        statusIcon: '',
        date: '31/08/2020',
        pedido: 'fake',
        vlCompra: 'R$ 2.000,00',
        formaPagamento: 'Ever257',
        statusPgto: 'Pendente',
        statusPedi: 'Aguardando Pagamento',
        activeExpansed: false,
        action: '',
      },
      {
        id: 2,
        statusIcon: '',
        date: '31/08/2020',
        pedido: 'fake',
        vlCompra: 'R$ 2.000,00',
        formaPagamento: 'Ever257',
        statusPgto: 'Cancelado',
        statusPedi: 'Aguardando Pagamento',
        activeExpansed: false,
        action: '',
      },
      {
        id: 3,
        statusIcon: '',
        date: '31/08/2020',
        pedido: 'fake',
        vlCompra: 'R$ 2.000,00',
        formaPagamento: 'Ever257',
        statusPgto: 'Aprovado',
        statusPedi: 'Aguardando Pagamento',
        activeExpansed: false,
        action: '',
      },
    ]);
  }, []);

  const filteredItems = compras.filter(
    (item) => item.formaPagamento && item.formaPagamento.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentCompras = filteredItems.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    const newDatas = compras.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setCompras(newDatas);
  };

  return (
    <S.Container>
      <S.ContainerStatus className="purchase__status">
        <CardPurchase
          className="purchase__status__commision"
          icon="sales"
          title="Compras"
          variant="sales"
          value="R$ 15.200"
        />
        <CardPurchase
          className="purchase__status__discount"
          icon="commision"
          title="Comissão"
          variant="commision"
          discount="40%"
        />
        <CardPurchase
          className="purchase__status__discount"
          icon="lootReleased"
          title="Ganhos ultimo mês"
          variant="lootReleased"
          discount="40%"
        />
        <CardPurchase
          className="purchase__status__discount"
          icon="totalBilled"
          title="Total faturado"
          variant="totalBilled"
          discount="40%"
        />
      </S.ContainerStatus>
      <S.Content>
        <S.TitleContainer>
          <h3>Filtros de Pesquisa</h3>
        </S.TitleContainer>

        <S.ContainerOps>
          <Select
            name="Forma de Pagamento"
            id="formasPgto"
            options={formasPagamento}
            defaultValue="Forma de Pagamento"
            onChange={(e) => console.log(e)}
          />

          <Select
            name="Status de Pagamento"
            id="statusPgto"
            options={statusPgto}
            defaultValue="Status de Pagamento"
            onChange={(e) => console.log(e)}
          />

          <Select
            name="Status de Pedido"
            id="statusPedi"
            options={statusPedi}
            defaultValue="Status de Pedido"
            onChange={(e) => console.log(e)}
          />

          <Select
            name="Data"
            id="datasPedido"
            options={datasPedido}
            defaultValue="Data"
            onChange={(e) => console.log(e)}
          />
        </S.ContainerOps>

        <S.ContainerFilterPrint>
          <MenuOpsFilterPrint filterText={filterText} setFilterText={setFilterText} />
        </S.ContainerFilterPrint>

        <S.ContainerDataTable id="print">
          <S.TableHeader>
            <S.TextCell className="text-upp" />
            <S.TextCell className="text-upp">DATA</S.TextCell>
            <S.TextCell className="text-upp off-1">PEDIDO</S.TextCell>
            <S.TextCell className="text-upp off-1">VALOR COMPRA</S.TextCell>
            <S.TextCell className="text-upp off-2">FORMA PAGAMENTO</S.TextCell>
            <S.TextCell className="text-upp off-3">STATUS PAGAMENTO</S.TextCell>
            <S.TextCell className="text-upp off-3">STATUS PEDIDO</S.TextCell>
            <S.TextCell className="text-upp">AÇÂO</S.TextCell>
            <S.TextCell className="text-upp" />
            <S.TextCell className="text-upp btn-expansed" />
          </S.TableHeader>
          <S.TableBody>
            <>
              {currentCompras.length <= 0 ? (
                <S.ContainerBackEndInfo>
                  <S.InfoBackEnd>Não há compras!</S.InfoBackEnd>
                  <S.ImageNotFound />
                </S.ContainerBackEndInfo>
              ) : (
                <>
                  {currentCompras.map((item) => (
                    <>
                      <S.TableLine>
                        <S.TableCell className="text-upp">
                          <S.ContainerAction>
                            <S.StatusPgto statusPagamento={item.statusPgto} />
                          </S.ContainerAction>
                        </S.TableCell>
                        <S.TableCell className="text-upp">{item.date}</S.TableCell>
                        <S.TableCell className="text-upp off-1">{item.pedido}</S.TableCell>
                        <S.TableCell className="text-upp off-1">{item.vlCompra}</S.TableCell>
                        <S.TableCell className="text-upp off-2">{item.formaPagamento}</S.TableCell>
                        <S.TableCell className="text-upp off-3">{item.statusPgto}</S.TableCell>
                        <S.TableCell className="text-upp off-3">{item.statusPedi}</S.TableCell>
                        <S.TableCell className="text-upp">
                          <S.ButtonGroup>
                            <S.ActionApproved href="#!" onClick={() => console.log('Aprovar')}>
                              Aprovar
                            </S.ActionApproved>
                            <S.Separator>|</S.Separator>
                            <S.ActionNotApproved href="#!" onClick={() => console.log('Reprovar')}>
                              Reprovar
                            </S.ActionNotApproved>
                          </S.ButtonGroup>
                        </S.TableCell>
                        <S.TableCell className="text-upp">
                          <S.ButtonGroupEdit>
                            <button onClick={() => handleOpenModal(item)} type="button">
                              <img src={InconAdd} alt="Editar" />
                            </button>
                          </S.ButtonGroupEdit>
                        </S.TableCell>
                        <S.TableCell className="text-upp off-button-set">
                          <S.Expansed href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                            <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                          </S.Expansed>
                        </S.TableCell>
                      </S.TableLine>
                      {item.activeExpansed ? (
                        <S.TableLineExpansed className="activeExpansed">
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>PEDIDO</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.pedido}</p>
                            </div>
                          </S.TableCellExpansed>
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>VALOR COMPRA</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.vlCompra}</p>
                            </div>
                          </S.TableCellExpansed>
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>FORMA PAGAMENTO</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.formaPagamento}</p>
                            </div>
                          </S.TableCellExpansed>
                          <S.TableCellExpansed className="text-upp">
                            <S.TitleValueExpan>
                              <p>STATUS PEDIDO</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.statusPedi}</p>
                            </div>
                          </S.TableCellExpansed>
                        </S.TableLineExpansed>
                      ) : null}
                    </>
                  ))}
                </>
              )}
            </>
          </S.TableBody>
        </S.ContainerDataTable>

        <Pagination currentPage={currentPage} perPage={perPage} total={compras.length} paginate={paginate} />
      </S.Content>
      <ModalCompra selected={selected} isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible} />
    </S.Container>
  );
};

export default Compras;
