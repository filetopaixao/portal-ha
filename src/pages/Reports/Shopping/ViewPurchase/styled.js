import styled, { css } from 'styled-components';

const scrollbarDefault = {
  scroll: () => css`
    ::-webkit-scrollbar {
      width: 10px;
      height: 20px;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 5px;

      background-color: #5e5e5e;
      background-clip: padding-box;
    }
    ::-webkit-scrollbar-track {
      background-color: #fbfbfb;
    }
    ::-webkit-scrollbar-button:single-button {
      display: block;

      height: 13px;
      width: 16px;

      border-style: solid;

      background-color: #fbfbfb;
    }
    ::-webkit-scrollbar-button:single-button:vertical:decrement {
      border-width: 0 8px 8px 8px;
      border-color: transparent transparent #555555 transparent;
    }
    ::-webkit-scrollbar-button:single-button:vertical:decrement:hover {
      border-color: transparent transparent #777777 transparent;
    }
    ::-webkit-scrollbar-button:single-button:vertical:increment {
      border-width: 8px 8px 0 8px;
      border-color: #555555 transparent transparent transparent;
    }
    ::-webkit-scrollbar-button:vertical:single-button:increment:hover {
      border-color: #777777 transparent transparent transparent;
    }
  `,
};

export const Container = styled.div``;

export const Content = styled.section``;

export const ContainerStatus = styled.div`
  display: block;
  width: 100%;

  div.purchase {
    margin: 0 auto;
    margin-bottom: 10px;

    @media (min-width: 1300px) {
      margin: inherit;
    }
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 20px;
  }
`;

export const SampleStyle = styled.ul`
  padding: 16px;

  list-style: none;
  border-radius: 10px;

  background: #cecece42;

  li {
    font-size: 12px;

    display: flex;
    justify-content: space-between;

    margin-bottom: 10px;

    color: #7c7c7c;

    p:first-child {
      font-weight: bold;
      text-transform: uppercase;

      display: block;

      /* margin-right: 30px; */
      /* width: 110px; */
    }

    p:last-child {
      text-align: center;

      /* width: 100px; */
    }
  }
`;

export const TitleContainer = styled.div`
  margin: 15px 0;

  color: #5e5e5e;
`;

export const ContainerFilterPrint = styled.div`
  > div {
    margin-top: 10px;
  }
`;

export const ContentDataTable = styled.section`
  margin-top: 10px;
  margin-bottom: 10px;
  min-height: 17rem;
  padding: 5px 20px;

  border-radius: 10px;

  background: #ffffff;

  .lkQkBS {
    flex-grow: 1;
    max-width: inherit;
    min-width: inherit;
  }

  .ghCufV {
    flex-grow: 0.5;
    min-width: inherit;
    justify-content: center;
  }
`;

export const ContainerOps = styled.div`
  display: block;

  width: 100%;

  > div {
    margin-bottom: 10px;
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 20px;

    margin-bottom: inherit;
  }

  select {
    border: 1px solid #cdcdcd;
    background: #f1eeee 0% 0% no-repeat padding-box;
    color: #5e5e5e;
  }
`;

export const ModalContainer = styled.div`
  > div#modal {
    > div {
      width: 65%;
    }
  }
`;

export const ModalContent = styled.div`
  &.content {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 20px;
  }

  .p10 {
    padding: 10px;
  }

  .p20 {
    padding: 20px;
  }

  .content-info {
    padding: 10px 5px;
  }

  @media (min-width: 1500px) {
    .content-info {
      padding: 20px 10px;
    }
  }
`;

export const ContainerInfo = styled.div`
  &.border_top {
    border-top: 4px solid #5e5e5e;
  }

  .titleContainer {
    padding-left: 15px;
  }

  .flex-center {
    display: flex;
    align-items: center;

    > p:first-child {
      margin-right: 20px;
    }
  }

  .border-tb {
    border-top: 4px solid #5e5e5e;
    border-bottom: 4px solid #5e5e5e;
  }

  @media (min-width: 1500px) {
    .mb20 {
      margin-bottom: 20px;
    }
  }

  .mb30 {
    margin-bottom: 30px;
  }

  .text-bold {
    font-weight: bold;
  }

  .flex-center-between {
    display: flex;
    align-items: center;
    justify-content: space-between;

    > p:first-child {
      margin-right: 20px;
    }
  }
`;

export const ContainerList = styled.div`
  &.list {
  }

  .background-gray {
    background: #f2f2f2bd;
    border-radius: 12px;
  }
`;

export const ContainerTable = styled.div`
  border: 1px solid #bdbdbd;
  margin-top: 25px;

  li {
    color: #5e5e5e;
    font-weight: 400;
    font-size: 14px;
  }

  > div:first-child {
    ul {
      display: flex;
      justify-content: space-between;

      padding: 10px 15px 0 10px;
      margin-bottom: 20px;

      width: 100%;

      list-style: none;
    }
  }

  > div.table_body_purchase {
    min-height: 200px;
    height: 100%;
    max-height: 200px;

    @media (max-width: 1500px) {
      max-height: 300px;
    }

    overflow-y: scroll;

    ${scrollbarDefault.scroll}

    ul {
      display: flex;
      justify-content: space-between;

      width: 100%;

      padding: 0 0px 0 10px;
      margin: 0 5px 30px 0;

      list-style: none;

      li.btn-bigger-one {
        display: flex;
        justify-content: space-around;

        width: 60px;
        height: 40px;

        border-radius: 9px;

        background: #f2f2f2 0% 0% no-repeat padding-box;
      }

      li {
        align-items: center;
        display: flex;
        justify-content: center;

        max-width: 60px;
        width: 100%;
        height: 40px;

        img {
          display: flex;
          justify-content: center;
          align-items: center;

          width: 40px;
          height: auto;

          margin: 0 auto;
        }
      }
    }

    @media (max-width: 1500px) {
      font-size: 12px;
    }
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;

  margin-top: 10px;

  width: 100%;
  button {
    margin: 0 auto;
  }
`;
