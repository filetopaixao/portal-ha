import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { Button } from '@components/atoms/';

import { Actions as ProductsActions } from '@redux/ducks/products';

import Modal from '@components/molecules/Modals';

import * as S from './styled';

const ModalCompra = ({ isModalVisible, setIsModalVisible }) => {
  const dispatch = useDispatch();
  const productsStore = useSelector((state) => state.products);
  // selected,
  // const handleCloser = () => {
  // console.log(selected);
  // setIsModalVisible(!isModalVisible);
  // };

  const [productsSelected, setProductsSelected] = useState([]);

  useEffect(() => {
    dispatch(ProductsActions.getProductsSelected());
    setProductsSelected(productsStore.productHandleSelected || []);
  }, [productsStore, dispatch, productsSelected]);

  return (
    <S.ModalContainer>
      <Modal alignCenter title="" isVisible={isModalVisible} onClose={() => setIsModalVisible(!isModalVisible)}>
        <S.ModalContent className="content">
          <div>
            <S.ContainerInfo>
              <div className="mb20 content-info">
                <div className="flex-center">
                  <p>Pedido</p>
                  <p className="text-bold">Ever251</p>
                </div>

                <div className="flex-center">
                  <p>Data</p>
                  <p className="text-bold">20/11/2020</p>
                </div>
              </div>
              <h3 className="titleContainer">Dados do Cliente</h3>
            </S.ContainerInfo>
            <S.ContainerInfo className="border_top">
              <div className="mb20 content-info">
                <div className="flex-center-between">
                  <p>Nome:</p>
                  <p>Fulano de Tal</p>
                </div>

                <div className="flex-center-between">
                  <p>Contato:</p>
                  <p>(00) 0 0000-0000</p>
                </div>
              </div>
              <h3 className="titleContainer">Origem</h3>
            </S.ContainerInfo>
            <S.ContainerInfo className="border_top">
              <div className="mb20 content-info">
                <div className="flex-center-between">
                  <p>Cupom de desconto</p>
                  <p>Cupom de desconto</p>
                </div>
                <div className="flex-center-between">
                  <p>Desconto</p>
                  <p>Desconto</p>
                </div>
              </div>

              <h3 className="titleContainer">Total da Venda</h3>
            </S.ContainerInfo>
            <S.ContainerInfo className="border_top">
              <div className="mb20 content-info">
                <div className="flex-center-between ">
                  <p>Subtotal</p>
                  <p>Subtotal</p>
                </div>
                <div className="flex-center-between">
                  <p>Desconto</p>
                  <p>Desconto</p>
                </div>
                <div className="flex-center-between">
                  <p>Frete</p>
                  <p>Frete</p>
                </div>
                <div className="flex-center-between">
                  <p>Total</p>
                  <p>Total</p>
                </div>
              </div>
              <h3 className="titleContainer">Ganhos</h3>
            </S.ContainerInfo>
            <S.ContainerInfo className="border_top">
              <p>Evercoin</p>
              <h3 className="titleContainer">Comissão</h3>
            </S.ContainerInfo>
          </div>

          <S.ContainerList className="list">
            <div className="background-gray p20">
              <S.ContainerInfo>
                <h3 className="titleContainer">Entrega</h3>
                <div className="mb20 border-tb p10 content-info">
                  <div>
                    <p>Pedido</p>
                  </div>
                  <div>
                    <p>Pedido</p>
                  </div>
                  <div>
                    <p>Pedido</p>
                  </div>
                </div>
                <h3 className="titleContainer">Itens do Pedido</h3>
              </S.ContainerInfo>

              <S.ContainerTable>
                <div className="table_header_purchase">
                  <ul>
                    <li>Produto</li>
                    <li>Descrição</li>
                    <li>Qt</li>
                    <li>Preço Un.</li>
                  </ul>
                </div>

                <div className="table_body_purchase full_width">
                  {productsSelected.map((product) => (
                    <ul key={product.id}>
                      <li>
                        <img src={product.srcImg} alt={product.nameImg} />
                      </li>
                      <li>{product.title}</li>
                      <li className="btn-bigger-one">
                        <p>{product.qtd}</p>
                      </li>
                      <li>{product.valueProduct}</li>
                    </ul>
                  ))}
                </div>
              </S.ContainerTable>
            </div>
            <S.ButtonContainer>
              <Button onClick={() => {}} variant="primary" type="button">
                Ver mais dados do cliente
              </Button>
            </S.ButtonContainer>
          </S.ContainerList>
        </S.ModalContent>
      </Modal>
    </S.ModalContainer>
  );
};

ModalCompra.propTypes = {
  // selected: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
};
ModalCompra.defaultProps = {
  // selected: {},
  isModalVisible: false,
  setIsModalVisible: () => {},
};

export default ModalCompra;
