// revenda / doacao
const tipoApresentacao = 'revenda';

export const getLinkUser = [
  'Home',
  tipoApresentacao === 'revenda' ? 'Plataforma' : 'Doação Recorrente',
  tipoApresentacao === 'revenda' ? 'Landing page' : 'Doação por Evento',
  tipoApresentacao === 'revenda' ? 'Carrinho Dinâmico' : 'Doação I.R.',
  tipoApresentacao === 'revenda' ? 'Cupom de desconto' : 'Passos que Salvam',
  'Links de apoio',
  // 'Adquirir produtos',
  // 'Meus clientes',
  // 'Parceiros',
  // 'Empreendedores',
  'Saque',
  tipoApresentacao === 'revenda' ? 'Relatório de Vendas' : 'Relatório de Doações',
  'Vendas',
  'Compras',
  'Parceiros',
  'Empreendedores',
  'Saques',
  'Espaço EVER',
  'Bank',
  'Shopping',
  'Marketing',
  'University',
  // 'Calendário',
  tipoApresentacao === 'revenda' ? 'Meu Gerente' : 'Meu Coordenador',
  'Suporte',
];

export default getLinkUser;
