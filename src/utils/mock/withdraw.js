export const getListWithdraw = {
  withdraw: [
    {
      id: 1,
      value: 'R$ 78.955,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Estornado por motivo: CONTA INVÁLIDA',
      permitted: false,
    },
    {
      id: 2,
      value: 'R$ 300,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 3,
      value: 'R$ 165,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 4,
      value: 'R$ 625,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 5,
      value: 'R$ 755,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 6,
      value: 'R$ 55,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 7,
      value: 'R$ 955,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Estornado por motivo: CONTA INVÁLIDA',
      permitted: false,
    },
    {
      id: 8,
      value: 'R$ 55,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 9,
      value: 'R$ 8.955,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 10,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 11,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 12,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 13,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 14,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 15,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
    {
      id: 16,
      value: 'R$ 150,00',
      bankAccount: 'Banco Bradesco S.A',
      requestDate: '10/03/2020 16:57',
      status: 'Realizado em 13/03/2020',
      permitted: true,
    },
  ],
  pages: [
    { number: 1, selected: true },
    { number: 2, selected: false },
    { number: 3, selected: false },
  ],
};

export default getListWithdraw;
