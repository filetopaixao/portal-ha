export const formateDate = (value) => new Intl.DateTimeFormat('pt-BR').format(new Date(value));

export const formateDateTime = (value) =>
  new Intl.DateTimeFormat('pt-BR', {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }).format(value);

export const formateDateExt = (value) =>
  new Intl.DateTimeFormat('pt-br', { year: 'numeric', month: 'long', day: 'numeric' }).format(new Date(value));

export const formateDateExt2 = (value) =>
  new Intl.DateTimeFormat('pt-br', { dateStyle: 'long' }).format(new Date(value));

export default formateDate;
