import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

// add rowk, email
const Profile = ({ image, name, telephony, phone }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ProfileContainer theme={themeStore.themeName}>
      <S.ProfileImage>
        <img src={image} alt={name} />
      </S.ProfileImage>

      <S.ProfileInfo>
        <S.ProfileName>{name}</S.ProfileName>
        <S.ProfileWork>Coordenador HA</S.ProfileWork>

        <S.ProfileText>bruno.gerente@ha.com.br</S.ProfileText>
        <S.ProfileText>{telephony}</S.ProfileText>
        <S.ProfileText>{phone}</S.ProfileText>

        <S.ButtonGroup className={document.body.clientWidth < 1024 ? 'center' : 'start'}>
          <S.Button color={themeStore.primaryColor} variant="secondary" showIcon icon="email">
            enviar e-mail
          </S.Button>
          <S.Button color="#2FDF46" variant="success" showIcon icon="whats">
            enviar mensagem
          </S.Button>
        </S.ButtonGroup>
      </S.ProfileInfo>
    </S.ProfileContainer>
  );
};

Profile.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  // work: PropTypes.string,
  // email: PropTypes.string,
  telephony: PropTypes.string,
  phone: PropTypes.string,
};
Profile.defaultProps = {
  image: '',
  name: '',
  // work: '',
  // email: '',
  telephony: '',
  phone: '',
};

export default Profile;
