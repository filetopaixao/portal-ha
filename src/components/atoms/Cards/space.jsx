import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Badge from '@components/atoms/Badge';

import Bank from '@assets/images/bank.svg';
import Marketing from '@assets/images/marketing.svg';
import University from '@assets/images/university.svg';
import Store from '@assets/images/shopping.svg';
import TutorialPlataforma from '@assets/images/tutorial_plataforma.svg';
import VendaFisica from '@assets/images/venda_fisica.svg';
import VendaOnline from '@assets/images/venda_online.svg';
import Produtos from '@assets/images/produtos.svg';
import MaterialMarketing from '@assets/images/material_marketing.svg';
import PlanaformaDeEnsino from '@assets/images/plataforma_de_ensino.svg';
import ParceriasIndicacoes from '@assets/images/parcerias_indicacoes.svg';
import AcompanhamentoEverHelp from '@assets/images/Acompanhamento_EVER_help.svg';

import * as S from './styled';

const images = {
  bank: Bank,
  marketing: Marketing,
  university: University,
  store: Store,
  shopping: Store,
  tutorialPlataforma: TutorialPlataforma,
  vendaFisica: VendaFisica,
  vendaOnline: VendaOnline,
  produtos: Produtos,
  materialMarketing: MaterialMarketing,
  plataformaDeEnsino: PlanaformaDeEnsino,
  parceriasIndicacoes: ParceriasIndicacoes,
  acompanhamentoEverHelp: AcompanhamentoEverHelp,
};

const CardSpace = ({ className, variant, icon, title, isComing, isSupport }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.CardSpace
      variant={variant}
      className={className}
      primaryColor={themeStore.primaryColor}
      secondaryColor={themeStore.secondaryColor}
      tertiaryColor={themeStore.tertiaryColor}
      theme={themeStore.themeName}
    >
      {isComing ? <Badge /> : null}
      <S.CardSpaceIcon
        isSupport={isSupport}
        isComing={isComing}
        src={images[icon]}
        primaryColor={themeStore.primaryColor}
      >
        <div alt="title" />
      </S.CardSpaceIcon>
      <S.CardSpaceTitle className={className} isComing={isComing}>
        {title}
      </S.CardSpaceTitle>
    </S.CardSpace>
  );
};

CardSpace.propTypes = {
  className: PropTypes.string,
  isComing: PropTypes.bool,
  variant: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string,
  isSupport: PropTypes.bool,
};
CardSpace.defaultProps = {
  className: '',
  isComing: false,
  variant: '',
  icon: '',
  title: '',
  isSupport: false,
};

export default CardSpace;
