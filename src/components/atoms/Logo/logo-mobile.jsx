import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import logo from '@assets/images/logo-fpass-mobile.png';

import * as S from './styled';

const LogoMobile = ({ size }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Container>
      <S.Logo size={size}>
        <img src={themeStore.logo ? themeStore.logo : logo} alt="logo" />
      </S.Logo>
    </S.Container>
  );
};
LogoMobile.propTypes = {
  size: PropTypes.string.isRequired,
};

export default LogoMobile;
