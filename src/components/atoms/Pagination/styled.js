import styled from 'styled-components';

export const Container = styled.div`
  color: #7c7c7c;
  display: flex;
  float: right;
`;

export const Number = styled.div`
  margin-right: 7px;
  margin-top: 3px;
  height: 22px;
  width: 22px;
  font-size: 14px;
  text-align: center;
  border-radius: ${(props) => (props.selected ? '50px' : '')};
  box-shadow: ${(props) => (props.selected ? '3px 3px 9px' : '')};
  font-weight: ${(props) => (props.selected ? 'bold' : '')};
`;

export const Input = styled.input`
  border: solid 1px #bdbdbd;
  height: 23px;
  border-radius: 7px 0px 0px 7px;
  width: 43px;
  margin-top: 2px;
`;

export const Button = styled.span`
  background-color: #01a3ff;
  color: #ffff;
  padding: 1px 4px;
  border-radius: 0px 7px 7px 0px;
  font-size: 16px;
  margin-left: -1px;
`;

export const Part = styled.span`
  margin-right: 20px;
  margin-left: 10px;
`;

export const TextPage = styled.span`
  margin-right: 17px;
`;

export default Container;
