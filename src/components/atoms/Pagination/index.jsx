import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Pagination = ({ pages }) => (
  <S.Container>
    {pages.map(({ number, selected }) => (
      <S.Number selected={selected} key={number}>
        {number}
      </S.Number>
    ))}
    <S.Part>|</S.Part>
    <S.TextPage>Ir para página</S.TextPage>
    <S.Input type="number" />
    <S.Button>{'>'}</S.Button>
  </S.Container>
);

Pagination.propTypes = {
  pages: PropTypes.arrayOf,
};
Pagination.defaultProps = {
  pages: [],
};

export default Pagination;
