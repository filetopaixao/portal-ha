import styled from 'styled-components';

export const ItenLink = styled.li`
  display: flex;
  align-items: center;

  background-color: ${(props) => (props.selected ? `${props.selectedBackground}33` : '')};
  //background-color: ${(props) => (props.retractable ? '#ffff' : '')};

  cursor: ${(props) => (props.disabled ? '' : 'pointer')};

  &:hover {
    background: rgba(1, 163, 255, 0.05);
  }
`;

export const IconLink = styled.div`
  margin-right: 10px;

  border-radius: 0px 40px 40px 0px;

  background-color: ${(props) => (props.selected ? props.selectedBackground : '')};
`;

export const ContainerText = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;
`;

export const Text = styled.div`
  font-size: 12px;
  font-weight: bold;

  // min-width: 140px;

  /* padding: 15px 0px 15px 20px; */

  color: ${(props) => (props.selected ? props.selectedColor : '')};
  color: ${(props) => (props.disabled ? 'rgba(94,94,94, 0.3)' : '')};

  @media (min-width: 1500px) {
    font-size: 16px;
  }
`;

export const Count = styled.div`
  /* padding: 15px 40px 15px 20px; */
  color: ${(props) => (props.selected ? 'rgba(1,163,255)' : '')};
  color: ${(props) => (props.disabled ? 'rgba(94,94,94, 0.3)' : '')};
  font-size: 16px;
  text-align: right;
`;

export const Drop = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  > div {
    /* margin: 0 auto;
    display: block; */

    display: flex;
    align-items: center;
  }

  color: ${(props) => (props.selected ? 'rgba(1,163,255)' : '')};
  color: ${(props) => (props.disabled ? 'rgba(94,94,94, 0.3)' : '')};
`;

export default ItenLink;
