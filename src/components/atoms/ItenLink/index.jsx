/* eslint-disable no-nested-ternary */
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Icon from '@components/atoms/Icon';
import PropTypes from 'prop-types';

import { Actions as MenuActions } from '@redux/ducks/menu';

import * as S from './styled';

export const ItenLink = ({
  handleSelecteLink,
  text,
  icon,
  selected,
  disabled,
  isPermission,
  count,
  haveChildren,
  route,
}) => {
  const dispatch = useDispatch();
  const menu = useSelector((state) => state.menu);
  const themeStore = useSelector((state) => state.theme);

  const [drop, setDrop] = useState('dropdown');

  function changeDrop() {
    if (drop === 'dropdown') {
      dispatch(MenuActions.showLink(text));
      setDrop('dropup');
    } else {
      dispatch(MenuActions.hideLink(text));
      setDrop('dropdown');
    }
  }

  const history = useHistory();

  const handleExecute = () => {
    handleSelecteLink(route);

    if (route !== null) {
      if (route !== 'submenu') {
        history.push(route);
      }
    }
  };

  return (
    <>
      {route !== null ? (
        <S.ItenLink
          onClick={() => handleExecute()}
          selected={selected}
          disabled={disabled}
          isPermission={isPermission}
          retractable={menu.retractable}
          selectedBackground={themeStore.primaryColor}
        >
          <S.IconLink selected={selected} selectedBackground={themeStore.primaryColor}>
            <Icon
              icon={icon}
              position="left"
              color={selected ? '#fff' : disabled ? 'rgba(94,94,94, 0.3)' : '#707070'}
            />
          </S.IconLink>
          {!menu.retractable ? (
            <S.ContainerText count={count} haveChildren={haveChildren}>
              <S.Text selected={selected} disabled={disabled} count={count} selectedColor={themeStore.primaryColor}>
                {text}
              </S.Text>
              {count > 0 ? (
                <S.Count selected={selected} disabled={disabled}>
                  {count}
                </S.Count>
              ) : (
                ''
              )}
              {haveChildren ? (
                <S.Drop selected={selected} disabled={disabled} onClick={changeDrop}>
                  <Icon icon={drop} position="right" color="#707070" />
                </S.Drop>
              ) : (
                ''
              )}
            </S.ContainerText>
          ) : (
            ''
          )}
        </S.ItenLink>
      ) : null}
    </>
  );
};

ItenLink.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
  isPermission: PropTypes.bool,
  count: PropTypes.number,
  haveChildren: PropTypes.bool,
  route: PropTypes.string,
  handleSelecteLink: PropTypes.func,
};
ItenLink.defaultProps = {
  text: 'Home',
  icon: 'home',
  selected: false,
  disabled: false,
  isPermission: true,
  count: 0,
  haveChildren: false,
  route: '',
  handleSelecteLink: () => {},
};

export default ItenLink;
