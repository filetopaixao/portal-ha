import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Badge = ({ text }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Badge theme={themeStore.secondaryColor}>
      <S.BadgeText>{text}</S.BadgeText>
    </S.Badge>
  );
};

Badge.propTypes = {
  text: PropTypes.string,
};
Badge.defaultProps = {
  text: 'em breve!',
};

export default Badge;
