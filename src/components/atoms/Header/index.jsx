/* eslint-disable no-extra-boolean-cast */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import Notification from '@assets/images/notification.svg';
// import Language from '@assets/images/language.svg';

import Icon from '@components/atoms/Icon';
import MenuProfile from '@assets/images/arrowDown.svg';

import { Actions as MenuActions } from '@redux/ducks/menu';
import { getUser } from '@services/user';

import Logo from '@assets/images/logo_ever_white.png';

import Count from './count';

import * as S from './styled';

const Header = () => {
  const themeStore = useSelector((state) => state.theme);
  const dispatch = useDispatch();
  const retractable = useSelector((state) => state.menu.retractable);

  const typeUser = (typeGender) => {
    switch (typeGender.type) {
      case '1':
        return 'Admin';
      case '2':
        return typeGender.gender === 'male' ? 'Empreendedor' : 'Empreendedora';
      case '3':
        return typeGender.gender === 'male' ? 'Parceiro' : 'Parceira';
      default:
        return '';
    }
  };

  const [userData, setUserData] = useState({});

  useEffect(() => {
    getUser()
      .then((response) => {
        const userDataResponse = response.data;
        setUserData({ ...userDataResponse, photo: userDataResponse.image.url });
      })
      .catch((err) => {
        setUserData({ first_name: '', type_id: '', gender: '', photo: Logo });
        console.log(err);
      });
  }, []);

  const [isMenuVisible, setIsMenuVisible] = useState(false);

  const handleMenu = () => {
    setIsMenuVisible(!isMenuVisible);

    setTimeout(() => {
      setIsMenuVisible(false);
    }, 100000);
  };

  const handleCloserApp = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('data_user_email');
    localStorage.removeItem('data_user_password');
  };

  return (
    <S.HeaderContainer>
      <S.HeaderRetractable onClick={() => dispatch(MenuActions.retractableMenu())}>
        <Icon icon={retractable ? 'menu' : 'menuRetractable'} color="#000" />
      </S.HeaderRetractable>
      <S.HeaderRetractableMobile onClick={() => dispatch(MenuActions.retractableMobileMenu())}>
        <Icon icon="menu" color={themeStore.themeName === 'Dark' ? '#979EA8' : '#82868D'} />
      </S.HeaderRetractableMobile>

      {!userData ? (
        <h3>Carregando informações</h3>
      ) : (
        <S.HeaderRight>
          <S.HeaderNotification>
            <img src={Notification} alt="Notificação" className="img-sizing" />
            <S.HeaderNotificationCount primaryColor={themeStore.primaryColor}>
              <Count number={0} />
            </S.HeaderNotificationCount>
          </S.HeaderNotification>
          <S.HeaderProfile>
            <div className="button_menu_profile">
              <img src={userData.photo} alt="Profile" />
              <button type="button" onClick={() => handleMenu()}>
                <img src={MenuProfile} alt="Menu" />
              </button>
            </div>
            {isMenuVisible ? (
              <S.CardProfile
                theme={themeStore.themeName}
                primaryColor={themeStore.primaryColor}
                secondaryColor={themeStore.secondaryColor}
                tertiaryColor={themeStore.tertiaryColor}
              >
                <div className="profile_image">
                  <div>
                    <S.ImageProfileCard image={userData.photo} />
                    <button type="button">
                      <div className="icon-camera" />
                    </button>
                  </div>
                  <h3>{userData.first_name}</h3>
                  <p>{typeUser({ type: userData.type_id, gender: userData.gender })}</p>
                </div>
                <div className="profile_options">
                  <ul>
                    <li>
                      <Link to="/myAccount/tabcontent/content1" onClick={() => setIsMenuVisible(false)}>
                        Minha Conta
                      </Link>
                    </li>
                    <li>
                      <Link to="/myAccount/tabcontent/content4" onClick={() => setIsMenuVisible(false)}>
                        Dados bancários
                      </Link>
                    </li>
                    <li>
                      <Link
                        to="/myAccount/tabcontent/activeChangePass/content1"
                        onClick={() => setIsMenuVisible(false)}
                      >
                        Alterar Senha
                      </Link>
                    </li>
                    <li>
                      <Link to="/" onClick={() => handleCloserApp()}>
                        Sair
                      </Link>
                    </li>
                  </ul>
                </div>
              </S.CardProfile>
            ) : null}
          </S.HeaderProfile>
        </S.HeaderRight>
      )}
    </S.HeaderContainer>
  );
};

export default Header;
