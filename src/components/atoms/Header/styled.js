import styled from 'styled-components';

import IconPhoto from '@assets/images/icon_photo_profile.svg';

export const HeaderContainer = styled.header`
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  flex-direction: row;

  .img-sizing {
    @media (max-width: 1400px) {
      width: 20px;
    }

    @media (min-width: 1400px) {
      height: 24px;
      width: 24px;
    }
  }
`;

export const HeaderRetractable = styled.div`
  cursor: pointer;
  div {
    margin-left: 12px;
  }
  display: none;

  @media (min-width: 992px) {
    margin-top: 20px;
    display: block;
    div {
      margin-left: 0px;
    }
  }
`;

export const HeaderRetractableMobile = styled.div`
  cursor: pointer;
  div {
    margin-left: 12px;
  }
  display: block;

  @media (min-width: 992px) {
    margin-top: 20px;
    display: none;
    div {
      margin-left: 0px;
    }
  }
`;

export const HeaderRight = styled.header`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex-direction: row;

  height: 70px;
  width: 100%;

  padding: 0 15px;
`;

export const HeaderNotification = styled.div`
  display: block;

  position: relative;

  height: 19px;
  width: 19px;
  margin-right: 20px;

  @media (min-width: 768px) {
    height: 29px;
    width: 31px;
  }

  img {
    /* height: 20px; */
    width: 20px;
  }
`;

export const HeaderNotificationCount = styled.div`
  font-size: 8px;
  font-weight: normal;
  text-align: center;

  display: flex;
  justify-content: center;
  align-items: center;

  border-radius: 50px;
  position: absolute;
  right: -3px;
  top: -4px;
  height: 10px;
  width: 10px;

  @media (min-width: 768px) {
    height: 15px;
    width: 15px;
    font-size: 10px;
  }

  background: ${(props) => props.primaryColor};
  color: #fff;
`;

export const HeaderLanguage = styled.div`
  display: block;
  height: 20px;
  width: 20px;

  margin-right: 20px;
`;

export const HeaderProfile = styled.div`
  > div:first-child {
    display: flex;
    justify-content: space-between;
    align-items: center;

    height: 45px;
    width: 65px;

    @media (min-width: 1400px) {
      width: 90px;
    }
  }

  img {
    height: 100%;
    width: 100%;
    max-width: 50px;
    max-height: 50px;

    border-radius: 50%;

    @media (min-width: 1400px) {
      max-width: 45px;
      max-height: 45px;
    }
  }

  .button_menu_profile {
    button {
      margin-left: 10px;
      transform: rotate(180deg);
      img {
        width: 18px;
        height: 18px;

        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        transform: rotate(180deg);
      }
    }
  }

  button {
    width: 30px;

    border: none;
    cursor: pointer;

    background: transparent;

    @media (min-width: 1400px) {
      height: 20px;
      width: 24px;
    }
  }
`;

export const CardProfile = styled.div`
  position: absolute;
  right: 1.5%;

  width: 220px;
  max-height: 180px;
  height: 100%;

  z-index: 10;

  > div.profile_image {
    text-align: center;

    padding: 20px;

    border-radius: 8px 8px 0 0;

    background: ${(props) =>
      props.tertiaryColor
        ? `transparent linear-gradient(270deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
        : `transparent linear-gradient(270deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};

    h3 {
      margin-top: 10px;
    }

    h3,
    p {
      color: #fff;
    }

    > div {
      width: 90px;
      margin: 0;
      margin: 0 auto;

      button {
        display: flex;
        justify-content: center;
        align-items: center;

        position: absolute;
        top: 44%;
        right: 30%;

        width: 30px;
        height: 30px;

        border-radius: 50%;

        background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

        color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

        .icon-camera {
          -webkit-mask-image: url(${IconPhoto});
          mask-image: url(${IconPhoto});
          mask-repeat: no-repeat;
          mask-size: 20px;
          background-color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
          width: 20px;
          height: 20px;
        }
      }
    }
  }

  div.profile_options {
    padding: 20px;

    border-radius: 0 0 8px 8px;

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

    li {
      list-style: none;
    }

    li {
      margin-bottom: 15px;
    }

    a {
      text-decoration: none;
      cursor: pointer;

      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }
  }
`;

export const ImageProfileCard = styled.div`
  width: 90px;
  height: 90px;

  margin: 0 auto;

  border-radius: 50%;

  background-image: url(${(props) => props.image});
  background-repeat: no-repeat;
  background-size: contain;
`;
