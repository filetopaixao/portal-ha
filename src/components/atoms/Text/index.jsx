import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Text = ({ className, children, size, variant, onClick }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Text variant={variant} size={size} className={className} onClick={onClick} theme={themeStore.themeName}>
      {children}
    </S.Text>
  );
};

Text.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  size: PropTypes.string,
  variant: PropTypes.string,
  onClick: PropTypes.func,
};
Text.defaultProps = {
  className: '',
  size: 'sm',
  children: '',
  variant: 'normal',
  onClick: () => false,
};

export default Text;
