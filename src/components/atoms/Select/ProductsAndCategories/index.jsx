import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { getProductByCategory } from '@services/products';

import * as S from './styled';

const SelectCategories = ({ className, options, onChange, defaultValue }) => {
  const [productCategories, setProductCategories] = useState([]);

  useEffect(() => {
    const serializable = options.map((item) => {
      const arrayAux = {
        id: item.id,
        name: item.name,
        products: [],
      };
      getProductByCategory(item.id)
        .then((response) => {
          response.data.data.map((product) => arrayAux.products.push(product));
        })
        .catch((err) => {
          if (err) {
            console.log(err);
          }
        });
      return arrayAux;
    });
    console.log(serializable);
    setProductCategories(serializable);
  }, [options]);

  return (
    <S.ContainerSelect className={className}>
      <S.Select onChange={onChange}>
        <S.Option hidden>{defaultValue}</S.Option>
        {productCategories.map(({ name, id }) => (
          <S.Option value={id} key={id}>
            {name}
          </S.Option>
        ))}
      </S.Select>
    </S.ContainerSelect>
  );
};

SelectCategories.propTypes = {
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.object),
};
SelectCategories.defaultProps = {
  className: '',
  defaultValue: 'Selecione uma opção',
  onChange: () => false,
  options: [],
};

export default SelectCategories;
