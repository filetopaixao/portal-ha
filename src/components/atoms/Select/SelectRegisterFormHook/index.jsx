import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Select = ({
  variant,
  width,
  id,
  name,
  minWidth,
  className,
  options,
  register,
  defaultValue,
  error,
  errorMessage,
}) => (
  <S.ContainerSelect width={width} minWidth={minWidth} className={className}>
    <S.Select variant={variant} ref={register} id={id} name={name}>
      <S.Option hidden>{defaultValue}</S.Option>
      {options.map(({ label, value }) => (
        <S.Option value={value} key={label}>
          {label}
        </S.Option>
      ))}
    </S.Select>

    {error && <S.ErrorMessage>{errorMessage.message}</S.ErrorMessage>}
  </S.ContainerSelect>
);

Select.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  variant: PropTypes.string,
  width: PropTypes.string,
  minWidth: PropTypes.string,
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  register: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  options: PropTypes.arrayOf(PropTypes.object),
  error: PropTypes.string,
  errorMessage: PropTypes.string,
};
Select.defaultProps = {
  id: '',
  name: '',
  variant: 'secondary',
  width: '',
  minWidth: '',
  className: '',
  defaultValue: 'Selecione uma opção',
  register: '',
  options: [],
  error: '',
  errorMessage: '',
};

export default Select;
