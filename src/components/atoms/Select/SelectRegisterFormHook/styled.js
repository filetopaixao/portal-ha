import styled, { css } from 'styled-components';

import Container from '../../Container';

import ArrowTop from '../../../../assets/images/arrowTop.svg';

const variants = {
  primary: () => css`
    background: #f1eeee 0% 0% no-repeat padding-box;
    border: 1px solid #cdcdcd;
    color: #5e5e5eb3;
  `,
  secondary: () => css`
    background: #ffffff 0% 0% no-repeat padding-box;
    border: 1px solid #eaeaea;
    color: #5e5e5eb3;
  `,
  adminMode: () => css`
    background: #fbfbfb 0% 0% no-repeat padding-box;
    border: 1px solid #cecece;
    border-radius: 15px;
    letter-spacing: 0px;
    color: #82868d;
  `,
};

export const ContainerSelect = styled(Container)`
  position: relative;

  width: ${(props) => props.width};
  min-width: ${(props) => props.minWidth};

  &::after {
    display: block;
    visibility: visible;
    width: 10px;
    height: 10px;

    position: absolute;
    right: 13.2px;
    top: 14px;
    z-index: 1;

    background: url(${ArrowTop}) no-repeat;
    background-size: contain;

    transform: rotate(180deg);
  }
`;

export const Select = styled.select`
  width: 100%;
  height: 40px;
  padding-left: 20px;
  position: relative;

  appearance: none;
  border-radius: 10px;

  ${(props) => variants[props.variant]};
`;

export const ErrorMessage = styled.div`
  font-size: 12px;
  text-align: left;
  text-transform: uppercase;

  margin-top: 5px;

  color: #f14479;
`;

export const Option = styled.option`
  font-size: 13px;
  font-weight: normal;
  text-transform: uppercase;

  color: #5e5e5eb3;
`;

export const IconColor = styled.p`
  font-size: 13px;
  font-weight: normal;

  color: #000;
`;
