import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Select = ({ width, minWidth, className, options, onChange, defaultValue }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ContainerSelect className={className} width={width} minWidth={minWidth}>
      <S.Select onChange={onChange} theme={themeStore.themeName}>
        <S.Option hidden>{defaultValue}</S.Option>
        {options.map(({ label, value }) => (
          <S.Option value={value} key={label}>
            {label}
          </S.Option>
        ))}
      </S.Select>
    </S.ContainerSelect>
  );
};

Select.propTypes = {
  width: PropTypes.string,
  minWidth: PropTypes.string,
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.object),
};
Select.defaultProps = {
  width: '',
  minWidth: '',
  className: '',
  defaultValue: 'Selecione uma opção',
  onChange: () => false,
  options: [],
};

export default Select;
