import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Spacer = ({ size }) => {
  const themeStore = useSelector((state) => state.theme);
  return <S.Spacer size={size} theme={themeStore.themeName} />;
};

Spacer.propTypes = {
  size: PropTypes.string.isRequired,
};

export default Spacer;
