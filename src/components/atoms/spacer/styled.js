import styled from 'styled-components';

export const Spacer = styled.div`
  border-style: solid;
  border-bottom-width: 1px;
  border-top-width: 0;
  border-right-width: 0;
  border-left-width: 0;
  border-color: ${(props) => (props.theme === 'Dark' ? ' #2F3743' : '#EAEAEA')};
  margin-left: 11px;
  max-width: ${(props) => (props.size === 'sm' ? '40px' : '')};
  width: ${(props) => (props.size === 'md' ? '90%' : '')};
`;

export default Spacer;
