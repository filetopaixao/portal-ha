import styled, { css } from 'styled-components';

const variants = {
  login: () => css`
    background: #ccedff;
    color: #2d2d3a;
    border: none;
    border-radius: 10px;
  `,

  formData: () => css`
    background: #f2f2f2;
    color: #2d2d3a;
    border: none;
    border-radius: 10px;
  `,

  adminMode: () => css`
    background: #fbfbfb 0% 0% no-repeat padding-box;
    border: 1px solid #eaeaea;
    border-radius: 15px;
    color: #82868d;
  `,

  default: () => css`
    background: #fff;
    color: #2d2d3a;
    border: none;
    border-radius: 10px;
    &:active {
      background: inherit;
    }
    &:-webkit-autofill {
      -webkit-box-shadow: inherit;
      background: inherit;
      -webkit-text-fill-color: inherit;
    }
  `,
};

export const InputContainer = styled.div`
  margin-bottom: 30px;
  position: relative;
`;

export const Input = styled.input`
  font-size: 12px;
  display: block;
  height: 40px;

  padding: 14px 0 14px 30px;
  width: 100%;

  ${(props) => (props.variant ? variants[props.variant] : variants.default)};

  &:disabled {
    cursor: not-allowed;
    background: #eaeaea;
  }

  @media (min-width: 1400px) {
    font-size: 16px;
  }
`;

export const ErrorMessage = styled.div`
  font-size: 12px;
  text-align: left;
  text-transform: uppercase;
  position: absolute;

  margin-top: 5px;

  color: #f14479;
`;

export default Input;
