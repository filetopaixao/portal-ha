import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Input = ({
  className,
  disabled,
  id,
  name,
  onBlur,
  placeholder,
  type,
  variant,
  error,
  errorMessage,
  register,
  onChange,
}) => (
  <S.InputContainer>
    <S.Input
      ref={register}
      type={type}
      id={id}
      className={className}
      name={name}
      placeholder={placeholder}
      onBlur={onBlur}
      disabled={disabled}
      variant={variant}
      autocomplete="off"
      onChange={onChange}
    />

    {error && <S.ErrorMessage>{errorMessage.message}</S.ErrorMessage>}
  </S.InputContainer>
);

Input.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  onBlur: PropTypes.func,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  variant: PropTypes.string,
  error: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  errorMessage: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  register: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  onChange: PropTypes.func,
};
Input.defaultProps = {
  className: '',
  disabled: false,
  id: '',
  name: '',
  onBlur: () => false,
  placeholder: 'input default',
  type: 'text',
  variant: '',
  error: '',
  errorMessage: '',
  register: '',
  onChange: () => {},
};

export default Input;
