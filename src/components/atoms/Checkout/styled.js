import styled, { css } from 'styled-components';

import ButtonStyled from '@components/atoms/Button/styled';
import Button from '@components/atoms/Button';
import Ctn from '@components/atoms/Container';
import Ipt from '@components/atoms/Input';
import Lbl from '@components/atoms/Label';
import Txt from '@components/atoms/Text';

import Coupon from '@assets/images/discount-coupon.svg';
import Ecommerce from '@assets/images/ecommerce.svg';

const variants = {
  normal: () => css`
    background-color: #fff;
  `,
  resume: () => css`
    background: linear-gradient(45deg, #0077fe 40%, #7adbba 100%);
  `,
  cupom: () => css`
    background: linear-gradient(45deg, #01a3ff 40%, #83c2f5 100%);
  `,
};

const images = {
  coupon: () => css`
    &::before {
      content: '';
      clear: both;
      display: block;
      visibility: visible;

      width: 13rem;
      height: 13rem;
      position: absolute;
      background-image: url(${Coupon});
      background-size: contain;
      background-repeat: no-repeat;

      @media (min-width: 1200px) {
        width: 16rem;
        height: 16rem;
        top: 50px;
      }
    }
  `,
  ecommerce: () => css`
    &::before {
      content: '';
      clear: both;
      display: block;
      visibility: visible;

      width: 13rem;
      height: 13rem;
      position: absolute;
      background-image: url(${Ecommerce});
      background-size: contain;
      background-repeat: no-repeat;

      @media (min-width: 1200px) {
        width: 16rem;
        height: 16rem;
        top: 50px;
      }
    }
  `,
};

export const ContainerImage = styled.div`
  display: flex;
  justify-content: center;
`;

export const ContentImage = styled.div`
  content: url(${Ecommerce});
  width: 13rem;
  height: 13rem;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: 16rem;
  }
`;

export const Container = styled(Ctn)`
  padding: 15px 20px;

  @media (min-width: 600px) {
    padding: 48px;
  }

  border-radius: 15px;

  ${(props) => variants[props.variant]};
`;

export const ContainerCheckout = styled.div`
  width: 100%;
`;

export const CheckoutTitle = styled.h2`
  font-size: 16px;
  letter-spacing: 0px;
  font-weight: 600;
  text-align: center;

  width: 100%;
  margin-bottom: 30px;

  opacity: 1;
  color: #5e5e5e;

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;

export const Input = styled(Ipt)`
  font-size: 14px;
  background: #f2f2f2 0% 0% no-repeat padding-box;
  border-radius: 15px;
  opacity: 1;

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;

export const Label = styled(Lbl)`
  font-size: 14px;

  padding-left: 10px;
  margin-bottom: 5px;

  @media (min-width: 1500px) {
    font-size: 16px;
    margin-bottom: 7px;
  }
`;

export const ButtonGroup = styled(Button.Group)`
  margin: 30px 0 0;

  @media (min-width: 768px) {
    margin-top: 49px;
  }
`;

export const Btn = styled(ButtonStyled)`
  width: 100%;

  @media (min-width: 768px) {
    padding: 15px 0;
  }
`;

export const Text = styled(Txt)`
  font-size: 14px;
  text-transform: uppercase;

  display: block;
  width: 100%;

  color: #fff;

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;

/** *****************************Container Resume********************************* */

export const ContainerResume = styled(Container)`
  min-height: 230px;

  padding: 15px 20px;

  @media (min-width: 600px) {
    padding: 48px;
  }

  position: relative;
  overflow: hidden;

  ${(props) => images[props.image]}

  &.resumeCheckout {
    p {
      text-align: right;
    }
  }
`;

export const ContainerResumeInfo = styled(Ctn)`
  justify-content: flex-end;
`;

export const ContainerText = styled.div`
  line-height: 25px;

  width: 100%;

  &:not(:last-child) {
    margin-bottom: 15px;
  }
`;

export const TitleResume = styled(Text)`
  text-align: right;
  font-size: 14px;
  font-weight: normal;
  letter-spacing: 0px;

  text-transform: uppercase;
  opacity: 1;

  color: #fdfefe;
`;

export const TextResume = styled(Text)`
  font-size: 25px;
  font-weight: 600;
`;

export const ContainerButtonVideo = styled.div`
  position: absolute;
  right: 20px;
  bottom: 20px;
  z-index: 2;

  img {
    width: 35px;
    height: 24px;
  }
`;
