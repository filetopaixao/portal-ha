import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Checkout = ({ className, dataInput, title, label, btnText }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.CheckoutContainer className={`createCheckout ${className}`} variant="normal" theme={themeStore.themeName}>
      <S.CheckoutTitle theme={themeStore.themeName}>{title}</S.CheckoutTitle>

      <S.CheckoutContent>
        <S.CheckoutLabel theme={themeStore.themeName}>{label}</S.CheckoutLabel>
        <S.CheckoutInput
          id={dataInput.id}
          name={dataInput.name}
          onChange={dataInput.onChange}
          placeholder={dataInput.placeholder}
          type={dataInput.type}
          value={dataInput.value}
          theme={themeStore.themeName}
        />
      </S.CheckoutContent>

      <S.CheckoutButtonGroup className="center">
        <S.CheckoutButton primaryColor={themeStore.primaryColor} onClick={dataInput.onClick}>
          <S.CheckoutText size="sm" variant="medium">
            {btnText}
          </S.CheckoutText>
        </S.CheckoutButton>
      </S.CheckoutButtonGroup>
    </S.CheckoutContainer>
  );
};
Checkout.propTypes = {
  className: PropTypes.string,
  dataInput: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    onChange: PropTypes.func,
    onClick: PropTypes.func,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
  }),
  btnText: PropTypes.string,
  title: PropTypes.string,
  label: PropTypes.string,
};
Checkout.defaultProps = {
  className: '',
  dataInput: {},
  btnText: '',
  title: '',
  label: '',
};

export default Checkout;
