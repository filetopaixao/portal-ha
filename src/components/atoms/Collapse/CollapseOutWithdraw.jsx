import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const CollapseOut = ({ children, title, titlePosition, active, onClick, id }) => {
  return (
    <>
      <S.ContainerWithdraw out active={active}>
        <S.ContainerTitle active={active} out>
          <S.Title active={active} position={titlePosition}>
            {title}
          </S.Title>
          <S.Close onClick={(e) => onClick(e)} active={active} id={id} />
        </S.ContainerTitle>
      </S.ContainerWithdraw>
      <S.ContainerContent active={active} out>
        {children}
      </S.ContainerContent>
    </>
  );
};

CollapseOut.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  active: PropTypes.bool,
  title: PropTypes.string,
  titlePosition: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.string,
};
CollapseOut.defaultProps = {
  children: '',
  active: false,
  title: 'Title',
  titlePosition: 'center',
  onClick: () => false,
  id: '',
};

export default CollapseOut;
