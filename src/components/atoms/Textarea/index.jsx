import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Textarea = ({ id, className, name, value, placeholder, onChange, disabled, rows, cols }) => (
  <S.Textarea
    id={id}
    className={className}
    name={name}
    value={value}
    placeholder={placeholder}
    onChange={onChange}
    disabled={disabled}
    rows={rows}
    cols={cols}
  />
);

Textarea.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  cols: PropTypes.number,
  value: PropTypes.string,
  rows: PropTypes.number,
};

Textarea.defaultProps = {
  placeholder: 'default textarea...',
  rows: 30,
  cols: 500,
  className: '',
  disabled: false,
  id: '',
  name: '',
  onChange: () => false,
  value: '',
};

export default Textarea;
