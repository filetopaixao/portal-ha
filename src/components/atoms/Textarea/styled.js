import styled from 'styled-components';

export const Textarea = styled.textarea`
  background: #f2f2f2 0% 0% no-repeat padding-box;
  border-radius: 15px;
  opacity: 1;
  padding: 14px 30px;
  border: none;
  font-size: 16px;
  font-family: 'Poppins', sans-serif !important;
`;

export default Textarea;
