import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const ToggleSwitch = ({ value, onChange }) => (
  <S.Label>
    <input type="checkbox" name="on" id="on" onClick={onChange} value={value} />
    <div className="slider">
      <div />
    </div>
  </S.Label>
);

ToggleSwitch.propTypes = {
  value: PropTypes.bool,
  onChange: PropTypes.func,
};
ToggleSwitch.defaultProps = {
  value: false,
  onChange: () => false,
};

export default ToggleSwitch;
