import React, { useState } from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';

import Toast from './index';

export default {
  title: 'molecules/Toasts',
  component: Toast,
  decorators: [withKnobs],
};

const variants = ['success', 'danger', 'warnig'];
const icons = ['success'];

export const element = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isToastVisible, setIsToastVisible] = useState(true);

  return (
    <ContainerStorybook>
      <Toast
        variant={select('Variant', variants, 'success')}
        icon={select('Icon', icons, 'success')}
        showIcon={select('Show Icon', [true, false], true)}
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Concluido com sucesso!!
      </Toast>
    </ContainerStorybook>
  );
};

element.story = {
  name: 'Default',
};
