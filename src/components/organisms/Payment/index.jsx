import React from 'react';
import { useFormik } from 'formik';
// import PropTypes from 'prop-types';

import { Input, Label } from '@components/atoms';

import { cpfMask, emailMask, phoneMask, cepMask, dateMask } from '@utils/masksForms';
import { DataPersonalValidation } from './validations';

import * as S from './styled';

const DataPersonal = () => {
  const { handleBlur, handleChange, handleSubmit, values, errors } = useFormik({
    initialValues: {
      name: '',
      cpf: '',
      email: '',
      whats: '',
      phone: '',
      cep: '',
      birthday: '',
      genrer: '',
    },
    onSubmit(inputValues) {
      console.log(inputValues);
    },
    validationSchema: DataPersonalValidation,
  });

  return (
    <>
      <S.Form onSubmit={handleSubmit}>
        <S.ContainerInput className="dataPersonal">
          <Label htmlFor="dataPersonal__name">Nome do destinátário:</Label>
          <Input
            id="dataPersonal__name"
            type="text"
            name="name"
            placeholder="Nome do destinatário"
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.name}
          />
        </S.ContainerInput>

        <S.ContainerInput>
          <Label htmlFor="dataPersonal__cpf">CPF:</Label>
          <Input
            id="dataPersonal__cpf"
            type="text"
            name="cpf"
            placeholder="CPF"
            value={cpfMask(values.cpf)}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.cpf}
          />
        </S.ContainerInput>

        <S.ContainerInput className="dataPersonal">
          <Label htmlFor="dataPersonal__email">E-mail:</Label>
          <Input
            id="dataPersonal__email"
            type="text"
            name="email"
            placeholder="Email do destinatário"
            value={emailMask(values.email)}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.email}
          />
        </S.ContainerInput>

        <S.ContainerInput>
          <Label htmlFor="dataPersonal__whats">WhatsApp:</Label>
          <Input
            id="dataPersonal__whats"
            type="text"
            name="whats"
            placeholder="WhatsApp"
            value={phoneMask(values.whats)}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.whats}
          />
        </S.ContainerInput>

        <S.ContainerInput>
          <Label htmlFor="dataPersonal__phone">Telefone de contato:</Label>
          <Input
            id="dataPersonal__phone"
            type="text"
            name="phone"
            placeholder="Telefone do Destinatário"
            value={phoneMask(values.phone)}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.phone}
          />
        </S.ContainerInput>

        <S.ContainerInput>
          <Label htmlFor="dataPersonal__cep">Cep:</Label>
          <Input
            id="dataPersonal__cep"
            type="text"
            name="cep"
            placeholder="CEP do Destinatário"
            value={cepMask(values.cep)}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.cep}
          />
        </S.ContainerInput>

        <S.ContainerInput>
          <Label htmlFor="dataPersonal__birthday">Data de nascimento:</Label>
          <Input
            id="dataPersonal__birthday"
            type="text"
            name="birthday"
            placeholder="Data de Nascimento"
            value={dateMask(values.birthday)}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.birthday}
          />
        </S.ContainerInput>

        <S.ContainerInput>
          <Label htmlFor="dataPersonal__genrer">Gênero:</Label>
          <Input
            id="dataPersonal__genrer"
            type="text"
            name="genrer"
            placeholder="Sexo"
            value={values.genrer}
            onChange={handleChange}
            onBlur={handleBlur}
            errorMessage={errors.genrer}
          />
        </S.ContainerInput>
        <button name="save" id="save" type="submit">
          save
        </button>
      </S.Form>
    </>
  );
};

DataPersonal.propTypes = {};

DataPersonal.defaultProps = {};

export default DataPersonal;
