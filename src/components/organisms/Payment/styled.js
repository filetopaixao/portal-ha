import styled from 'styled-components';

import { Button, Container, Label as Lbl, Text } from '@components/atoms';

export const Label = styled(Lbl)`
  font-size: 14px;
  font-weight: 400;

  @media (min-width: 768px) {
    font-size: 16px;
  }
`;

export const ContainerAddress = styled(Container)`
  .paymentAddress__address:not(:last-child) {
    margin-bottom: 10px;
  }

  /* @media (min-width: 768px) {
    .paymentAddress__address {
      padding: 0 0 0 20px;
    }
  } */
`;

export const ContainerInput = styled(Container)`
  display: block;

  height: auto;

  &:not(:last-child) {
    margin-bottom: 10px;
  }
`;

export const Box = styled.div`
  display: flex;

  width: 100%;
  padding: 15px 0 15px 20px;

  border-radius: 10px;
  border-style: solid;
  border-width: 1px;

  border-color: #eaeaea;
  background: #fbfbfb;
`;

export const ButtonGroup = styled(Button.Group)`
  margin-top: 10px;
  margin-bottom: 35px;
`;

export const Form = styled.form`
  width: 100%;

  &.dataPersonalForm {
    > div {
      display: grid;
      grid-template-columns: 1fr 1fr;
      column-gap: 30px;
    }
  }
`;

export const ContainerInputAddress = styled(Box)`
  align-items: center;

  position: relative;
  overflow: hidden;

  .container__checkbox {
    width: 8%;
    margin-right: 20px;

    @media (min-width: 768px) {
      width: 2%;
    }
  }
  .container__address {
    p {
      margin-bottom: 5px;
    }
  }

  @media (min-width: 768px) {
    .container__address,
    .container__checkbox {
      height: 70%;
    }
  }
`;

export const ContainerExclude = styled.div`
  font-size: 18px;
  font-weight: bold;

  display: none;
  visibility: hidden;

  cursor: pointer;
  position: absolute;
  right: 0;
  top: 0;
  width: 35px;
  height: 100%;

  background: #bdbdbd;
  color: #5e5e5e;

  @media (min-width: 768px) {
    display: flex;
    justify-content: center;
    align-items: center;
    visibility: visible;
  }
`;

export const ContainerPaymentForm = styled(Container)`
  .paymentForms__title,
  .paymentForms__options {
    margin-bottom: 10px;
  }

  .paymentForms__methods:not(:last-child) {
    margin-bottom: 10px;
  }
`;

export const AddPaymentForm = styled(Text)`
  margin-bottom: 10px;
  padding-left: 20px;
  position: relative;
  cursor: pointer;

  color: #01a3ff;

  &::after,
  &::before {
    clear: both;
    display: block;
    visibility: visible;

    position: absolute;
    left: 0;
    top: 2px;

    width: 12px;
    height: 12px;
  }

  &::before {
    content: '';
    border-radius: 3px;

    background: #01a3ff;
  }
  &::after {
    font-size: 12px;
    content: '+';

    left: 2.5px;
    top: -0.6px;

    color: #fff;
  }
`;

export const ContainerCreditCard = styled(Container)`
  padding: 14px 16px;
  border-radius: 15px;

  border-style: solid;
  border-width: 1px;

  background: #fbfbfb;
  border-color: #eaeaea;

  .creditCard__card {
    margin-top: 10px;
  }

  > div:not(:last-child) {
    margin-bottom: 10px;
  }
`;

export const ContainerValidateCard = styled(Container)`
  justify-content: space-between;

  .creditCard__validate {
    &__month {
      width: calc(50% - 10px);
      margin-right: 10px;
    }

    &__age {
      width: 50%;
    }

    &__cvv {
      width: 100%;
      margin-top: 10px;
    }
  }
`;

export const ContainerListProducts = styled(Box)``;
