import React from 'react';
import { useFormik } from 'formik';
// import PropTypes from 'prop-types';

import { Input, Label } from '@components/atoms';

import { cpfMask, emailMask, phoneMask } from '@utils/masksForms';
import { DataPersonalValidation } from './validations';

import * as S from './styled';

const DataPersonal = () => {
  const { handleBlur, handleChange, handleSubmit, values, errors } = useFormik({
    initialValues: {
      name: '',
      email: '',
      cpf: '',
      phone: '',
    },
    onSubmit(inputValues) {
      console.log(inputValues);
    },
    validationSchema: DataPersonalValidation,
  });

  return (
    <>
      <S.Form className="dataPersonalForm" onSubmit={handleSubmit}>
        <div>
          <div>
            <S.ContainerInput className="dataPersonal">
              <Label htmlFor="dataPersonal__name">Nome do destinátário:</Label>
              <Input
                id="dataPersonal__name"
                type="text"
                name="name"
                variant="login"
                placeholder="Nome do destinatário"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMessage={errors.name}
              />
            </S.ContainerInput>

            <S.ContainerInput className="dataPersonal">
              <Label htmlFor="dataPersonal__email">E-mail:</Label>
              <Input
                id="dataPersonal__email"
                type="text"
                variant="login"
                name="email"
                placeholder="Email do destinatário"
                value={emailMask(values.email)}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMessage={errors.email}
              />
            </S.ContainerInput>
          </div>

          <div>
            <S.ContainerInput>
              <Label htmlFor="dataPersonal__cpf">CPF:</Label>
              <Input
                id="dataPersonal__cpf"
                type="text"
                variant="login"
                name="cpf"
                placeholder="CPF"
                value={cpfMask(values.cpf)}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMessage={errors.cpf}
              />
            </S.ContainerInput>

            <S.ContainerInput>
              <Label htmlFor="dataPersonal__phone">Telefone de contato:</Label>
              <Input
                id="dataPersonal__phone"
                type="text"
                variant="login"
                name="phone"
                placeholder="Telefone do Destinatário"
                value={phoneMask(values.phone)}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMessage={errors.phone}
              />
            </S.ContainerInput>
          </div>
        </div>
      </S.Form>
    </>
  );
};

DataPersonal.propTypes = {};

DataPersonal.defaultProps = {};

export default DataPersonal;
