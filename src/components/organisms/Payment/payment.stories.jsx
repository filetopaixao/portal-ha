import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';
import DtPersonal from './index';
import DataAddress from './address';
import DataCreditCard from './creditCard';

export default {
  title: 'organisms/Payment',
  component: DtPersonal,
  decorators: [withKnobs],
};

const dataAddress = [
  {
    state: 'Palmas-TO',
    cep: ' (770001-000)',
    address: 'Alameda 08, Qi 16, Lote 24',
    neighborhood: '105 Sul, Número 24',
    complement: 'Próximo a prefeitura',
  },
  {
    state: 'Palmas-TO',
    cep: ' (770001-000)',
    address: 'Alameda 08, Qi 16, Lote 24',
    neighborhood: '105 Sul, Número 24',
    complement: 'Próximo a prefeitura',
  },
];

export const DataPersonal = () => (
  <ContainerStorybook>
    <DtPersonal />
  </ContainerStorybook>
);

export const Address = () => (
  <ContainerStorybook>
    <DataAddress datAddress={dataAddress} />
  </ContainerStorybook>
);

export const CreditCard = () => (
  <ContainerStorybook>
    <DataCreditCard />
  </ContainerStorybook>
);
