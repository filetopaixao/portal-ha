export default {
  breakpoints: {
    xs: 0, // Extra small screen / phone
    sm: 576, // Small screen / phone
    md: 768, // Medium screen / tablet
    lg: 992, // Large screen / desktop
    xl: 1200, // Extra large screen / wide desktop
  },

  container: {
    sm: 540,
    md: 720,
    lg: 960,
    xl: 1140,
  },
};
