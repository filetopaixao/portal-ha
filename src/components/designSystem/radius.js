export default {
  xs: 2,
  s: 3,
  base: 4,
  m: 8,
  l: 100,
};
