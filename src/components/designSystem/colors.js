export default {
  primary: {
    base: '#29bdba',
    light: '#43C8AD',
    dark: '#228EA8',
  },

  secondary: {
    base: '#ced4da',
    light: '#D4DCDF',
    dark: '#B4B7BF',
  },

  danger: {
    base: '#ef5350',
    light: '#F3657D',
    dark: '#D46544',
  },

  info: {
    base: '#42a5f5',
    light: '#59CFF9',
    dark: '#376ED9',
  },

  success: {
    base: '#66bb6a',
    light: '#84C579',
    dark: '#58A56C',
  },

  warning: {
    base: '#ffee58',
    light: '#FFD66C',
    dark: '#D1DF4A',
  },

  text: {
    base: '#212121',
    light: '#454F5B',
    link: '#0084EC',
  },

  light: {
    base: '#D4DCDF',
    light: '#f4f4f4',
    dark: '#B4B7BF',
  },

  dark: {
    base: '#212121',
    light: '#454F5B',
  },

  white: {
    base: '#ffffff',
  },

  black: {
    base: '#000000',
  },

  border: {
    base: '#e0e0e0',
    blue: '#0c86ea',
  },
};
