export default {
  baseFontSize: '16px',

  sizes: {
    xs: 12,
    s: 14,
    base: 16, // [default] p, h5, h6
    m: 20, // h4
    l: 24, // h3
    xl: 30, // h2
    xxl: 40, // h1
  },

  fontWeight: {
    thin: 100,
    light: 300,
    regular: 400,
    bold: 700,
    solid: 900,
  },

  lineHeight: {
    headings: 1.1,
  },
};
