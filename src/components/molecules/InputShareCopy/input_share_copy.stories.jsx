import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';
import InputShareCopy from './index';

export default {
  title: 'molecules/InputShareCopy',
  component: InputShareCopy,
  decorators: [withKnobs],
};

export const element = () => (
  <ContainerStorybook>
    <InputShareCopy />
  </ContainerStorybook>
);

element.story = {
  name: 'Default',
};
