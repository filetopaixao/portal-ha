import styled from 'styled-components';
import { shade } from 'polished';

import Button from '@components/atoms/Button';
import Input from '@components/atoms/Input';
import Toast from '@components/atoms/Toast';

import IconCopy from '@assets/images/copy.svg';
import IconShare from '@assets/images/share.svg';

export const ButtonS = styled(Button)``;
export const InputS = styled(Input)``;
export const ToastS = styled(Toast)``;

export const Container = styled.div`
  padding: 0;
  display: flex;

  button {
    transition: background-color 0.2s;
    z-index: 2;

    @media (max-width: 1050px) {
      display: block;
      margin-top: 10px;
      font-size: 12px;
    }
  }

  button:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }

  button:hover {
    background: ${shade(0.2, '#1045d4')};
  }

  button:first-child {
    margin-bottom: 10px;

    @media (min-width: 414px) {
      margin-bottom: 0;
      margin-right: 10px;
    }

    @media (min-width: 1024px) {
      margin-left: 10px;
    }
  }

  @media (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const ContainerToShare = styled.div`
  display: block;
`;

export const ContainerToast = styled.div``;

export const ContentToShare = styled.ul`
  position: relative;

  margin-top: 0px;
  float: right;
  right: 0;
  padding: 10px;

  list-style: none;
  border-radius: 10px;

  background: #fff;
  box-shadow: 0px 3px 10px #00000033;

  li {
    display: flex;

    img {
      width: 20px;
      height: 20px;

      margin-right: 15px;
    }

    p {
      font-size: 13px;

      color: #5e5e5e;
    }

    &:nth-child(-n + 2) {
      margin-bottom: 12px;
    }

    a {
      text-decoration: none;

      display: flex;
      justify-content: flex-end;

      cursor: pointer;
    }
  }
`;

export const ContentInput = styled.div`
  display: flex;
  justify-items: center;

  width: 100%;
  height: 40px;
  padding: 5px 10px;

  border-radius: 10px;
  border: 1px solid #cdcdcd;

  background: #f1eeee 0% 0% no-repeat padding-box;

  transition: border-color 0.3s;

  > div {
    width: 100%;
  }

  &:hover {
    border-color: ${shade(0.1, '#cdcdcd')};
  }

  @media (max-width: 991px) {
    width: 100%;
  }
`;

export const SCInput = styled(Input)`
  text-decoration: none;

  width: 100%;
  height: 100%;

  padding: 0;
  padding-left: 5px;

  border: none;
  cursor: pointer;
  opacity: 0.3;

  background: #f2f2f2;
  color: #5e5e5e;

  &:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }
`;

export const CopyButton = styled.button`
  width: 40px;
  height: 40px;

  border: none;
  outline: none;

  background: url(${IconCopy}) no-repeat;
  cursor: pointer;
`;

export const ShareButton = styled.button`
  width: 40px;
  height: 40px;

  border: none;
  outline: none;

  background: url(${IconShare}) no-repeat;
  cursor: pointer;
`;
