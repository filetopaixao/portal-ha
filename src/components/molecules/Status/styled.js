import styled from 'styled-components';

export const ContainerStatus = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
`;

export const Cards = styled(ContainerStatus)`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  column-gap: 10px;
  width: 100%;

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    column-gap: 5px;
    row-gap: 10px;

    > div {
      margin: 0 auto;
    }
  }
`;
