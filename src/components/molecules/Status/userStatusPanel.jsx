import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Card from '../../atoms/Cards';

import * as S from './styled';

const userStatusPanel = ({ data }) => {
  const Theme = () => {
    const themeStore = useSelector((state) => state.theme);
    return themeStore;
  };

  return (
    <S.ContainerStatus>
      <S.Cards>
        <Card
          icon="saldo"
          title="PONTOS"
          description={data.coins}
          className="card_status"
          color={Theme().primaryColor}
        />
        <Card
          icon="liberacao"
          title={Theme().tipoApresentacao === 'revenda' ? 'AGUARDANDO LIBERAÇÃO' : 'DOAÇÃO MENSAL'}
          description={data.commissionNotReleased}
          className="card_status"
          color={Theme().secondaryColor}
        />
        <Card
          icon="liberado"
          title={Theme().tipoApresentacao === 'revenda' ? 'SALDO LIBERADO' : 'DOAÇÃO ÚNICA'}
          description={data.commissionReleased}
          className="card_status"
          color={Theme().primaryColor}
        />
        <Card
          icon="faturado"
          title={Theme().tipoApresentacao === 'revenda' ? 'TOTAL FATURADO' : 'TOTAL DE DOAÇÕES'}
          description={data.totalEarnings}
          className="card_status"
          color={Theme().secondaryColor}
        />
      </S.Cards>
    </S.ContainerStatus>
  );
};

userStatusPanel.propTypes = {
  data: PropTypes.objectOf(PropTypes.string),
};
userStatusPanel.defaultProps = {
  data: {},
};

export default userStatusPanel;
