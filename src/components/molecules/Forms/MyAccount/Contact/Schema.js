import * as Yup from 'yup';

const Schema = Yup.object().shape({
  email: Yup.string().email('email inválido').required('campo obrigatório'),
  phone: Yup.string()
    .required('campo obrigatório')
    .test({
      name: 'validatePhone',
      test(value) {
        const rePhoneNumber = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/.test(
          value
        );

        if (!rePhoneNumber) {
          return this.createError({
            message: 'digite o numero corretamente',
            path: 'phone',
          });
        }
        return true;
      },
    }),
  whatsapp: Yup.string().test({
    name: 'validatePhone',
    test(value) {
      const rePhoneNumber = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/.test(
        value
      );
      console.log(value);
      if (!rePhoneNumber && value !== undefined) {
        return this.createError({
          message: 'Número de telefone inválido',
          path: 'whatsapp',
        });
      }
      return true;
    },
  }),
});

export default Schema;
