import * as Yup from 'yup';

const Schema = Yup.object().shape({
  currentPassword: Yup.string().required('A senha atual é necessária.'),
  newPassword: Yup.string().required('Nova senha é necessária.'),
  passwordConfirmation: Yup.string().oneOf([Yup.ref('newPassword'), null], 'As senhas devem corresponder'),
});

export default Schema;
