import React, { useState } from 'react';
import { useFormik } from 'formik';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Modal from '@components/molecules/Modals';

import { UpdatePassword } from '../../../../../services/auth';

import Schema from './Schema';

import * as S from '../styled';

const ChangePasswordForm = ({ closeModal }) => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const formik = useFormik({
    initialValues: {
      currentPassword: '',
      newPassword: '',
      passwordConfirmation: '',
    },
    onSubmit: async (values) => {
      const tokenLocal = localStorage.getItem('token');
      const response = await UpdatePassword({ token: tokenLocal, password: values.newPassword });
      console.log(values, tokenLocal, response);
      setIsModalVisible(!isModalVisible);
    },
    validationSchema: Schema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  return (
    <S.FormAccount onSubmit={formik.handleSubmit} type="changePass">
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputPassword">
          Senha atual:
        </S.LabelAccount>
        <S.InputAccount
          name="currentPassword"
          variant="login"
          type="text"
          id="inputPassword"
          onChange={formik.handleChange}
          value={formik.values.currentPassword}
          placeholder="Digite a senha atual"
          theme={themeStore.themeName}
        />
        {formik.errors.currentPassword ? <S.ErrorMessage>{formik.errors.currentPassword}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputNewPass">
          Nova senha:
        </S.LabelAccount>
        <S.InputAccount
          name="newPassword"
          variant="login"
          type="text"
          id="inputNewPass"
          onChange={formik.handleChange}
          value={formik.values.newPassword}
          placeholder="Digite a nova senha"
          theme={themeStore.themeName}
        />
        {formik.errors.newPassword ? <S.ErrorMessage>{formik.errors.newPassword}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputNewRepass">
          Repetir nova senha:
        </S.LabelAccount>
        <S.InputAccount
          name="passwordConfirmation"
          variant="login"
          type="text"
          id="inputNewRepass"
          onChange={formik.handleChange}
          value={formik.values.passwordConfirmation}
          placeholder="Repita a nova senha"
          theme={themeStore.themeName}
        />
        {formik.errors.passwordConfirmation ? (
          <S.ErrorMessage>{formik.errors.passwordConfirmation}</S.ErrorMessage>
        ) : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.ButtonAccount primaryColor={themeStore.primaryColor} variant="primary" type="submit">
          alterar senha
        </S.ButtonAccount>
      </S.FormGroup>
      <Modal variant="md" title={null} isVisible={isModalVisible} onClose={() => setIsModalVisible(!isModalVisible)}>
        <S.Modalcontainer>
          <S.ConfirmCircle />
        </S.Modalcontainer>
        <S.Modalcontainer>
          <S.H3>Senha alterada com sucesso!</S.H3>
        </S.Modalcontainer>
        <S.CtConfirmBtn>
          <S.ButtonAccount
            variant="secondary"
            onClick={() => {
              setIsModalVisible(!isModalVisible);
              closeModal();
            }}
          >
            ok
          </S.ButtonAccount>
        </S.CtConfirmBtn>
      </Modal>
    </S.FormAccount>
  );
};

ChangePasswordForm.propTypes = {
  closeModal: PropTypes.func,
};

ChangePasswordForm.defaultProps = {
  closeModal: () => false,
};

export default ChangePasswordForm;
