import styled from 'styled-components';
// import { shade } from 'polished';

import { Container } from '@components/atoms/Container/styled';

import Input from '@components/atoms/Input/styled';
import Label from '@components/atoms/Label/styled';
import IconAlert from '@assets/images/alert.svg';
import IconClose from '@assets/images/close.svg';
import IconSuccess from '@assets/images/icon_success.svg';

export const FormGroup = styled.div`
  margin: 15px auto;

  text-align: center;
`;

export const ButtonAccount = styled.button`
  font-size: 12px;
  text-transform: uppercase;
  font-weight: bold;

  height: 53px;
  width: 200px;

  margin: 0 auto;

  border: none;
  border-radius: 15px;

  color: #fff;
  background: ${(props) => props.primaryColor};
  box-shadow: 0px 3px 6px #00000029;

  @media (min-width: 992px) {
    width: ${(props) => props.size};
  }
`;

export const FormGroupRow = styled.div`
  margin-bottom: 15px;
  width: 100%;
`;

export const InputLeft = styled.div`
  width: 100%;

  @media (min-width: 992px) {
    width: 50%;
    padding-right: 5px;
  }

  select {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const InputRight = styled.div`
  width: 100%;

  @media (min-width: 992px) {
    width: 50%;
    padding-left: 5px;
  }

  select {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const RadioGroup = styled.div`
  display: flex;

  padding: 20px;

  border-radius: 15px;

  background: #f2f2f2;

  div {
    display: flex;

    width: 50%;
  }
`;

export const FormAccount = styled.form`
  flex: 100%;

  margin-top: 15px;

  input[type='radio'] {
    display: flex;

    width: 20px;

    margin-right: 10px;
  }
  .form-group {
    margin-bottom: 10px;
  }

  @media (min-width: 992px) {
    flex: 55%;
    ${(props) => (props.type === 'changePass' ? 'padding: 0 30px;' : '')}
  }
`;

export const Alert = styled.div`
  font-size: 10px;

  display: flex;

  margin: 10px 0 10px 0;

  color: ${(props) => props.secondaryColor};
`;

export const AlertIcon = styled.div`
  padding: 0px 20px;

  -webkit-mask-image: url(${IconAlert});
  mask-image: url(${IconAlert});
  mask-repeat: no-repeat;
  mask-size: 23px;
  background-color: ${(props) => props.secondaryColor};
  width: 23px;
  height: 23px;
`;

export const InputAccount = styled(Input)`
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  ::-webkit-calendar-picker-indicator {
    filter: invert(0.5);
  }
`;

export const LabelAccount = styled(Label)`
  font-size: 15px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  margin-bottom: 5px;
`;

export const CardAddress = styled.div`
  display: flex;

  width: 100%;
  height: auto;

  margin-bottom: 10px;

  border-radius: 15px;
  border: 1px solid #bdbdbd;

  background: #f2f2f2;
  .div-content-card {
    font-size: 13px;

    width: 100%;

    padding: 10px 20px;

    color: #5e5e5e;
  }
`;

export const RemoveAddress = styled.div`
  width: 20px;
  height: 100%;

  border-radius: 0px 13px 13px 0px;

  background: url(${IconClose}) center center no-repeat #bdbdbd;

  position: relative;
  float: right;
  cursor: pointer;
`;

export const ErrorMessage = styled.div`
  font-size: 12px;

  color: red;
`;

export const Modalcontainer = styled.div`
  display: flex;
  justify-content: center;

  margin: 20px 0;
`;

export const ConfirmCircle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 200px;
  height: 200px;

  margin: auto;

  border-radius: 50%;

  background: #2fdf46;
  background: url(${IconSuccess}) center center no-repeat;

  clear: both;
`;

export const H3 = styled.h3`
  text-align: center;
`;

export const CtConfirmBtn = styled(Container)`
  @media (min-width: 992px) {
    padding: 0 120px;
  }
`;

export const Row = styled.div`
  margin-bottom: 15px;

  @media (min-width: 992px) {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
`;
