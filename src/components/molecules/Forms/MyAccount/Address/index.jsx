import React from 'react';
import { useSelector } from 'react-redux';
import { useFormik } from 'formik';
import PropTypes from 'prop-types';

import { addUserAddress } from '@services/user';

import Schema from './Schema';

import * as S from '../styled';

const Address = ({ funcSubmit, addressesArray, setAddresses }) => {
  const formik = useFormik({
    initialValues: {
      cep: '',
      city: '',
      complement: '',
      country: '',
      neighborhood: '',
      number: '',
      state: '',
      street: '',
    },
    onSubmit: () => {},
    validationSchema: Schema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  const themeStore = useSelector((state) => state.theme);

  const handleAddAddress = async (e) => {
    e.preventDefault();
    console.log();
    const response = await addUserAddress({
      cep: formik.values.cep,
      street: formik.values.street,
      complement: formik.values.complement,
      neighborhood: formik.values.neighborhood,
      number: formik.values.number,
      city: formik.values.city,
      state: formik.values.state,
      country: formik.values.country,
    });
    console.log(response);
    funcSubmit();
    console.log('funcionou o adicionar');
    setAddresses([...addressesArray, formik.values]);
  };

  return (
    <S.FormAccount onSubmit={(e) => handleAddAddress(e)}>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputCep">
          CEP:
        </S.LabelAccount>
        <S.InputAccount
          variant="formData"
          name="cep"
          type="text"
          id="inputCep"
          onChange={formik.handleChange}
          value={formik.values.cep}
          theme={themeStore.themeName}
        />
        {formik.errors.cep ? <S.ErrorMessage>{formik.errors.cep}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputStreet">
          Rua:
        </S.LabelAccount>
        <S.InputAccount
          name="street"
          variant="formData"
          type="text"
          id="inputStreet"
          onChange={formik.handleChange}
          value={formik.values.street}
          theme={themeStore.themeName}
        />
        {formik.errors.street ? <S.ErrorMessage>{formik.errors.street}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputNeighborhood">
          Bairro:
        </S.LabelAccount>
        <S.InputAccount
          name="neighborhood"
          variant="formData"
          type="text"
          id="inputneighborhood"
          onChange={formik.handleChange}
          value={formik.values.neighborhood}
          theme={themeStore.themeName}
        />
        {formik.errors.neighborhood ? <S.ErrorMessage>{formik.errors.neighborhood}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputNumber">
          Número:
        </S.LabelAccount>
        <S.InputAccount
          name="number"
          variant="formData"
          type="text"
          id="inputNumber"
          onChange={formik.handleChange}
          value={formik.values.number}
          theme={themeStore.themeName}
        />
        {formik.errors.number ? <S.ErrorMessage>{formik.errors.number}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputComplement">
          Aos cuidados / Complemento:
        </S.LabelAccount>
        <S.InputAccount
          name="complement"
          variant="formData"
          type="text"
          id="inputComplement"
          onChange={formik.handleChange}
          value={formik.values.complement}
          theme={themeStore.themeName}
        />
        {formik.errors.complement ? <S.ErrorMessage>{formik.errors.complement}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputCity">
          Cidade:
        </S.LabelAccount>
        <S.InputAccount
          name="city"
          variant="formData"
          type="text"
          id="inputCity"
          onChange={formik.handleChange}
          value={formik.values.city}
          theme={themeStore.themeName}
        />
        {formik.errors.city ? <S.ErrorMessage>{formik.errors.city}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputState">
          Estado:
        </S.LabelAccount>
        <S.InputAccount
          name="state"
          variant="formData"
          type="text"
          id="inputState"
          onChange={formik.handleChange}
          value={formik.values.state}
          theme={themeStore.themeName}
        />
        {formik.errors.state ? <S.ErrorMessage>{formik.errors.state}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputCountry">
          País:
        </S.LabelAccount>
        <S.InputAccount
          name="country"
          variant="formData"
          type="text"
          id="inputCountry"
          onChange={formik.handleChange}
          value={formik.values.country}
          theme={themeStore.themeName}
        />
        {formik.errors.country ? <S.ErrorMessage>{formik.errors.country}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.ButtonAccount primaryColor={themeStore.primaryColor} variant="primary" type="submit">
          Salvar Alterações
        </S.ButtonAccount>
      </S.FormGroup>
    </S.FormAccount>
  );
};

Address.propTypes = {
  funcSubmit: PropTypes.func,
  setAddresses: PropTypes.func,
  addressesArray: PropTypes.arrayOf(PropTypes.shape),
};
Address.defaultProps = {
  funcSubmit: () => {},
  setAddresses: () => {},
  addressesArray: '',
};

export default Address;
