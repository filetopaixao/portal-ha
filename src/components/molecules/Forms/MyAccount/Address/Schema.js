import * as Yup from 'yup';

const Schema = Yup.object().shape({
  cep: Yup.string().max(18).required('campo obrigatório'),
  street: Yup.string().required('campo obrigatório'),
  complement: Yup.string().required('campo obrigatório'),
  country: Yup.string().required('campo obrigatório'),
  neighborhood: Yup.string().required('campo obrigatório'),
  district: Yup.string().required('campo obrigatório'),
  number: Yup.string().required('campo obrigatório'),
  city: Yup.string().required('campo obrigatório'),
  state: Yup.string().required('campo obrigatório'),
});

export default Schema;
