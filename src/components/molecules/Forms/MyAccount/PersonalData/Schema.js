import * as Yup from 'yup';

import { validCpf } from '@utils/validators';

const Schema = Yup.object().shape({
  first_name: Yup.string()
    .required('campo obrigatório')
    .test({
      name: 'validateName',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.test(value);

        if (!onlyText) {
          return this.createError({
            message: 'não pode conter números',
            path: 'first_name',
          });
        }
        return true;
      },
    }),
  last_name: Yup.string()
    .required('campo obrigatório')
    .test({
      name: 'validateLast_name',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.test(value);

        if (!onlyText) {
          return this.createError({
            message: 'não pode conter números',
            path: 'last_name',
          });
        }
        return true;
      },
    }),
  gender: Yup.string().required('campo obrigatório'),
  birthday: Yup.string().max(10).required('campo obrigatório'),
  cpf: Yup.string()
    .max(14)
    .required('campo obrigatório')
    .test('testCpf', 'campo obrigatório', (value) => validCpf(value)),
});

export default Schema;
