import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';

import { forgotPassword } from '../../../services/auth';

import * as S from './styled';

import SchemaForgotPass from './SchemaForgotPass';

const ForgotPassForm = ({ redirectTo }) => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    onSubmit: async (values) => {
      const response = await forgotPassword({ email: values.email, redirect_url: 'https://loja.everbynature.com.br/' });

      console.log(response);
      console.log({ email: values.email, redirect_url: 'https://loja.everbynature.com.br/' });

      history.push(redirectTo);
    },
    validationSchema: SchemaForgotPass,
  });

  return (
    <S.Form onSubmit={formik.handleSubmit}>
      <S.InputComponent
        name="email"
        type="email"
        variant="login"
        placeholder="e-mail"
        onChange={formik.handleChange}
        value={formik.values.email}
        theme={themeStore.themeName}
        primaryColor={themeStore.primaryColor}
      />
      {formik.errors.email ? <div className="ErrorMessage">{formik.errors.email}</div> : null}

      <S.Submit
        icon="arrowRight"
        variant="primary"
        showIcon
        type="submit"
        primaryColor={themeStore.primaryColor}
        secondaryColor={themeStore.secondaryColor}
        tertiaryColor={themeStore.tertiaryColor}
      >
        Continuar
      </S.Submit>
    </S.Form>
  );
};

ForgotPassForm.propTypes = {
  redirectTo: PropTypes.string,
};

ForgotPassForm.defaultProps = {
  redirectTo: '',
};

export default ForgotPassForm;
