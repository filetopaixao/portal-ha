import styled from 'styled-components';
import Input from '@components/atoms/Input/styled';
import Button from '@components/atoms/Button/styled';

export const InputComponent = styled(Input)`
  margin: 15px 0px;

  background: ${(props) => (props.theme === 'Dark' ? '#111A21' : '#F5F6FA')};

  border: ${(props) => `2px solid ${props.primaryColor}`};

  color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};

  @media (min-width: 768px) {
    font-size: 16px;
  }
`;

export const Form = styled.form`
  button {
    width: 100%;
  }

  a {
    text-decoration: none;

    color: ${(props) => props.secondaryColor};
  }

  .forgot {
    text-align: right;
  }

  .remember,
  .forgot {
    font-size: 15px;

    display: inline-block;

    width: 50%;
    margin-bottom: 50px;

    color: ${(props) => props.secondaryColor};

    @media (min-width: 768px) {
      font-size: 14px;
    }
  }

  .remember div {
    display: inline-flex;

    margin-right: 10px;
  }

  .remember span,
  .forgot a {
    font-size: 12px;

    top: -3px;
    position: relative;

    @media (min-width: 768px) {
      font-size: 14px;
    }
  }

  .ErrorMessage {
    font-size: 14px;
    color: red;
  }
`;

export const Submit = styled(Button)`
  width: 100%;

  &::after {
    ${(props) => (props.icon === 'arrowRight' ? 'right: 15px; top: 10px; width: 12px;' : '')}
  }
`;
