import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import ImageConfSuccess from '@assets/images/icon_success.svg';
import Modal from '../ModalComponent';

import * as S from './styled';

const ModalConfSuccess = ({ headerTitle, functionExecute, msg1, msg2, isModalConfSuccess, setIsModalConfSuccess }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <Modal
      variant="md"
      alignCenter
      title=""
      isVisible={isModalConfSuccess}
      onClose={() => setIsModalConfSuccess(!isModalConfSuccess)}
      MaxHeight="460px"
      ContentHeight="auto"
    >
      <S.ModalContent>
        {headerTitle ? (
          <S.ModalHeader theme={themeStore.themeName}>
            <h3>{headerTitle}</h3>
          </S.ModalHeader>
        ) : null}
        <S.ImageContainer ImageConfSuccess={ImageConfSuccess} primaryColor={themeStore.primaryColor} />

        <S.ContainerInfo theme={themeStore.themeName}>
          {msg1 ? <h3>{msg1}</h3> : null}
          {msg2 ? <p>{msg2}</p> : null}
        </S.ContainerInfo>

        <S.ModalBody>
          <S.ButtonGroup>
            <S.Button
              onClick={() => functionExecute()}
              type="button"
              className="btn-save"
              primaryColor={themeStore.primaryColor}
            >
              OK
            </S.Button>
          </S.ButtonGroup>
        </S.ModalBody>
      </S.ModalContent>
    </Modal>
  );
};

ModalConfSuccess.propTypes = {
  isModalConfSuccess: PropTypes.bool,
  setIsModalConfSuccess: PropTypes.func,
  headerTitle: PropTypes.string,
  msg1: PropTypes.string,
  msg2: PropTypes.string,
  functionExecute: PropTypes.func,
};
ModalConfSuccess.defaultProps = {
  isModalConfSuccess: false,
  setIsModalConfSuccess: () => {},
  headerTitle: '',
  msg1: '',
  msg2: '',
  functionExecute: () => {},
};

export default ModalConfSuccess;
