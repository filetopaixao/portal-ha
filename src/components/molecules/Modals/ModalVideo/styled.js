import styled from 'styled-components';

export const Container = styled.div`
  button#btnModelCloser {
    margin-top: -15px;
    margin-right: -20px;

    @media (min-width: 600px) {
      margin-top: -20px;
    }
  }
`;

export const ModalContentVideo = styled.div`
  width: 100%;
  height: 100%;
  max-height: 424px;

  iframe {
    min-height: 424px;
    width: 100%;

    border: none;
    border-radius: 10px;

    @media (max-width: 700px) {
      min-height: 300px;
      height: 100%;
    }
  }
`;
