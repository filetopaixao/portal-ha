import React from 'react';
import PropTypes from 'prop-types';

import Modal from '../ModalComponent';

import * as S from './styled';

const ModalComponentVideo = ({ isModalVideoVisible, setIsModalVideoVisible, linkVideo }) => {
  return (
    <S.Container>
      <Modal
        title=""
        isVisible={isModalVideoVisible}
        onClose={() => setIsModalVideoVisible(!isModalVideoVisible)}
        variant="mdMin"
        alignCenter
        MaxHeight="95vh"
        MaxWidth="600px"
        ContentHeight="auto"
        classType="video"
      >
        <S.ModalContentVideo>
          <iframe title="tutorial" className="embed-responsive-item" src={linkVideo} allowFullScreen />
        </S.ModalContentVideo>
      </Modal>
    </S.Container>
  );
};

ModalComponentVideo.propTypes = {
  linkVideo: PropTypes.string,
  isModalVideoVisible: PropTypes.bool,
  setIsModalVideoVisible: PropTypes.func,
};
ModalComponentVideo.defaultProps = {
  linkVideo: '',
  isModalVideoVisible: false,
  setIsModalVideoVisible: () => {},
};

export default ModalComponentVideo;
