import styled, { css } from 'styled-components';

import Text from '@components/atoms/Text';
import Button from '@components/atoms/Button/styled';

import ChangePassImg from '@assets/images/change_password.svg';
import IconSuccess from '@components/atoms/Icon/success';
import Confirm from '@assets/images/confirm.svg';

const variants = {
  lg: () => css`
    width: 85%;
  `,
  md: () => css`
    width: 50%;
  `,
  sm: () => css`
    width: 30%;
  `,
};

export const Modal = styled.div`
  display: flex;
  justify-content: center;
  ${(props) => props.alignCenter && 'align-items: center;'}

  width: 100%;
  height: 100vh;

  position: fixed;
  left: 0;
  top: 0;
  overflow: auto;
  z-index: 999;

  box-shadow: 0 3px 26px rgba(0, 0, 0, 0.3);
  background: #ffffff;
  background-color: rgba(0, 0, 0, 0.6);
`;

export const ModalContent = styled.div`
  display: inline-table;

  ${(props) => variants[props.variant]}
  height: auto;

  margin: 20px 0 20px 0;
  ${(props) => (props.type === 'changePass' ? 'padding: 0;' : 'padding: 15px 30px;')}

  border-radius: 20px;
  z-index: 2;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  overflow: hidden;

  span {
    margin-left: 10px;
  }

  @media (max-width: 500px) {
    width: 90%;
    padding: 15px 15px;
  }

  @media (min-width: 992px) {
    ${(props) =>
      props.type === 'changePass' ? 'display: flex; flex-direction: row; height: 450px; padding: 0;' : null}
  }
`;

export const ModalBody = styled.div`
  ${(props) => (props.center ? 'text-align: center;' : '')}

  height: auto;
  margin-top: 10px;

  @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  }

  ${(props) => (props.type === 'changePass' ? 'width: 100%;' : '')}

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }
`;

export const ModalHeader = styled.header`
  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    width: 100%;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;

export const PanelPixel = styled(Modal)`
  .panelPixel__content {
    max-width: 330px;
    width: 100%;

    overflow: hidden;

    @media (min-width: 1024px) {
      margin-top: 10%;
    }

    @media (min-width: 768px) {
      max-width: 700px;
    }

    @media (min-width: 1024px) {
      max-width: 750px;
    }
  }

  .panelPixel__form {
    &__input {
      &:not(:last-child) {
        margin-bottom: 10px;
      }
    }
  }

  .btnGroup {
    margin: 0;
  }
`;

export const PanelPixelForm = styled.form`
  width: 100%;

  background: dodgerblue;

  label {
    font-size: 13px;

    margin-bottom: 5px;
  }
`;

export const Message = styled(Text)`
  &.success {
    color: #2fdf46;
  }

  &.error {
    color: #f14479;
  }
`;

export const SampleStyle = styled.ul`
  padding: 16px;
  list-style: none;

  li {
    display: flex;
    justify-content: space-between;
    margin-bottom: 10px;
    color: #7c7c7c;
    font-size: 12px;

    p {
      &:first-child {
        font-weight: bold;
        margin-right: 30px;
        width: 110px;
        display: block;
        text-transform: uppercase;
      }

      &:last-child {
        text-align: center;
        width: 100px;
      }
    }
  }
`;

export const Sidebar = styled.div`
  width: 100%;
  height: 200px;
  background: url(${ChangePassImg}) center center no-repeat #ccedff;
  @media (min-width: 992px) {
    width: 300px;
    height: auto;
  }
`;

export const ModalContainerVideo = styled.div`
  div#modal {
    align-items: center;

    background-color: #bfbfbfab;

    > div {
      max-height: 424px;
      max-width: 754px;

      padding: 0px 30px 25px;

      > div {
        margin-top: 0;
      }

      @media (max-width: 700px) {
        height: 50%;

        padding: 30px;
      }
    }
  }
`;

export const ButtonCloserModal = styled.div`
  display: flex;
  justify-content: flex-end;

  &.closerSidebar {
    display: none;
    @media (max-width: 600px) {
      display: flex;
      padding-right: 10px;
    }
  }

  &.closerContent {
    display: flex;
    padding-right: 10px;
    @media (max-width: 600px) {
      display: none;
    }
  }

  button {
    font-size: 14px;

    padding: 0;

    letter-spacing: 0px;
    text-transform: none;

    opacity: 1;
    box-shadow: none;

    color: #cbcaca;
    background: transparent;

    &:focus {
      outline: thin dotted;
      outline: 0px auto -webkit-focus-ring-color;
      outline-offset: 0px;
    }
  }
`;

export const ModalContainerDefault = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;

  height: auto;
  width: 80%;

  border-radius: 20px;

  position: absolute;
  left: 10%;
  top: 10%;

  box-shadow: 0 3px 26px rgba(0, 0, 0, 0.3);
  background: #fff;
`;

export const ModalExtendPeriod = styled(ModalContainerDefault)`
  padding: 43px 20px 30px 20px;

  z-index: 2;

  @media (min-width: 768px) {
    width: 50%;
    left: 30%;
  }

  button {
    font-weight: bold;
    font-size: 16px;

    padding: 2px 60px;
    margin-top: 20px;
  }
`;

export const ModalConfirm = styled(ModalContainerDefault)`
  padding-top: 43px;
  padding-bottom: 52px;

  width: 60%;

  z-index: 2;

  button {
    padding: 2px 60px;
    margin-top: 20px;
    font-weight: bold;
    font-size: 16px;
  }
`;

export const ContainerSuccessIcon = styled.div`
  display: flex;
  justify-content: center;
`;

export const ContainerIcon = styled(IconSuccess)`
  height: 150px;
  width: 150px;

  margin-bottom: 34px;

  @media (min-width: 992px) {
    height: 200px;
    width: 200px;

    margin-bottom: 20px;
  }
`;

export const ImageCuston = styled.div`
  img {
    height: 10rem;
  }
`;

export const ModalTitle = styled.h3`
  text-align: center;

  width: 100%;
`;

export const ModalDescription = styled.p`
  display: none;

  width: 100%;

  text-align: center;
  font-size: 14px;

  @media (min-width: 768px) {
    display: block;
  }
`;

export const ModalDescriptionMobile = styled.p`
  text-align: center;
  font-size: 12px;

  width: 100%;

  @media (min-width: 768px) {
    display: none;
  }
`;

export const Buttons = styled.div`
  button {
    width: 100%;
  }

  @media (min-width: 768px) {
    display: flex;

    button {
      margin: 1rem;
    }
  }
`;

export const ConfirmIconContainer = styled.div`
  width: 100%;

  display: flex;
  justify-content: center;

  margin-bottom: 10px;
`;

export const ConfirmIcon = styled.div`
  width: 150px;
  height: 150px;

  background: url(${Confirm}) center center no-repeat;
  background-size: cover;

  @media (min-width: 992px) {
    width: 200px;
    height: 200px;
  }
`;

export const ContainerBtns = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media (min-width: 992px) {
    flex-direction: row;

    padding: 0 20px;
  }
`;

export const ContainerBtn = styled.div`
  @media (min-width: 992px) {
    width: 50%;

    display: flex;
    justify-content: center;

    padding: 0 20px;
  }
`;

export const ButtonAccount = styled(Button)`
  font-size: 12px;

  width: 100%;

  margin-bottom: 10px;
  padding: 10px 0px;

  border-radius: 8px;
  @media (min-width: 992px) {
    ${(props) => (props.size === '100%' ? 'width: 100%;' : 'width: 200px; position: relative; float: right;')}
  }
`;
