import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import ImageDelConf from '@assets/images/out.svg';
import Modal from '../ModalComponent';

import * as S from './styled';

const ModalDelConf = ({ headerTitle, msg1, msg2, exeFunctionAction, isModalDelConf, setIsModalDelConf }) => {
  const themeStore = useSelector((state) => state.theme);
  const handleExecute = () => {
    exeFunctionAction();
  };

  return (
    <Modal
      variant="md"
      alignCenter
      title=""
      isVisible={isModalDelConf}
      onClose={() => setIsModalDelConf(!isModalDelConf)}
      MaxHeight="460px"
      ContentHeight="auto"
    >
      <S.ModalContent>
        {headerTitle ? (
          <S.ModalHeader theme={themeStore.themeName}>
            <h3>{headerTitle}</h3>
          </S.ModalHeader>
        ) : null}
        <S.ImageContainer ImageDelConf={ImageDelConf} primaryColor={themeStore.primaryColor} />

        <S.ContainerInfo theme={themeStore.themeName}>
          {msg1 ? <h3>{msg1}</h3> : null}

          {msg2 ? <p>{msg2}</p> : null}
        </S.ContainerInfo>

        <S.ModalBody>
          <S.ButtonGroup>
            <S.Button
              onClick={() => handleExecute()}
              type="button"
              className="btn-save"
              primaryColor={themeStore.primaryColor}
            >
              SIM
            </S.Button>
            <S.Button
              onClick={() => setIsModalDelConf(!isModalDelConf)}
              type="button"
              className="btn-save"
              primaryColor={themeStore.primaryColor}
            >
              NÃO
            </S.Button>
          </S.ButtonGroup>
        </S.ModalBody>
      </S.ModalContent>
    </Modal>
  );
};

ModalDelConf.propTypes = {
  isModalDelConf: PropTypes.bool,
  setIsModalDelConf: PropTypes.func,
  exeFunctionAction: PropTypes.func,
  headerTitle: PropTypes.string,
  msg1: PropTypes.string,
  msg2: PropTypes.string,
};
ModalDelConf.defaultProps = {
  isModalDelConf: false,
  setIsModalDelConf: () => {},
  exeFunctionAction: () => {},
  headerTitle: '',
  msg1: '',
  msg2: '',
};

export default ModalDelConf;
