import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@components/atoms/Button';

import * as S from './styled';

const ModalConfirm = ({ children, title, onClose, id, isVisible, variant, alignCenter, btnCloser }) => {
  const themeStore = useSelector((state) => state.theme);
  const handleOutSideClick = (e) => {
    if (e.target.id === id) onClose();
  };

  return (
    <>
      {isVisible ? (
        <S.Modal id={id} onClick={handleOutSideClick} alignCenter={alignCenter}>
          <S.ModalContent variant={variant} theme={themeStore.themeName}>
            {btnCloser ? (
              <S.ButtonCloserModal>
                <Button className="btnMdCloser" type="button" onClick={() => onClose()}>
                  Fechar X
                </Button>
              </S.ButtonCloserModal>
            ) : null}
            {title ? (
              <S.ModalHeader theme={themeStore.themeName}>
                <h3>{title}</h3>
              </S.ModalHeader>
            ) : null}
            <S.ModalBody>{children}</S.ModalBody>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  );
};

ModalConfirm.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.object,
    PropTypes.element,
    PropTypes.number,
    PropTypes.func,
    PropTypes.string,
  ]),
  title: PropTypes.string,
  onClose: PropTypes.func,
  id: PropTypes.string,
  isVisible: PropTypes.bool,
  variant: PropTypes.string,
  alignCenter: PropTypes.bool,
  btnCloser: PropTypes.bool,
};

ModalConfirm.defaultProps = {
  children: () => {},
  title: 'title',
  onClose: () => false,
  id: 'modal',
  isVisible: false,
  variant: 'lg',
  alignCenter: false,
  btnCloser: true,
};

export default ModalConfirm;
