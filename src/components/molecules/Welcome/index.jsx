import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Text from '@components/atoms/Text';

import * as S from './styled';

const Welcome = ({ name, msg, author }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Welcome image={themeStore.capa ? themeStore.capa : 'welcome'} theme={themeStore.themeName}>
      <div className="welcome__container">
        <Text size="lg" variant="medium" className="welcome__title">
          {`Olá, ${themeStore.tipoApresentacao === 'revenda' ? 'agente' : 'voluntário'} ${name}`}
        </Text>

        <Text size="md" variant="light" className="welcome__description">
          {msg ||
            'O servidor pode estar com problemas, por favor aguarde obrigado(a). O servidor pode estar com problemas, por favor aguarde obrigado(a).'}
        </Text>

        <Text size="md" variant="light" className="welcome__author">
          {author}
        </Text>
      </div>
    </S.Welcome>
  );
};

Welcome.propTypes = {
  name: PropTypes.string,
  msg: PropTypes.string,
  // person: PropTypes.string,
  author: PropTypes.string,
};
Welcome.defaultProps = {
  name: 'Carla',
  msg: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
  // person: 'Mr. ',
  author: 'Lorem Ipsum',
};

export default Welcome;
