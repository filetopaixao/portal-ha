import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import ModalVideo from '@components/molecules/Modals/ModalVideo';
import ButtonGroupShareCopy from '@components/molecules/ButtonGroupShareCopy';

import Video from '@assets/images/video.svg';

import * as S from './styled';

const BannerEcommerce = ({ link, title, imageMobile, copyShare }) => {
  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';
  const handleOpenModalVideo = () => {
    setIsModalVideoVisible(!isModalVideoVisible);
  };
  const themeStore = useSelector((state) => state.theme);
  return (
    <>
      <S.BannerContainer
        imageMobile={imageMobile}
        primaryColor={themeStore.primaryColor}
        secondaryColor={themeStore.secondaryColor}
        tertiaryColor={themeStore.tertiaryColor}
      >
        <S.BannerContent>
          <S.BannerTitle>{title}</S.BannerTitle>
          <S.ButtonGroup className="content_banner">
            {link ? <S.BannerLinkText type="text" id="copy" onChange={link} value={link} /> : null}
            <div>{copyShare ? <ButtonGroupShareCopy /> : null}</div>
          </S.ButtonGroup>
        </S.BannerContent>
        <S.BannerContentVideo>
          <a href="#!" onClick={() => handleOpenModalVideo()}>
            <img src={Video} alt="Assitir" />
          </a>
        </S.BannerContentVideo>
      </S.BannerContainer>
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

BannerEcommerce.propTypes = {
  link: PropTypes.string,
  title: PropTypes.string,
  imageMobile: PropTypes.bool,
  copyShare: PropTypes.bool,
};
BannerEcommerce.defaultProps = {
  link: '',
  title: '',
  imageMobile: true,
  copyShare: true,
};

export default BannerEcommerce;
