import styled from 'styled-components';

export const MenuOpsTable = styled.div`
  display: flex;
  justify-items: center;
  justify-content: space-between;

  margin-top: 60px;

  img {
    width: 24px;
    height: 24px;
  }

  @media (max-width: 991px) {
    display: block;

    width: 100%;
    margin: 0 auto;
    margin-top: 20px;
    height: 70px;
  }
`;

export const ContentButton = styled.div`
  display: flex;
  align-items: center;
  justify-items: center;
  justify-content: space-around;

  width: 100px;
  padding: 5px 0;

  button {
    text-transform: uppercase;
    font-size: 12px;

    padding: 0;

    border: none;
    box-shadow: none;
    cursor: pointer;

    background: transparent;
    color: #dbdbdb;

    transition: color 0.2s;

    &:hover {
      color: #5e5e5e;
    }

    &:focus {
      outline: thin dotted;
      outline: 0px auto -webkit-focus-ring-color;
      outline-offset: 0px;
    }
  }

  @media (max-width: 991px) {
    float: right;
  }
`;
