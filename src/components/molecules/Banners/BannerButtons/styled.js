import styled, { css } from 'styled-components';
import { shade } from 'polished';

import WelcomeIcon from '@assets/images/welcome.svg';
import WelcomeEcommerceIcon from '@assets/images/imagem_ecommerce.svg';
import Entrepreneurs from '@assets/images/entrepreneurs.svg';
import EntrepreneursMobile from '@assets/images/entrepreneursMobile.svg';
import ImageCalendar from '@assets/images/welcome_calendar.svg';
import Partners from '@assets/images/partners.svg';

const images = {
  welcome: () => css`
    background: url(${WelcomeIcon}) no-repeat;
  `,
  ecommerce: () => css`
    background: url(${WelcomeEcommerceIcon}) no-repeat;
  `,
  entrepreneurs: () => css`
    background: url(${Entrepreneurs}) no-repeat;
  `,
  entrepreneursMobile: () => css`
    background: url(${EntrepreneursMobile}) no-repeat;
  `,
  calendar: () => css`
    background: url(${ImageCalendar}) no-repeat;
  `,
  partners: () => css`
    background: url(${Partners}) no-repeat;
  `,
};

export const BannerContainer = styled.div`
  display: flex;
  align-items: center;

  padding: 30px 40px;
  margin-bottom: 10px;
  height: 150px;
  margin: 0 auto;

  border-radius: 15px;
  position: relative;

  background: linear-gradient(to bottom, #0dbafb, #2551e1);

  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;

    height: 200px;
    width: 350px;

    position: absolute;
    right: 20px;
    bottom: 0;
    top: -30px;
    opacity: 1;
    z-index: 1;

    @media (max-width: 800px) {
      right: 8px;
    }

    ${(props) => images[props.image]}
    background-size: contain;
  }

  &::after {
    @media (min-width: 1300px) {
      height: 250px;
    }

    @media (max-width: 1050px) {
      top: 10px;
    }

    @media (max-width: 720px) {
      height: 105px;
      width: 135px;
      top: 110px;
      ${(props) => props.imageMobile && images.entrepreneursMobile}
    }
  }

  @media (max-width: 1100px) {
    padding: 30px 20px;
    height: 250px;
    width: 100%;
  }
`;

export const ButtonGroup = styled.div`
  > div:last-child {
    @media (max-width: 800px) {
      margin-top: 50px;
    }

    > div {
      @media (max-width: 991px) {
        display: block;
      }
    }
  }
`;

export const BannerContent = styled.div`
  div.content_banner {
    display: block;
    @media (min-width: 991px) {
      display: flex;
      align-items: center;
    }

    @media (max-width: 1200px) {
      display: block;
    }

    @media (min-width: 1300px) {
      p {
        margin-right: 50px;
      }
    }
  }
`;

export const BannerContentVideo = styled.div`
  position: absolute;
  right: 10px;
  bottom: 10px;
  z-index: 2;
`;

export const BannerContainerSimple = styled.div`
  display: flex;
  align-items: center;

  padding: 30px 40px;
  margin-bottom: 10px;
  height: 150px;
  margin: 0 auto;

  border-radius: 15px;
  position: relative;

  background: linear-gradient(to bottom, #0dbafb, #2551e1);

  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;

    height: 200px;
    width: 350px;

    position: absolute;
    right: 20px;
    bottom: 0;
    top: -30px;
    opacity: 1;
    z-index: 1;

    ${(props) => images[props.image]}
    background-size: contain;
  }

  &::after {
    @media (min-width: 1300px) {
      height: 250px;
    }

    @media (max-width: 1050px) {
      top: 10px;
    }

    @media (max-width: 720px) {
      height: 105px;
      width: 167px;
      top: 110px;
      ${(props) => props.imageMobile && images.entrepreneursMobile}
    }
  }

  @media (max-width: 1100px) {
    padding: 30px 20px;
    height: 250px;
    width: 100%;
  }
`;

export const BannerContentSimple = styled.div`
  ${(props) => (props.simple ? ' @media (min-width: 992px) {  max-width: 800px; } }' : '')}

  div {
    display: flex;
    align-items: center;

    @media (max-width: 991px) {
      div {
        margin-top: 50px;
      }
    }

    @media (max-width: 1200px) {
      display: block;

      > div {
        margin-top: 50px;

        @media (max-width: 991px) {
          display: block;
        }
      }
    }

    @media (min-width: 1300px) {
      p {
        margin-right: 50px;
      }
    }
  }
`;

export const BannerContentVideoSimple = styled.div`
  position: absolute;
  right: 10px;
  bottom: 10px;
  z-index: 2;
`;

export const BannerTitle = styled.h4`
  font-size: 18px;
  font-weight: bold;

  display: block;
  margin-bottom: 10px;

  color: #fff;
`;

export const BannerLinkText = styled.input`
  display: block;
  flex: 1;

  width: 260px;

  border: none;

  background: transparent;
  color: #fff;
`;

export const BannerButtonGroup = styled.div`
  padding: 0;
  display: flex;

  button {
    transition: background-color 0.2s;
    z-index: 2;

    @media (max-width: 1050px) {
      display: block;
      margin-top: 10px;
      font-size: 12px;
    }
  }

  button:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }

  button:hover {
    background: ${shade(0.2, '#1045d4')};
  }

  button:first-child {
    margin-bottom: 10px;

    @media (min-width: 414px) {
      margin-bottom: 0;
      margin-right: 10px;
    }

    @media (min-width: 1024px) {
      margin-left: 10px;
    }
  }

  @media (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const ContainerToShare = styled.div`
  display: block;
`;

export const ContainerToast = styled.div``;

export const ContentToShare = styled.ul`
  position: absolute;

  margin-top: 15px;
  margin-left: 20px;
  padding: 10px;

  list-style: none;
  border-radius: 10px;

  background: #fff;

  li {
    display: flex;

    img {
      width: 20px;
      height: 20px;

      margin-right: 15px;
    }

    p {
      font-size: 13px;

      color: #5e5e5e;
    }

    &:nth-child(-n + 2) {
      margin-bottom: 12px;
    }
  }
`;
