import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import PropTypes from 'prop-types';

import ModalVideo from '@components/molecules/Modals/ModalVideo';

import Video from '@assets/images/video.svg';

import * as S from './styled';

const Banner = ({
  alignCenter,
  sizeImage,
  heightImage,
  widthImage,
  topPosition,
  // image,
  setMargin,
  textTop,
  textBottom,
  imageMobile,
}) => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';

  const handleOpenModalVideo = () => {
    setIsModalVideoVisible(!isModalVideoVisible);
  };

  return (
    <>
      <S.BannerContainer
        sizeImage={sizeImage}
        heightImage={heightImage}
        widthImage={widthImage}
        topPosition={topPosition}
        alignCenter={alignCenter}
        setMargin={setMargin}
        // image={image}
        imageMobile={imageMobile}
        primaryColor={themeStore.primaryColor}
        secondaryColor={themeStore.secondaryColor}
        tertiaryColor={themeStore.tertiaryColor}
      >
        <S.BannerContent>
          {textTop && <S.BannerTextTop>{textTop}</S.BannerTextTop>}
          {textBottom && <S.BannerTextBottom>{textBottom}</S.BannerTextBottom>}
        </S.BannerContent>
        <S.BannerContentVideo>
          <a href="#!" onClick={() => handleOpenModalVideo()}>
            <img src={Video} alt="Assitir" />
          </a>
        </S.BannerContentVideo>
      </S.BannerContainer>
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

Banner.propTypes = {
  // image: PropTypes.string,
  textTop: PropTypes.string,
  textBottom: PropTypes.string,
  setMargin: PropTypes.string,
  imageMobile: PropTypes.bool,
  alignCenter: PropTypes.bool,
  sizeImage: PropTypes.bool,
  heightImage: PropTypes.string,
  widthImage: PropTypes.string,
  topPosition: PropTypes.string,
};
Banner.defaultProps = {
  // image: '',
  textTop: '',
  textBottom: '',
  setMargin: '',
  imageMobile: true,
  alignCenter: false,
  sizeImage: false,
  heightImage: '',
  widthImage: '',
  topPosition: '',
};

export default Banner;
