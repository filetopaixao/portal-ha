import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Filter = ({ filterText, onFilter, placeholder, width }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ContentInput id="filter_container" width={width} theme={themeStore.themeName}>
      <S.FilterInput
        id="search"
        name="search"
        type="text"
        placeholder={placeholder}
        value={filterText}
        onChange={onFilter}
        theme={themeStore.themeName}
      />
    </S.ContentInput>
  );
};

Filter.propTypes = {
  width: PropTypes.string,
  placeholder: PropTypes.string,
  filterText: PropTypes.string,
  onFilter: PropTypes.func,
};

Filter.defaultProps = {
  width: '400px',
  placeholder: 'filtro de pesquisa',
  filterText: '',
  onFilter: () => {},
};

export default Filter;
