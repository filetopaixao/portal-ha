import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import * as S from './styled';

const Space = ({ data, title, rota, isSupport }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ContainerSpace>
      <S.Title>{title}</S.Title>
      <S.Cards>
        {data.map(({ icon, variant, isComing, id, name }) => (
          <Link className="link_default" key={id} to={`${rota}/${id}/${name}`}>
            <S.Card
              className="card_space_link"
              variant={variant}
              icon={icon}
              title={name}
              isComing={isComing}
              theme={themeStore.themeName}
              isSupport={isSupport}
            />
          </Link>
        ))}
      </S.Cards>
    </S.ContainerSpace>
  );
};

Space.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  rota: PropTypes.string,
  isSupport: PropTypes.bool,
};
Space.defaultProps = {
  data: [],
  title: '',
  rota: '',
  isSupport: false,
};

export default Space;
