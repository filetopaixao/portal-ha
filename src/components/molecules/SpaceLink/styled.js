import styled from 'styled-components';
import Crd from '../../atoms/Cards/space';

export const ContainerSpace = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;

  padding: 0;
  margin: 0;

  margin-top: 25px;
  max-width: 780px;

  @media (max-width: 425px) {
    margin-top: 0;
    max-width: 330px;
    width: 330px;
    height: 424px;
  }

  @media (max-width: 375px) {
    margin-top: 0;
    max-width: 330px;
    width: 330px;
    height: 424px;
  }

  @media (max-width: 320px) {
    margin-top: 0;
    max-width: 290px;
    width: 290px;
    height: 424px;
  }
`;

export const Card = styled(Crd)`
  font-size: 18px;
  text-align: center;
  text-transform: none;

  padding: 0 10px;
  margin: 0;

  max-width: 180px;
  max-height: 167px;
  width: 180px;
  height: 167px;

  background: transparent;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (max-width: 375px) {
    font-size: 12px;

    max-width: 120px;
    max-height: 96;
    width: 120px;
    height: 96px;
  }

  @media (max-width: 425px) {
    font-size: 12px;

    max-width: 120px;
    max-height: 96;
    width: 120px;
    height: 96px;
  }
`;

export const Cards = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;

  padding: 0;
  margin: 0;

  width: 100%;
  border-radius: 15px;

  background: transparent;

  .link_default {
    text-decoration: none;

    padding: 0;
    margin: 0;

    max-width: 180px;
    max-height: 167px;
    width: 180px;
    height: 167px;

    @media (max-width: 375px) {
      width: 120px;
      height: 96px;
      max-width: 120px;
      max-height: 96;
    }

    @media (max-width: 425px) {
      width: 120px;
      height: 96px;
      max-width: 120px;
      max-height: 96;
    }
  }

  @media (max-width: 425px) {
    justify-content: center;

    width: 330px;
    max-width: 330px;
  }

  @media (max-width: 375px) {
    justify-content: center;

    width: 330px;
    max-width: 330px;
  }

  @media (max-width: 320px) {
    justify-content: center;

    width: 290px;
    max-width: 290px;
  }

  .card_space:not(:last-child) {
    margin-bottom: 20px;
  }

  @media (min-width: 375px) {
    .card_space:nth-child(3),
    .card_space:nth-child(4) {
      margin-bottom: 0;
    }

    .card_space:nth-child(2),
    .card_space:nth-child(4) {
      margin-left: 15px;
    }
  }

  @media (min-width: 414px) {
    justify-content: space-between;
    padding: 20px;

    .card_space:not(:last-child) {
      margin-bottom: 0;
    }
    .card_space:nth-child(2),
    .card_space:nth-child(4) {
      margin-left: 0;
    }
  }
  @media (min-width: 992px) {
    .card_space:nth-child(4) {
      margin-left: 0;
    }
  }
  @media (min-width: 1366px) {
    /* padding: 35px 80px; */
  }
`;

export const Title = styled.h3`
  font-size: 18px;
  font-weight: 600;

  display: block;

  margin-bottom: 10px;

  width: 100%;
`;
