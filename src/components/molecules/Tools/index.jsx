import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

// import Badge from '@components/atoms/Badge';

import * as S from './styled';

const Tool = ({ data, title, onClick }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ContainerTool>
      <S.Title>{title}</S.Title>

      <S.Cards theme={themeStore.themeName}>
        {data.map(({ icon, description, variant, toolsName }) => {
          let desc;
          let link;
          if (description === 'E-COMMERCE') {
            desc = themeStore.tipoApresentacao === 'revenda' ? 'PLATAFORMA' : 'DOAÇÃO RECORRENTE';
            link = '/platform';
          } else if (description === 'CARRINHO DINÂMICO') {
            desc = themeStore.tipoApresentacao === 'revenda' ? 'CARRINHO DINÂMICO' : 'DOAÇÃO I.R.';
            link = '/playlist';
          } else if (description === 'CUPOM DE DESCONTO') {
            desc = themeStore.tipoApresentacao === 'revenda' ? 'CUPOM DE DESCONTO' : 'PASSOS QUE SALVAM';
            link = '/cupom';
          } else if (description === 'LANDING PAGE') {
            desc = themeStore.tipoApresentacao === 'revenda' ? 'LANDING PAGE' : 'DOAÇÃO POR EVENTO';
            link = '/landingPages';
          } else {
            desc = description;
          }

          return (
            <a href={link}>
              <S.CardSale
                icon={icon}
                toolsName={toolsName}
                key={description}
                variant={variant}
                onClick={onClick}
                theme={themeStore.primaryColor}
              >
                <S.CardSaleTitle>{desc}</S.CardSaleTitle>
              </S.CardSale>
            </a>
          );
        })}
      </S.Cards>
    </S.ContainerTool>
  );
};
Tool.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  onClick: PropTypes.func,
};
Tool.defaultProps = {
  data: [],
  title: '',
  onClick: () => false,
};

export default Tool;
