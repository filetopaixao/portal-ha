import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@components/atoms/Button';
import Modal from '@components/molecules/Modals/ModalComponent';
import capaFpass from '@assets/images/capaFpass.png';

import * as S from './styled';

const PopUp = ({ title, isModalVisible, setIsModalVisible }) => {
  // const [active, setActive] = useState(false);
  const themeStore = useSelector((state) => state.theme);
  const handleCloserModal = () => {
    setIsModalVisible(!isModalVisible);
    // setActive(true);
    // setTimeout(() => {
    // setIsModalVisible(!isModalVisible);
    window.location.reload();
    // }, 3000);
  };

  return (
    <S.Container
      primaryColor={themeStore.primaryColor}
      secondaryColor={themeStore.secondaryColor}
      tertiaryColor={themeStore.tertiaryColor}
    >
      <Modal
        alignCenter
        MaxHeight="60vh"
        variant="lg"
        title={title}
        isVisible={isModalVisible}
        onClose={() => setIsModalVisible(!isModalVisible)}
        btnCloser={false}
      >
        <S.Content>
          <S.ContentInfo primaryColor={themeStore.primaryColor}>
            <header>
              <h3>
                Seja muito bem vindo a
                <br />
                Equipe de Agentes FPASS
              </h3>
            </header>

            <p>Parabéns por dar esse grande passo na sua vida profissional, com destino à sua liberdade financeira.</p>
            <p>Vamos decolar nessa jornada rumo ao sucesso!</p>

            <Button variant="primary" type="button" onClick={() => handleCloserModal()}>
              VAMOS LÁ!
            </Button>
          </S.ContentInfo>

          <div className="container-background-cloud">
            <S.ContainerImage>
              <S.Image image={themeStore.capa ? themeStore.capa : capaFpass} />
            </S.ContainerImage>
          </div>
        </S.Content>
      </Modal>
    </S.Container>
  );
};

PopUp.propTypes = {
  title: PropTypes.string,
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
};
PopUp.defaultProps = {
  title: '',
  isModalVisible: false,
  setIsModalVisible: () => {},
};

export default PopUp;
