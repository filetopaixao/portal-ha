import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const InputFile = ({ type, className, name, onBlur, onChange, placeholder, value, variant, errorMessage }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Container theme={themeStore.themeName} primaryColor={themeStore.primaryColor}>
      <S.Label htmlFor="input-file" primaryColor={themeStore.primaryColor}>
        Adicione um arquivo
      </S.Label>
      <S.Input
        id="input-file"
        type={type}
        className={className}
        name={name}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        onBlur={onBlur}
        variant={variant}
      />
      {errorMessage && <S.ErrorMessage>{errorMessage}</S.ErrorMessage>}
    </S.Container>
  );
};

InputFile.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  variant: PropTypes.string,
  errorMessage: PropTypes.string,
};
InputFile.defaultProps = {
  type: 'file',
  className: '',
  name: '',
  onChange: () => false,
  onBlur: () => false,
  placeholder: 'Adicione um arquivo',
  value: '',
  variant: '',
  errorMessage: '',
};

export default InputFile;
