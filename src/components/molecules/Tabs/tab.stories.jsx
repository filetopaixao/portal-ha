import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';
import Tab from './index';
import MenuTab from './menuTab';

export default {
  title: 'molecules/Tab',
  component: Tab,
  decorators: [withKnobs],
};

export const element = () => (
  <ContainerStorybook>
    <Tab />
  </ContainerStorybook>
);

export const menuTab = () => (
  <ContainerStorybook>
    <MenuTab />
  </ContainerStorybook>
);

element.story = {
  name: 'Default',
};
