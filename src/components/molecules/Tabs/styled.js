import styled from 'styled-components';
import { Container as Ct } from '@components/atoms/Container/styled';
import { Button as Bt } from '@components/atoms/Button/styled';

export const Container = styled(Ct)``;

export const GridButtons = styled.div`
  ${(props) => (props.menu ? 'display: block; width: 100%;' : 'display: none;')}

  @media (min-width: 992px) {
    display: grid;

    width: 100%;

    grid-template-columns: ${(props) => {
      const width = 100 / props.qtd;
      let grid = '';
      for (let i = 0; i < props.qtd; i += 1) {
        grid += `${width}% `;
      }
      return `${grid}`;
    }};
  }
`;

export const GridCollapse = styled.div`
  display: block;

  width: 100%;
  @media (min-width: 992px) {
    display: none;
  }
`;

export const Content = styled.main``;

export const ContentCointainer = styled.main`
  ${(props) => (props.menu ? 'display: block; width: 100%;' : 'display: none;')}

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  @media (min-width: 992px) {
    display: block;

    width: 100%;

    padding: 70px 100px;

    border-radius: 20px;
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    ${(props) => {
      if (props.menu) return 'background: none';
      if (props.theme === 'Dark') return '#202731';
      return '#FFFFFF';
    }};
  }
`;

export const Column = styled.div`
  padding: 10px;
`;

export const Button = styled(Bt)`
  font-size: 13px;

  width: 100%;

  ${(props) => {
    if (props.active) {
      return `background: ${props.secondaryColor}; color: #fff;`;
    }
    if (props.theme === 'Dark') {
      return `background: #202731; border: 1px solid #2F3743; color: ${props.secondaryColor};`;
    }
    return `background: #fff; border: 1px solid #EAEAEA; color: ${props.secondaryColor};`;
  }}
`;

export const Welcome = styled.div`
  margin: 10px 0px;
  @media (min-width: 992px) {
    margin: 70px 0px;
    width: 100%;
  }
`;
