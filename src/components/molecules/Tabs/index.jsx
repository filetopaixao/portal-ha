import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

import CollapseOut from '@components/atoms/Collapse/CollapseOut';

import * as S from './styled';

const Tab = ({ content1, content2, content3, content4, buttonTxt1, buttonTxt2, buttonTxt3, buttonTxt4 }) => {
  const themeStore = useSelector((state) => state.theme);
  const [activeBtn1, setActiveBtn1] = useState(true);
  const [activeBtn2, setActiveBtn2] = useState(false);
  const [activeBtn3, setActiveBtn3] = useState(false);
  const [activeBtn4, setActiveBtn4] = useState(false);
  const [content, setContent] = useState(content1);

  const { pathname } = useLocation();
  const routeArray = pathname.split('/');
  const opContent = routeArray[routeArray.length - 1];

  const handleContent1 = useCallback(() => {
    setContent(content1);
    setActiveBtn1(true);
    setActiveBtn2(false);
    setActiveBtn3(false);
    setActiveBtn4(false);
  }, [content1]);

  const handleContent2 = useCallback(() => {
    setContent(content2);
    setActiveBtn1(false);
    setActiveBtn2(true);
    setActiveBtn3(false);
    setActiveBtn4(false);
  }, [content2]);

  const handleContent3 = useCallback(() => {
    setContent(content3);
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(true);
    setActiveBtn4(false);
  }, [content3]);

  const handleContent4 = useCallback(() => {
    setContent(content4);
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(false);
    setActiveBtn4(true);
  }, [content4]);

  const loadContent = useCallback(() => {
    switch (opContent) {
      case 'content1':
        return handleContent1();
      case 'content2':
        return handleContent2();
      case 'content3':
        return handleContent3();
      case 'content4':
        return handleContent4();
      default:
        return setContent(content1);
    }
  }, [handleContent1, handleContent2, handleContent3, handleContent4, content1, opContent]);

  useEffect(() => {
    loadContent();
  }, [loadContent, opContent]);

  const clickButton = (e) => {
    if (e.target.id === 'btn1') {
      if (activeBtn1) {
        setActiveBtn1(false);
      } else {
        handleContent1();
      }
    } else if (e.target.id === 'btn2') {
      if (activeBtn2) {
        setActiveBtn2(false);
      } else {
        handleContent2();
      }
    } else if (e.target.id === 'btn3') {
      if (activeBtn3) {
        setActiveBtn3(false);
      } else {
        handleContent3();
      }
    } else if (e.target.id === 'btn4') {
      if (activeBtn4) {
        setActiveBtn4(false);
      } else {
        handleContent4();
      }
    }
  };

  return (
    <S.Container>
      <S.GridButtons qtd={4}>
        <S.Column>
          <S.Button
            secondaryColor={themeStore.secondaryColor}
            theme={themeStore.themeName}
            onClick={clickButton}
            id="btn1"
            active={activeBtn1}
          >
            {buttonTxt1}
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            secondaryColor={themeStore.secondaryColor}
            theme={themeStore.themeName}
            onClick={clickButton}
            id="btn2"
            active={activeBtn2}
          >
            {buttonTxt2}
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            secondaryColor={themeStore.secondaryColor}
            theme={themeStore.themeName}
            onClick={clickButton}
            id="btn3"
            active={activeBtn3}
          >
            {buttonTxt3}
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button
            secondaryColor={themeStore.secondaryColor}
            theme={themeStore.themeName}
            onClick={clickButton}
            id="btn4"
            active={activeBtn4}
          >
            {buttonTxt4}
          </S.Button>
        </S.Column>
      </S.GridButtons>
      <S.GridCollapse>
        <CollapseOut active={activeBtn1} title={buttonTxt1} onClick={clickButton} id="btn1">
          {content1}
        </CollapseOut>
        <CollapseOut active={activeBtn2} title={buttonTxt2} onClick={clickButton} id="btn2">
          {content2}
        </CollapseOut>
        <CollapseOut active={activeBtn3} title={buttonTxt3} onClick={clickButton} id="btn3">
          {content3}
        </CollapseOut>
        <CollapseOut active={activeBtn4} title={buttonTxt4} onClick={clickButton} id="btn4">
          {content4}
        </CollapseOut>
      </S.GridCollapse>
      <S.ContentCointainer theme={themeStore.themeName}>
        <S.Content>{content}</S.Content>
      </S.ContentCointainer>
    </S.Container>
  );
};

Tab.propTypes = {
  content1: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  content2: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  content3: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  content4: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  buttonTxt1: PropTypes.string,
  buttonTxt2: PropTypes.string,
  buttonTxt3: PropTypes.string,
  buttonTxt4: PropTypes.string,
};

Tab.defaultProps = {
  content1: 'content 1',
  content2: 'content 2',
  content3: 'content 3',
  content4: 'content 4',
  buttonTxt1: 'Button Text 1',
  buttonTxt2: 'Button Text 2',
  buttonTxt3: 'Button Text 3',
  buttonTxt4: 'Button Text 4',
};

export default Tab;
