import styled from 'styled-components';

export const ProductsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: max-content;
  grid-gap: 20px;
  width: 100%;

  @media (max-width: 1300px) and (min-width: 600px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    width: 500px;
    margin: 0 auto;
  }

  @media (min-width: 1300px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr 1fr;
    width: 100%;
  }
`;

export default ProductsContainer;
