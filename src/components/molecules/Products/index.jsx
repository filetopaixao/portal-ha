import React from 'react';
import PropTypes from 'prop-types';

import Product from '@components/atoms/Product';

import * as S from './styled';

const Products = ({ data, page, handleSelected }) => {
  return (
    <>
      {handleSelected ? (
        <S.ProductsContainer type={page}>
          {data.map((item) => (
            <Product
              key={Math.random() * 1000}
              handleSelected={item.isSelected === false ? () => handleSelected(item) : () => {}}
              isSelected={item.isSelected}
              title={item.title}
              srcImg={item.srcImg}
              nameImg={item.nameImg}
              valueProduct={item.valueProduct}
              valueCommision={item.valueCommision}
              urlProduct={item.urlProduct}
              purchase={item.purchase}
              product={item.product}
            />
          ))}
        </S.ProductsContainer>
      ) : (
        <S.ProductsContainer type={page}>
          {data.map((item) => (
            <Product
              key={Math.random() * 1000}
              title={item.title}
              srcImg={item.srcImg}
              nameImg={item.nameImg}
              valueProduct={item.valueProduct}
              valueCommision={item.valueCommision}
              urlProduct={item.urlProduct}
              purchase={item.purchase}
              product={item.product}
            />
          ))}
        </S.ProductsContainer>
      )}
    </>
  );
};

Products.propTypes = {
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.number, PropTypes.string, PropTypes.node]),
  page: PropTypes.string,
  handleSelected: PropTypes.func,
};
Products.defaultProps = {
  data: [],
  page: '',
  handleSelected: () => {},
};

export default Products;
