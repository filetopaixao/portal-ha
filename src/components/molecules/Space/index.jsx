import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Card from '../../atoms/Cards/space';

import * as S from './styled';

const Space = ({ data, title }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ContainerSpace>
      <S.Title>{title}</S.Title>

      <S.Cards qtd={data.lenght} theme={themeStore.themeName}>
        {data.map(({ icon, description, variant, isComing }) => {
          let link;
          let target;
          if (description === 'MARKETING') {
            link = 'https://drive.google.com/drive/folders/10b1cWoZJGHjX3zVEUgcnvKotek6IDxfu';
            target = '_blank';
          } else {
            link = 'https://app.fpass.com.br/login';
            target = '_blank';
          }

          return (
            <a href={link} target={target}>
              <Card
                key={description}
                className="card_space"
                variant={variant}
                icon={icon}
                title={description}
                isComing={isComing}
              />
            </a>
          );
        })}
      </S.Cards>
    </S.ContainerSpace>
  );
};

Space.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
};
Space.defaultProps = {
  data: [],
  title: '',
};

export default Space;
