import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';

import ItenLink from '@components/atoms/ItenLink';

import { Actions as MenuActions } from '@redux/ducks/menu';

import * as S from './styled';

const Menu = ({ data }) => {
  function isvisible(show, isPermission) {
    return show && isPermission;
  }

  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    console.log('PATHNAME', location.pathname);
    dispatch(MenuActions.setSelectedLink(location.pathname !== '/' ? location.pathname : '/home'));
  }, [dispatch, location]);

  const handleSelecteLink = (text) => {
    dispatch(MenuActions.setSelectedLink(text));
  };

  return (
    <nav>
      <S.ListLinks>
        {data.map(({ text, icon, disabled, isPermission, selected, count, haveChildren, show, route }) =>
          isvisible(show, isPermission) ? (
            <ItenLink
              key={text}
              text={text}
              icon={icon}
              count={count}
              disabled={disabled}
              isPermission={isPermission}
              selected={selected}
              haveChildren={haveChildren}
              route={route}
              handleSelecteLink={handleSelecteLink}
            />
          ) : (
            ''
          )
        )}
      </S.ListLinks>
    </nav>
  );
};
Menu.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
};
Menu.defaultProps = {
  data: [],
};

export default Menu;
