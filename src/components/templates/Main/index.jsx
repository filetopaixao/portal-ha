import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import Header from '@components/atoms/Header';
import Menu from '@components/molecules/Menu';
import Logo from '@components/atoms/Logo';
import LogoMobile from '@components/atoms/Logo/logo-mobile';
import Spacer from '@components/atoms/spacer';

import { Actions as MenuActions } from '@redux/ducks/menu';
import Arrow from '@assets/images/arrow-left.webp';

import * as S from './styled';

const Template = ({ children, title }) => {
  const themeStore = useSelector((state) => state.theme);
  const menu = useSelector((state) => state.menu);
  const dispatch = useDispatch();
  dispatch(MenuActions.getAccess());

  return (
    <S.Container retractable={menu.retractable} className="app" theme={themeStore.themeName}>
      <S.Sidebar id="sidebar" theme={themeStore.themeName}>
        {menu.retractable ? <LogoMobile size="sm" /> : <Logo size="md" />}

        <Spacer size={menu.retractable ? 'sm' : 'md'} />
        <Menu data={menu.links} />
      </S.Sidebar>

      <S.SidebarMobile theme={themeStore.themeName} id="sidebar_retractable" retractableMobile={menu.retractableMobile}>
        <div className="logo-buton">
          <Logo size="md" />

          <S.HeaderRetractable onClick={() => dispatch(MenuActions.retractableMobileMenu())}>
            <img src={Arrow} alt="Voltar" />
          </S.HeaderRetractable>
        </div>
        <Spacer size="md" />
        <Menu data={menu.links} />
      </S.SidebarMobile>

      <S.MainContainer theme={themeStore.themeName}>
        <Header id="header" />

        <S.Title onClick={() => dispatch(MenuActions.retractableMobileMenuFalse())} theme={themeStore.themeName}>
          <h3>{title}</h3>
        </S.Title>

        <S.Children onClick={() => dispatch(MenuActions.retractableMobileMenuFalse())}>{children}</S.Children>
      </S.MainContainer>
    </S.Container>
  );
};

Template.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  title: PropTypes.string.isRequired,
};

export default Template;
